import React from "react";
import Ajax from "../REST/Ajax";
import StateStorage from "../REST/StateStorage";
import {
  Table,
  Button,
  ButtonToolbar,
  Glyphicon,
  ButtonGroup,
  FormGroup,
  FormControl,
  ControlLabel,
  Col,
  Form,
  Panel,
  PageHeader
} from "react-bootstrap";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Moment from "moment";
import MomentFormat from "moment-duration-format";
import Authentication from "../REST/Authentication.js";
import { RingLoader } from "react-spinners";
import TableActions from "./TableActions";

export default class TournamentList extends React.Component {
  constructor() {
    super();
    this.state = {
      tournaments: [],
      dataFetched: false
    };
  }

  componentWillMount() {
    Ajax.get("rest/tournament").end(
      function(body, resp) {
        if (resp.status === 200) {
          this.flattenData(body);
        }
        this.setState({ dataFetched: true });
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
  }

  flattenData = data => {
    this.setState({
      tournaments: data.map(tournament => {
        return {
          id: tournament.id,
          name: tournament.name,
          sport: tournament.sport,
          organizer: tournament.organizer.username,
          place: tournament.place,
          date: Moment(tournament.date, "DD/MM/YYYY", false).format(),
          time: tournament.time,
          participants: tournament.participants,
          description: tournament.description,
          submitted: tournament.matches.length > 0 ? true : false,
          winner: tournament.winner
        };
      })
    });
  };

  dateFormatter = (date, row) => {
    return Moment(date).format("DD/MM/YYYY");
  };

  getExpandedRow(row) {
    return (
      <div>
        <div>
          <strong>Start</strong>: {row.time}
        </div>
        <div>
          <strong>Organizer</strong>: {row.organizer}
        </div>
        <div>
          <strong>Description</strong>: {row.description}
        </div>
        <div>
          <strong>Number of participants</strong>: {row.participants.length}
        </div>
        <div>
          <strong>State</strong>:{" "}
          {row.submitted
            ? row.winner ? "Finished" : "In progress"
            : "Has not commenced yet"}
        </div>
        <div>
          <strong>Winner</strong>: {row.winner ? row.winner.username : "TBD"}
        </div>
      </div>
    );
  }

  getActions = (cell, row) => {
    return (
      <TableActions
        row={row}
        remove={this.handleTournamentRemove}
        redirect={this.handleTournamentRedirect}
      />
    );
  };

  handleTournamentRedirect = tournament => {
    Authentication.authorize(this.okCallback(tournament), () => {
      this.props.history.push("/");
    });
  };

  okCallback = tournament => {
    if (tournament.submitted) {
      this.props.history.push({
        pathname: "/tournament",
        state: { tournament: tournament }
      }); //Main Page
    } else {
      this.props.history.push({
        pathname: "/addplayers",
        state: { tournament: tournament }
      });
    }
  };

  handleTournamentRemove = tournament => {
    Ajax.del("rest/tournament/" + tournament.id).end(
      function(body, resp) {
        if (resp.status === 200) {
          var array = [...this.state.tournaments]; // make a separate copy of the array
          var index = array.indexOf(tournament);
          array.splice(index, 1);
          this.setState({ tournaments: array });
        }
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
    //TODO Ajax
  };

  handleFilterChange = event => {
    var filter = {
      participant: false,
      organizer: false
    };
    if (event.target.value !== "all") {
      filter[event.target.value] = true;
    }
    this.setState({ dataFetched: false });
    Ajax.post("rest/tournament/filter", filter).end(
      function(body, resp) {
        if (resp.status === 200) {
          this.flattenData(body);
        }
        this.setState({ dataFetched: true });
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
    //TODO Ajax
  };

  getNoDataIndicator = () => {
    if (this.state.dataFetched) {
      return "No tournaments found";
    }
    return (
      <div className="spinner">
        <RingLoader color={"#123abc"} loading={!this.state.dataFetched} />
      </div>
    );
  };

  render() {
    const options = {
      expandRowBgColor: "#f5f5f5", //TODO vyber nejakou hezci barvu
      expandBy: "column",
      noDataText: this.getNoDataIndicator()
    };
    const filter = (
      <FormControl
        onChange={this.handleFilterChange}
        componentClass="select"
        placeholder="select"
      >
        <option value="all">All</option>
        <option value="organizer">Organizer</option>
        <option value="participant">Participant</option>
      </FormControl>
    );

    return (
      <div>
        <PageHeader>Tournament list</PageHeader>
     
        <div className="tournament-list">
          <BootstrapTable
            options={options}
            keyField="id"
            data={this.state.tournaments}
            expandableRow={row => {
              return true;
            }}
            expandComponent={this.getExpandedRow}
            pagination
            trClassName="table-row"
          >
            <TableHeaderColumn
              dataField="name"
              dataSort={true}
              filter={{ type: "TextFilter" }}
            >
              Name
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="sport"
              filter={{ type: "TextFilter" }}
              dataSort={true}
            >
              Sport
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="date"
              dataFormat={this.dateFormatter}
              dataSort={true}
              filter={{
                type: "DateFilter",
                defaultValue: { date: new Date(), comparator: ">=" }
              }}
            >
              Date
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="place"
              filter={{ type: "TextFilter" }}
            >
              Place
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="button"
              expandable={false}
              dataFormat={this.getActions}
            >
              Role{filter}
            </TableHeaderColumn>
          </BootstrapTable>
          </div>
       
      </div>
    );
  }
}
