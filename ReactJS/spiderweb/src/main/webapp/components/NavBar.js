import React from "react";
import Link from "react-router-dom";
import Cookie from "../REST/Cookie";
import {
  Navbar,
  NavItem,
  Nav,
  NavDropdown,
  MenuItem,
  Glyphicon,
  Popover,
  OverlayTrigger
} from "react-bootstrap";
import Authentication from "../REST/Authentication";
import Ajax from "../REST/Ajax";

//společný pro všechny komponenty, krome Welcome
export default class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      user:null
    }
  }
  clickHandlerT = () => {
    Authentication.authorize(this.callbackT, this.callback);
  };

  clickHandlerC = () => {
    Authentication.authorize(this.callbackC, this.callback);
  };

  callbackT = () => {
    console.log("Callback T");
    this.props.history.push("/tournamentlist");
  };

  handleTooltip = () => {
    console.log("Tooltip");
  };

  callbackC = () => {
    console.log("Callback C");
    this.props.history.push("/create");
  };

  logout = () => {
    Authentication.logout(this.callback);
  };
  callback = () => {
    console.log("Callback reload");
    this.props.history.push("/");
    //window.location.reload();
  };
  componentDidMount(){
    if(!this.state.user){
      Ajax.get("rest/user/current").end((body, resp)=>{
        if(body){
          this.setState({user:body.username});
        }

      }, ()=>{
        console.log("NO current");
      });
    }
  }
  render() {
    const tips = (
      <Popover id="popover-trigger-hover-focus" title="Popover bottom">
        <strong>Holy guacamole!</strong> Check this info.
      </Popover>
    );
    const user = (<div style={{display: "inline-block"}}>  <Glyphicon glyph="user" />{this.state.user?"   "+this.state.user:"   Anonym"}</div>);
    return (
      <Navbar fixedTop className="sw-nav">
        <Navbar.Header className="sw-navheader">
          <Navbar.Brand>
            <a href="#" onClick={this.clickHandlerT}>
              <img src="./spiderweb-logo.png" />
            </a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>

        <Navbar.Collapse>
          <Nav>
            <NavItem eventKey={1} onClick={this.clickHandlerT}>
              <Glyphicon glyph="list" /> Tournaments
            </NavItem>
            <NavItem eventKey={2} onClick={this.clickHandlerC}>
              <Glyphicon glyph="plus" /> Create
            </NavItem>
          </Nav>
          
          <Nav pullRight>


            <NavDropdown eventKey={3} title={user} id="basic-nav-dropdown">

              <MenuItem eventKey={3.1} onClick={this.logout}><Glyphicon glyph="log-out" /> Logout</MenuItem>

            </NavDropdown>
          </Nav>
        </Navbar.Collapse>


      </Navbar>
    );
    ///return normal navBar
    // }else{
    //   //return nothing
    //   return <div>Nothing</div>;
    // }
  }
}
