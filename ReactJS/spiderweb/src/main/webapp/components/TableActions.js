import React from "react";
import Ajax from "../REST/Ajax";
import StateStorage from "../REST/StateStorage";
import {
  Table,
  Button,
  ButtonToolbar,
  Glyphicon,
  ButtonGroup,
  FormGroup,
  FormControl,
  ControlLabel,
  Col,
  Form,
  Panel,
  PageHeader
} from "react-bootstrap";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Moment from "moment";
import MomentFormat from "moment-duration-format";
import Authentication from "../REST/Authentication.js";
import { RingLoader } from "react-spinners";
import swal from 'sweetalert';

export default class TableActions extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: false
    };
  }

  render() {
    const row = this.props.row;
    return window.localStorage.getItem("username") === row.organizer ? (
      <ButtonToolbar className="table-actions">
        <Glyphicon onClick={() => this.props.redirect(row)} glyph="edit" />
        {this.state.loading ? (
          <span className="glyphicon">
            <RingLoader
              size={16}
              color={"#123abc"}
              loading={this.state.loading}
            />
          </span>
        ) : (
          <Glyphicon
            onClick={() => {
              this.setState({ loading: true });
              swal({
                title: "Are you sure?",
                text: "You are going to delete tournament "+this.props.row.name+". Once deleted, you will not be able to recover this tournament!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                  swal("Removed!", {
                    icon: "success",
                  });
                  this.props.remove(row);
                }
                else{
                  this.setState({loading:false});
                }
              });

            }}
            glyph="trash"
          />
        )}
      </ButtonToolbar>
    ) : row.submitted ? (
      <ButtonToolbar>
        <Glyphicon
          onClick={() => {
            this.props.redirect(row);
          }}
          glyph="edit"
        />
      </ButtonToolbar>
    ) : (
      <ButtonToolbar />
    );
  }
}
