import React from "react";
import Match from "./Match.js";
import Ajax from "../REST/Ajax";
import StateStorage from "../REST/StateStorage";
import { PageHeader } from "react-bootstrap";
import { RingLoader } from "react-spinners";
export default class Tournament extends React.Component {
  constructor(props) {
    super(props);
    this.matchRefs = [];
    this.state = {
      id: "",
      name: "",
      organizer: "",
      matches: [],
      render: false,
      loading: true,
      winner: null
    };
  }

  componentDidMount() {
    this.stateStorage.set(this.state.id);
  }

  componentDidUpdate() {
    this.stateStorage.set(this.state.id);
  }

  componentWillUnmount() {
    this.stateStorage.remove();
  }
  componentWillMount() {
    this.stateStorage = new StateStorage("tournament");
    this.getData();
  }

  updateCallback = match => {
    //TODO rerender match components only

    if (match.index !== 0) {
      const next = Math.floor((match.index - 1) / 2);
      const isPlayerOne = (match.index - 1) % 2 == 0;
      this.matchRefs[next].setAdvancingPlayer(match.winner, isPlayerOne);
    } else {
      this.setState({ winner: match.winner });
    }
  };

  getData = () => {
    const tid = this.props.location.state
      ? this.props.location.state.tournament.id
      : this.stateStorage.get();
    Ajax.get("rest/tournament/" + tid).end(
      function(body, resp) {
        if (resp.status === 200) {
          const id = body.id;
          const organizer = body.organizer;
          const matches = body.matches.map((match, index) => {
            return (
              <Match
                key={index}
                index={index}
                update={this.updateCallback}
                organizer={organizer}
                data={match}
                ref={match => {
                  this.matchRefs[index] = match;
                }}
              />
            );
          });
          const name = body.name;

          const winner = body.winner ? body.winner : null;
          this.setState({
            id,
            name,
            organizer,
            matches,
            loading: false,
            winner
          });
        }
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
  };

  render() {
    return (
      <div className="middle-spinner">
        <PageHeader>{this.state.name}</PageHeader>
        {this.state.winner ? (
          <h3 className="winner-header">
            <img className="winner-left" src="stars1.svg" /> Player{" "}
            <strong>{this.state.winner.username}</strong> won!<img
              className="winner-right"
              src="stars1.svg"
            />
          </h3>
        ) : (
          <div />
        )}
        {this.getTreeJSX(this.state.matches)}
        <RingLoader color={"#123abc"} loading={this.state.loading} />
      </div>
    );
  }
  getTreeJSX = data => {
    return <div className="main">{this.getRounds(data)}</div>;
  };
  getRounds = data => {
    let toR = [];
    for (var i = this.getBaseLog(2, data.length + 1) - 1; i >= 0; i--) {
      toR.push(
        <div key={i + 1} className={"col col_" + (i + 1)}>
          {this.getMatches(i, data)}
        </div>
      );
    }
    return toR;
  };
  getMatches = (i, data) => {
    let toR = [];
    for (var j = Math.pow(2, i + 1) - 1; j > Math.pow(2, i) - 1; j--) {
      toR.push(
        <div key={j - 1} className={"data data_" + (i + 1)}>
          {data[j - 1]}
        </div>
      );
    }
    return toR;
  };

  getBaseLog = (x, y) => {
    return Math.log(y) / Math.log(x);
  };
}
