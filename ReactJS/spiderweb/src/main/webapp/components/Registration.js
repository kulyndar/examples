import React from "react";
import {
  OverlayTrigger,
  Popover,
  Col,
  Form,
  FormGroup,
  ControlLabel,
  Button,
  Alert,
  HelpBlock
} from "react-bootstrap";
import FormControl from "react-bootstrap/lib/FormControl";
import Ajax from "../REST/Ajax";
import Authentication from "../REST/Authentication";
import Cookie from "../REST/Cookie";
import { RingLoader } from "react-spinners";

export default class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.errCallback = this.props.errCallback;
    this.state = {
      username: "",
      password: "",
      confirmation: "",
      usernameStatus: null,
      passwordStatus: null,
      loading: false,
      exists:false
    };
  }
  checkExist=()=>{
    if(this.state.username){
    Ajax.get("rest/user/exists?username="+this.state.username).end(()=>{
      if (this.state.username.length < 6) {
        this.setState({ usernameStatus: "error" });
      }else{
          this.setState({ usernameStatus: "success" });
      }
    }, ()=>{
      console.log("Exists");
      this.setState({exists:true});
      this.setState({usernameStatus:"error"});
    });

  }

  }
  /*Registration function. Sends registration data to backend*/
  registrate = () => {
    const user = {
      username: this.state.username, //name like in Java
      password: this.state.password
    };
    this.setState({ loading: true });
    //TODO map to Java
    Ajax.post("rest/registration", user).end(
      function(body, resp) {
        if (resp.status === 201) {
          Authentication.login(
            user.username,
            user.password,
            this.errorCallback,
            this.callback
          );
        }
      }.bind(this),
      function() {
        this.setState({ loading: false });
        this.errCallback("Username already exists");
      }.bind(this)
    );
  };
  callback = () => {
    console.log("Login ok. Setting cookie and redirect");
    Cookie.setCookie("logged", "true");
    window.localStorage.setItem("username", this.state.username);
    this.props.history.push("/");
  };
  errorCallback = () => {
    this.setState({ loading: false });
    console.log("Error"); //TODOerror callback
  };

  /*Validation. If everything is OK, calls registration, otherwise render error alert/popover*/
  validate = e => {
    e.preventDefault();
    console.log("validate");
    if(this.state.exists){
      return;
    }

    let ok = true;
    if (this.state.username.length < 6) {
      this.setState({ usernameStatus: "error" });
      ok = false;
    }
    if (this.state.password.length < 8) {
      this.setState({ passwordStatus: "error" });
      ok = false;
    } else {
      this.setState({ passwordStatus: "success" });
    }
    if (this.state.password != this.state.confirmation) {
      this.setState({ passwordStatus: "error" });
      this.setState({ password: "" });
      this.setState({ confirmation: "" });
      ok = false;
    }
    if (ok) {
      this.registrate();
    }
  };

  /*Handles changes in username, password and confirmation inputs*/
  handleChangeUsername = e => {
    this.setState({exists:false});
    this.setState({ username: e.target.value });
    if (this.state.username.length > 4) {
      //it starts from 0
      this.setState({ usernameStatus: "success" });
    } else {
      this.setState({ usernameStatus: null });
    }
  };
  handleChangePassword = e => {
    this.setState({ password: e.target.value });
    this.setState({ passwordStatus: null });
  };
  hadndleChangeConfirm = e => {
    this.setState({ confirmation: e.target.value });
    this.setState({ passwordStatus: null });
  };
onBlurPassword=()=>{
  if (this.state.password.length < 8) {
    this.setState({ passwordStatus: "error" });
  }else{
      this.setState({ passwordStatus: "success" });
  }
}
  /*Utils to show error and tooltips*/
  getValidationState = type => {
    if (type === "username") {
      return this.state.usernameStatus;
    } else {
      return this.state.passwordStatus;
    }
  };
  getPopover = type => {
    switch (type) {
      case "username-default":
        return (
          <Popover id="popover-trigger-hover" title="Username">
            Username has to contain min 6 letters/numbers
          </Popover>
        );
      case "password":
        return (
          <Popover id="popover-trigger-hover" title="Password">
            Password has to contain min 8 letters/numbers
          </Popover>
        );
      case "confirm":
        return (
          <Popover id="popover-trigger-hover" title="Password">
            Password has to contain min 8 letters/numbers
          </Popover>
        );
    }

    //TODO ADD ERROR POPOVERS (or alerts)
  };

  render() {
    return (
      <Form horizontal={true} className="form">
        <FormGroup
          controlId="username"
          validationState={this.getValidationState("username")}
        >
          <Col componentClass={ControlLabel} htmlFor="username" sm={3}>
            Username
          </Col>
          <Col sm={12} md={6}>
            <OverlayTrigger
              trigger="focus"
              placement="right"
              overlay={this.getPopover("username-default")}
            >
              <FormControl
                type="text"
                placeholder="Username"
                value={this.state.username}
                onChange={this.handleChangeUsername}
                onBlur={this.checkExist}
              />
            </OverlayTrigger>
            {this.state.exists?<HelpBlock>Username already exists. Try another one please</HelpBlock>:<div></div>}
            <FormControl.Feedback />
          </Col>
        </FormGroup>

        <FormGroup
          controlId="password"
          validationState={this.getValidationState("password")}
        >
          <Col componentClass={ControlLabel} htmlFor="password" sm={3}>
            Password
          </Col>
          <Col sm={12} md={6}>
            <OverlayTrigger
              trigger="focus"
              placement="right"
              overlay={this.getPopover("password")}
            >
              <FormControl
                type="password"
                placeholder="Password"
                value={this.state.password}
                onChange={this.handleChangePassword}
                onBlur={this.onBlurPassword}
              />
            </OverlayTrigger>
            <FormControl.Feedback />
          </Col>
        </FormGroup>

        <FormGroup
          controlId="confirm"
          validationState={this.getValidationState("password")}
        >
          <Col componentClass={ControlLabel} htmlFor="confirm" sm={3}>
            Confirm Your Password
          </Col>
          <Col sm={12} md={6}>
            <OverlayTrigger
              trigger="focus"
              placement="right"
              overlay={this.getPopover("confirm")}
            >
              <FormControl
                type="password"
                placeholder="Confirm Your Password"
                value={this.state.confirmation}
                onChange={this.hadndleChangeConfirm}
              />
            </OverlayTrigger>
            <FormControl.Feedback />
          </Col>
        </FormGroup>

        <FormGroup>
          {this.state.loading ? (
            <Col className="login-loader" md={12} sm={12}>
              <RingLoader
                size={40}
                color={"#123abc"}
                loading={this.state.loading}
              />
            </Col>
          ) : (
            <Col mdOffset={3} md={6} sm={10}>
              <Button
                type="submit"
                block
                bsStyle="success"
                onClick={this.validate}
              >
                Sign up
              </Button>
            </Col>
          )}
        </FormGroup>
      </Form>
    );
  }
}
