import React from "react";
import {
  ListGroup,
  ListGroupItem,
  Button,
  Glyphicon,
  Form,
  FormGroup,
  Col,
  MenuItem,
  Panel,
  PageHeader,
  Popover,
  OverlayTrigger,
  FormControl,
  HelpBlock
} from "react-bootstrap";
import { AsyncTypeahead } from "react-bootstrap-typeahead";
import Ajax from "../REST/Ajax";
import StateStorage from "../REST/StateStorage";
import Authentication from "../REST/Authentication.js";
import { RingLoader } from "react-spinners";
import "react-bootstrap-typeahead/css/Typeahead.css";

export default class AddPlayers extends React.Component {
  constructor(props) {
    super(props);
    this.stateStorage = new StateStorage("add");
    if (!this.props.location.state) {
      this.state = this.stateStorage.get();
    } else {
      this.state = {
        isLoading: false,
        options: [],
        multiple: true,
        participants: [],
        tournament: this.props.location.state.tournament,
        validate: false,
        loading: false
      };
    }
  }

  componentDidMount() {
    this.stateStorage.set(this.state);
  }

  componentDidUpdate() {
    this.stateStorage.set(this.state);
  }

  componentWillUnmount() {
    this.stateStorage.remove();
  }

  handleSubmit = e => {
    e.preventDefault();
    Authentication.authorize(this.okCallback, () => {
      this.props.history.push("/");
    });
  };

  getValidationState = () => {
    if (!this.state.validate) {
      return null;
    }
    if (this.validate()) {
      return "success";
    }
    return "error";
  };

  okCallback = () => {
    console.log(this.typeahead);
    this.setState({ validate: true });
    if (this.validate()) {
      this.setState({ loading: true });
      const data = {
        id: this.state.tournament.id,
        playersUsernames: this.state.participants.map(participant => {
          return participant.username;
        })
      };
      Ajax.post("rest/tournament/submit", data).end(
        function(body, resp) {
          if (resp.status === 200) {
            this.stateStorage.remove();
            this.props.history.push({
              pathname: "/tournament",
              state: { tournament: body }
            });
          }
        }.bind(this),
        function(err) {
          this.setState({ loading: false });
          alert("SMTH is wrong"); //TODO customize error
        }.bind(this)
      );
    }
  };

  validate = () => {
    return this.state.participants.length > 1;
  };

  handleSearch = query => {
    this.setState({ isLoading: true });

    Ajax.get("rest/user?search=" + query).end(
      function(newUsers, resp) {
        if (resp.status === 200) {
          var options = this.state.options;
          newUsers.map(user => {
            if (
              !this.findParticipantInArray(this.state.options, user) &&
              !this.findParticipantInArray(this.state.participants, user)
            ) {
              options = [...options, user];
            }
          });
          this.setState({
            isLoading: false,
            options
          });
        }
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
  };

  handleSelect = selected => {
    console.log(selected);
    this.setState({
      participants: selected
    });

    console.log(this.typeahead);
  };

  findParticipantInArray = (array, user) => {
    for (var i = 0; i < array.length; i++) {
      if (array[i].username == user.username) {
        return array[i];
      }
    }
    return false;
  };

  handleRemove = (array, participant) => {
    var array = [...this.state.participants]; // make a separate copy of the array
    var index = array.indexOf(participant);
    array.splice(index, 1);
    this.setState({ participants: array });
  };

  renderParticipantList = () => {
    if (this.state.participants.length != 0) {
      return (
        <ListGroup>
          {this.state.participants.map((participant, index) => {
            return (
              <ListGroupItem key={index}>
                {participant.username}

                <Glyphicon
                  className="pull-right"
                  onClick={() => this.handleRemove(participant)}
                  glyph="trash"
                />
              </ListGroupItem>
            );
          })}
        </ListGroup>
      );
    }
  };

  render() {
    return (
      <div className="middle-spinner">
        <PageHeader>
          Add players to <strong>{this.state.tournament.name}</strong>
        </PageHeader>
        <Panel className="sw-content">
          <div className="sw-add-container">
            <Col lg={3} md={3} sm={3} xs={3}>
              <Form
                onSubmit={this.handleSubmit}
                className="sw-add"
                horizontal={true}
              >
                <FormGroup
                  validationState={this.getValidationState()}
                  controlId="search"
                >
                  <Col>
                    <AsyncTypeahead
                      options={this.state.options}
                      isLoading={this.state.isLoading}
                      multiple={this.state.multiple}
                      ref={typeahead => (this.typeahead = typeahead)}
                      labelKey="username"
                      minLength={1}
                      onSearch={this.handleSearch}
                      onChange={this.handleSelect}
                      placeholder="Search for a user..."
                      renderMenuItemChildren={(option, props) => (
                        <div>
                          <strong>{option.username}</strong>
                        </div>
                      )}
                    />
                  </Col>

                  <FormControl.Feedback />

                  {this.getValidationState() == "error" ? (
                    <HelpBlock>Add at least 2 players</HelpBlock>
                  ) : (
                    <div />
                  )}
                </FormGroup>
              </Form>
            </Col>

            <Button
              disabled={this.state.loading}
              type="submit"
              onClick={this.handleSubmit}
            >
              Submit
            </Button>
          </div>
        </Panel>
        <RingLoader color={"#123abc"} loading={this.state.loading} />
      </div>
    );
  }
}
