import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Welcome from "./Welcome";
import Login from "./Login";
import TournamentList from "./TournamentList";
import CreateTournament from "./CreateTournament";
import AddPlayers from "./AddPlayers";
import Registration from "./Registration";
import NotFound from "./NotFound404";
import NavBar from "./NavBar";
import Cookie from "../REST/Cookie";
import { Panel } from "react-bootstrap";
import Tournament from "./Tournament";
import HistoryKeeper from "./HistoryKeeper";
import Authentication from "../REST/Authentication.js";

export default class App extends Component {
  constructor(props) {
    super(props);
  }
  getRouter() {
    if (Cookie.getCookie("logged") == "true") {
      return (
        <div>
          {/* <NavBar /> */}
          <Route component={NavBar} />
          <Route component={HistoryKeeper} />
          <Switch>
            <Route path="/tournamentlist" component={TournamentList} />
            <Route path="/create" component={CreateTournament} />
            <Route path="/addplayers" component={AddPlayers} />
            <Route path="/tournament" component={Tournament} />
            {/* Main page */}
            <Route exact path="/" component={TournamentList} />
            <Route component={NotFound} />
          </Switch>
        </div>
      );
    } else {
      return (
        //TODO check if not logged in then welcome page
        <div>
          <Route component={HistoryKeeper} />
          <Switch>

            <Route path="/" component={Welcome} />
            
          </Switch>
        </div>
      );
    }
  }

  render() {
    return this.getRouter();
  }
}
