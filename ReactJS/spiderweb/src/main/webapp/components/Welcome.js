import React from "react";
import {
  Button,
  Grid,
  Row,
  Col,
  Tab,
  Nav,
  NavItem,
  Transition,
  Alert
} from "react-bootstrap";
import Login from "./Login";
import Registration from "./Registration";
import { RingLoader } from "react-spinners";

export default class Welcome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      message: ""
    };
  }
  clickLogin = () => {
    this.props.history.push("/login");
  };
  clickRegistrate = () => {
    this.props.history.push("/registrate");
  };

  showAlert = message => {
    this.setState({ error: true, message });
  };

  /*Closes alert*/
  handleDismiss = () => {
    this.setState({ error: false });
  };
  getAlert = () => {
    return (alert = (
      <Alert bsStyle="danger" onDismiss={this.handleDismiss}>
        <h4>Oh snap! You got an error!</h4>
        <p>{this.state.message}</p>
      </Alert>
    ));
  };

  render() {
    console.log("WELCOME EVERYONE");
    const welcome = (
      <div className="welcome">
        <h1>Welcome</h1>
        <br />

        <p>
          Creating brackets online - have never been easier. SpiderWeb allows
          you to easily create and manage multiple tournaments online. Add
          participants, manage games, announce winners, solve conflicts and let
          players write down their scores.
        </p>
        <img src="spiderweb-logo.png" className="welcome-logo" />
      </div>
    );
    const login = (
      <Login errCallback={this.showAlert} history={this.props.history} />
    );
    const registration = (
      <Registration errCallback={this.showAlert} history={this.props.history} />
    );
    const content = (
      <Grid>
        <Col xs={6} mdOffset={2} md={8}>
          <Row>{this.state.error ? this.getAlert() : ""}</Row>

          <Tab.Container id="sign" defaultActiveKey="first">
            <Row className="clearfix tabs-content">
              <Col sm={12} md={12}>
                <img src="spiderweb-logo.png" className="welcome-logo" />
                <Tab.Content animation>
                  <Tab.Pane eventKey="first">{login}</Tab.Pane>
                  <Tab.Pane eventKey="second">{registration}</Tab.Pane>
                </Tab.Content>
                <Nav justified bsStyle="pills">
                  <NavItem eventKey="first">
                    Already have an account? Sign in
                  </NavItem>
                  <NavItem eventKey="second">Create new account</NavItem>
                </Nav>
              </Col>
            </Row>
          </Tab.Container>
        </Col>
      </Grid>
    );
    return content;
  }
}
