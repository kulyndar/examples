import React from "react";
import {
  Panel,
  Form,
  FormGroup,
  FormControl,
  ControlLabel,
  Col,
  Modal,
  Button,
  Glyphicon,
  OverlayTrigger,
  Popover
} from "react-bootstrap";
import Privilage from "../REST/Privilage";
import Result from "../REST/Result";
import Ajax from "../REST/Ajax";
import Authentication from "../REST/Authentication.js";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import swal from "sweetalert";

//Reprezentuje zápas
export default class Match extends React.Component {
  constructor(props) {
    super(props);
    this.privilage = new Privilage(this.props.data, this.props.organizer);
    const data = {};
    this.state = {
      id: this.props.data.id,
      index: this.props.index,
      playerOne: this.props.data.playerOne,
      playerTwo: this.props.data.playerTwo,
      organizer: this.props.organizer,

      update: this.props.update,
      status: this.props.data.status,
      notes: this.props.data.notes ? this.props.data.notes : [],
      newNote: "",
      show: false,
      validate: false,
      winner: ""
    };
  }

  componentDidMount() {
    this.setState({
      scoreOne: this.getScore("scoreOne"),
      scoreTwo: this.getScore("scoreTwo")
    });
  }

  hasPermission = () => {
    return (
      this.privilage.isPlayerOne() ||
      this.privilage.isPlayerTwo() ||
      this.privilage.isOrganizer()
    );
  };

  getStatusColor = () => {
    var backgroundColor = "#f8f8f8";
    if (this.state.status === "TBD") {
      backgroundColor = "#faebcc";
    } else if (this.state.status === "CONFLICT") {
      backgroundColor = "#ebccd1";
    } else if (this.state.status === "OK") {
      backgroundColor = "#d6e9c6";
    }
    return { backgroundColor };
  };

  hasBothPlayers = () => {
    return this.state.playerOne && this.state.playerTwo;
  };

  handleSubmit = () => {
    Authentication.authorize(this.okCallback, () => {
      this.props.history.push("/");
    });
  };

  okCallback = () => {
    this.setState({ validate: true });

    if (this.validate()) {
      const result = {
        scoreOne: this.state.scoreOne,
        scoreTwo: this.state.scoreTwo
      };

      Ajax.post("rest/match/" + this.state.id + "/setResult", result).end(
        function(body, resp) {
          if (resp.status === 200) {
            this.setState({
              status: body.status
            });

            if (this.state.status === "OK") {
              this.setState({
                winner:
                  this.playerOneIsWinner() === "winner"
                    ? this.state.playerOne
                    : this.state.playerTwo
              });
              const match = this.state;
              this.state.update(match);
            }
          }
        }.bind(this),
        function(err) {
          alert("SMTH is wrong"); //TODO customize error
        }.bind(this)
      );
    }
  };

  playerOneIsWinner = () => {
    if (
      this.state.status === "OK" &&
      this.getValidationState("scoreOne") !== "error"
    ) {
      return this.state.scoreOne > this.state.scoreTwo || !this.state.playerTwo
        ? "winner"
        : "loser";
    }
    return "not-decided";
  };

  playerTwoIsWinner = () => {
    if (
      this.state.status === "OK" &&
      this.getValidationState("scoreTwo") !== "error"
    ) {
      return this.state.scoreTwo > this.state.scoreOne || !this.state.playerOne
        ? "winner"
        : "loser";
    }
    return "not-decided";
  };

  validate = () => {
    console.log(this.state.scoreOne);
    console.log(this.state.scoreTwo);
    if (this.state.scoreOne != null && this.state.scoreTwo != null) {
      console.log("not empty");
      if (isNaN(this.state.scoreOne) || isNaN(this.state.scoreTwo)) {
        swal({
          title: "Invalid input",
          text: "Score fields acctept numbers only",
          icon: "error",
          button: "close",
          dangerMode: true
        });
        return false;
      } else if (this.state.scoreOne == this.state.scoreTwo) {
        console.log("draw");
        swal({
          title: "Draw not allowed",
          text: "Match must not result in draw",
          icon: "error",
          button: "close",
          dangerMode: true
        });
        return false;
      }

      return true;
    }
    return false;
  };

  getValidationState = key => {
    const other = key == "scoreOne" ? "scoreTwo" : "scoreOne";
    if (!this.state.validate) {
      return null;
    }
    if (
      this.state[key] != null &&
      this.state[key] != this.state[other] &&
      !isNaN(this.state[key])
    ) {
      return "success";
    }
    return "error";
  };

  getResults = () => {
    var results = [this.props.data.resultOne, this.props.data.resultTwo];
    const flat = results.map(r => {
      return {
        scoreOne: r.scoreOne,
        scoreTwo: r.scoreTwo,
        author: r.author.username,
        notes: r.notes
      };
    });
    return flat;
  };

  //TODO this shit is disgusting and needs rework
  getScore = key => {
    if (this.props.data.status === "OK" && this.hasBothPlayers()) {
      return this.props.data.resultOne[key];
    }
    if (this.props.data.status === "CONFLICT" && this.privilage.isOrganizer()) {
      return;
    }
    if (this.privilage.isPlayerOne() && this.props.data.resultOne) {
      return this.props.data.resultOne[key];
    }
    if (this.privilage.isPlayerTwo() && this.props.data.resultTwo) {
      return this.props.data.resultTwo[key];
    }
  };

  handleScoreChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  getExpandedRow(row) {
    return (
      <div>
        <div>
          <strong>Content</strong>: {row.content}
        </div>
      </div>
    );
  }

  setAdvancingPlayer = (player, isPlayerOne) => {
    if (isPlayerOne) {
      this.setState({ playerOne: player });
    } else {
      this.setState({ playerTwo: player });
    }
    this.forceUpdate();
  };

  getConflictetailPlayerOne = () => {
    return (
      <Popover id="popover-trigger-hover-focus" title="Accept?">
        <div>
          <strong>{this.state.playerOne.username}: </strong>{" "}
          {this.props.data.resultOne.scoreOne}
        </div>
        <div>
          <strong>{this.state.playerTwo.username}: </strong>{" "}
          {this.props.data.resultOne.scoreTwo}
        </div>
      </Popover>
    );
  };

  getConflictetailPlayerTwo = () => {
    return (
      <Popover id="popover-trigger-hover-focus" title="Accept?">
        <div>
          <strong>{this.state.playerOne.username}: </strong>{" "}
          {this.props.data.resultTwo.scoreOne}
        </div>
        <div>
          <strong>{this.state.playerTwo.username}: </strong>{" "}
          {this.props.data.resultTwo.scoreTwo}
        </div>
      </Popover>
    );
  };

  resolveConflict = result => {
    this.setState(
      {
        scoreOne: this.props.data[result].scoreOne,
        scoreTwo: this.props.data[result].scoreTwo
      },
      this.handleSubmit
    );
  };

  render() {
    return (
      <Panel
        onBlur={() => this.handleSubmit()}
        style={this.getStatusColor()}
        className="sw-match"
      >
        <Form horizontal>
          <FormGroup
            controlId="scoreOne"
            validationState={this.getValidationState("scoreOne")}
            className={this.playerOneIsWinner()}
          >
            <Col
              componentClass={ControlLabel}
              className={this.state.playerOne ? "" : "tbd"}
              lg={12}
            >
              {this.state.playerOne ? this.state.playerOne.username : "TBD"}
              {this.state.status === "CONFLICT" &&
              this.privilage.isOrganizer() ? (
                <OverlayTrigger
                  trigger={["hover", "focus"]}
                  placement="right"
                  overlay={this.getConflictetailPlayerOne()}
                >
                  <Glyphicon
                    onClick={() => this.resolveConflict("resultOne")}
                    className="conflict-detail"
                    glyph="ok"
                  />
                </OverlayTrigger>
              ) : (
                ""
              )}
            </Col>
            <Col className="sw-match-input" lg={12}>
              <FormControl
                disabled={
                  !this.hasPermission() ||
                  !this.hasBothPlayers() ||
                  (this.state.status === "OK" && !this.privilage.isOrganizer())
                }
                type="text"
                placeholder={
                  this.state.status === "CONFLICT" ? "Conflict" : "Score"
                }
                onChange={this.handleScoreChange}
                value={this.state.scoreOne}
              />
            </Col>
          </FormGroup>
          <FormGroup
            controlId="scoreTwo"
            validationState={this.getValidationState("scoreTwo")}
            className={this.playerTwoIsWinner()}
          >
            <Col
              componentClass={ControlLabel}
              className={this.state.playerTwo ? "" : "tbd"}
              lg={12}
            >
              {this.state.playerTwo ? this.state.playerTwo.username : "TBD"}
              {this.state.status === "CONFLICT" &&
              this.privilage.isOrganizer() ? (
                <OverlayTrigger
                  trigger={["hover", "focus"]}
                  placement="right"
                  overlay={this.getConflictetailPlayerTwo()}
                >
                  <Glyphicon
                    onClick={() => this.resolveConflict("resultTwo")}
                    className="conflict-detail"
                    glyph="ok"
                  />
                </OverlayTrigger>
              ) : (
                ""
              )}
            </Col>
            <Col className="sw-match-input" lg={12}>
              <FormControl
                disabled={
                  !this.hasPermission() ||
                  !this.hasBothPlayers() ||
                  (this.state.status === "OK" && !this.privilage.isOrganizer())
                }
                type="text"
                placeholder={
                  this.state.status === "CONFLICT" ? "Conflict" : "Score"
                }
                onChange={this.handleScoreChange}
                value={this.state.scoreTwo}
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col className="sw-match-detail">
              <a onClick={this.handleShow}>Details</a>
            </Col>
          </FormGroup>
        </Form>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.notes.map(note => {
              return (
                <div>
                  <strong>{note.author.username}:</strong>

                  {" " + note.content}
                </div>
              );
            })}

            <Form>
              <FormGroup controlId="Notes">
                <ControlLabel>
                  <h4>Notes</h4>
                </ControlLabel>
                <FormControl
                  componentClass="textarea"
                  onChange={this.handleNotes}
                  value={this.state.newNote}
                  placeholder="Add notes"
                  disabled={!this.hasPermission()}
                />
                {this.hasPermission() ? (
                  <Button onClick={this.submitNotes}>Add notes</Button>
                ) : (
                  ""
                )}
              </FormGroup>
            </Form>
          </Modal.Body>
        </Modal>
      </Panel>
    );
  }
  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = () => {
    this.setState({ show: true });
  };
  submitNotes = e => {
    const note = {
      content: this.state.newNote
    };

    Ajax.post("rest/match/" + this.state.id + "/addNote", note).end(
      function(body, resp) {
        if (resp.status === 200) {
          let n = this.state.notes;
          n.push(body);
          this.setState({ notes: n, newNote: "" });
        }
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
  };

  handleNotes = e => {
    this.setState({ newNote: e.target.value });
  };
}
