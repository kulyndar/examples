import React from "react";
import ReactDOM from "react-dom";
import {MemoryRouter} from "react-router-dom";
import App from "./components/App";
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css'; // for expandable table needed
// require('./node_modules/bootstrap/dist/css/bootstrap.css'); /*Nezobrazuke se spravne react bootstrap :( */
import './css/spiderweb.css';

ReactDOM.render(<MemoryRouter><App /></MemoryRouter>, document.getElementById("content"));
