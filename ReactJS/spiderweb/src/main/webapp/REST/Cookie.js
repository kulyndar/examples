class Cookie {
  constructor() {}
  getCookie(name) {
    var matches = document.cookie.match(
      new RegExp(
        "(?:^|; )" +
          name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") +
          "=([^;]*)"
      )
    );
    return matches ? matches[1] : undefined;
  }
  setCookie(name, data) {
    document.cookie = name + "=" + data;
  }
  deleteCookie(name) {
    document.cookie = name + "=";
  }
}
const INSTANCE = new Cookie();

export default INSTANCE;
