/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

export default class Result {
  constructor(data) {
    if (data) {
      this.id = data.id;
      this.scoreOne = data.scoreOne;
      this.scoreTwo = data.scoreTwo;
      //TODO get from data once BE submit result is done
      this.author = { username: "dandan123" };
    }
  }

  isAuthor() {
    return window.localStorage.getItem("username") === this.author;
  }

  getScoreOne() {
    return this.scoreOne ? this.scoreOne : 0;
  }

  setScoreOne(score) {
    this.scoreOne = score;
  }

  getScoreTwo() {
    return this.scoreTwo ? this.scoreTwo : 0;
  }

  setScoreTwo(score) {
    this.scoreTwo = score;
  }

  isFilled() {
    if (this.id) {
      return true;
    }
    return false;
  }

  scoreEquals(other) {
    return (
      other.isFilled() &&
      this.isFilled() &&
      this.scoreOne === other.scoreOne &&
      this.scoreTwo === other.scoreTwo
    );
  }
}
