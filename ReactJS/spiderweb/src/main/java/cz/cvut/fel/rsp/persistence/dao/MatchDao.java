package cz.cvut.fel.rsp.persistence.dao;

import cz.cvut.fel.rsp.model.Match;
import org.springframework.stereotype.Repository;

@Repository
public class MatchDao extends BaseDao<Match> {
    protected MatchDao() {
        super(Match.class);
    }
}
