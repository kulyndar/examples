package cz.cvut.fel.rsp.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class FilterDto {

    boolean participant;

    boolean organizer;

    Date date;
}
