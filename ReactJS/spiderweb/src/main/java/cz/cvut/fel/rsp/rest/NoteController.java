/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.model.Note;
import cz.cvut.fel.rsp.model.Result;
import cz.cvut.fel.rsp.service.repository.MatchService;
import cz.cvut.fel.rsp.service.repository.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Dan Nguyen
 */
@RestController
@RequestMapping("/note")
public class NoteController {

    private final MatchService matchService;
    private final NoteService noteService;

    @Autowired
    public NoteController(MatchService matchService, NoteService noteService) {
        this.matchService = matchService;
        this.noteService = noteService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/")
    public void add(@PathVariable Integer matchId, @RequestBody Note note) {
        matchService.addNote(matchId, note);
    }

}
