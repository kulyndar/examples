package cz.cvut.fel.rsp.rest.mapper;

import cz.cvut.fel.rsp.dto.UserDto;
import cz.cvut.fel.rsp.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class DtoMapper {

    public abstract User personDtoToPerson(UserDto dto);
}
