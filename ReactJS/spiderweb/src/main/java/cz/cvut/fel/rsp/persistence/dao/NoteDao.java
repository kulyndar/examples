/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.rsp.persistence.dao;

import cz.cvut.fel.rsp.model.Note;
import cz.cvut.fel.rsp.model.Result;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Dan Nguyen
 */
@Repository
public class NoteDao extends BaseDao<Note> {

    public NoteDao() {
        super(Note.class);
    }

}
