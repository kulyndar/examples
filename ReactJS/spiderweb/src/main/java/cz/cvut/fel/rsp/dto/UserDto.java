package cz.cvut.fel.rsp.dto;

import cz.cvut.fel.rsp.model.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto extends User {

    private String passwordOriginal;
}
