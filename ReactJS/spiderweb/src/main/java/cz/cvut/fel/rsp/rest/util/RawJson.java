package cz.cvut.fel.rsp.rest.util;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Represents raw JSON value, which should be returned by JSON serialization as is, without any processing.
 */
public class RawJson {

    private String value;

    public RawJson() {
    }

    public RawJson(String value) {
        this.value = value;
    }

    @JsonValue
    @JsonRawValue
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RawJson)) return false;

        RawJson rawJson = (RawJson) o;

        return value != null ? value.equals(rawJson.value) : rawJson.value == null;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }
}
