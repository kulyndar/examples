package cz.cvut.fel.rsp.service.repository;

import cz.cvut.fel.rsp.exception.AuthorizationException;
import cz.cvut.fel.rsp.exception.UsernameExistsException;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.persistence.dao.GenericDao;
import cz.cvut.fel.rsp.persistence.dao.UserDao;
import cz.cvut.fel.rsp.service.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class PersonService extends AbstractRepositoryService<User> {

    private final UserDao dao;

    private final PasswordEncoder passwordEncoder;

    private final SecurityUtils securityUtils;

    @Autowired
    public PersonService(UserDao dao, PasswordEncoder passwordEncoder, SecurityUtils securityUtils) {
        this.dao = dao;
        this.passwordEncoder = passwordEncoder;
        this.securityUtils = securityUtils;
    }

    @Override
    protected GenericDao<User> getPrimaryDao() {
        return dao;
    }

    @Override
    void prePersist(User instance) {
        Objects.requireNonNull(instance);
        Objects.requireNonNull(instance.getUsername());
        Objects.requireNonNull(instance.getPassword());

        verifyUniqueUsername(instance);
        instance.encodePassword(passwordEncoder);
    }

    @Override
    void preUpdate(User instance) {
        final User current = securityUtils.getCurrentUser();
        if (!current.getId().equals(instance.getId())) {
            throw new AuthorizationException("Modifying other user\'s account is forbidden.");
        }
        verifyUniqueUsername(instance);
        if (instance.getPassword() != null) {
            instance.encodePassword(passwordEncoder);
        } else {
            instance.setPassword(current.getPassword());
        }
    }

    @Override
    void postUpdate() {
        securityUtils.updateCurrentUser();
    }

    private void verifyUniqueUsername(User update) {
        final User existing = dao.findByUsername(update.getUsername());
        if (existing != null && !existing.getId().equals(update.getId())) {
            throw new UsernameExistsException("Username " + update.getUsername() + " already exists.");
        }
    }


    public User findByUsername(String username){
        return dao.findByUsername(username);
    }


    /**
     * Checks whether an instance with the specified username exists.
     *
     * @param username The username to search for
     * @return Record existence status
     */
    public boolean exists(String username) {
        Objects.requireNonNull(username);
        return dao.exists(username);
    }

    public List<User> findUserByString(String searchString) {
        return dao.findUserByString(searchString);
    }
}
