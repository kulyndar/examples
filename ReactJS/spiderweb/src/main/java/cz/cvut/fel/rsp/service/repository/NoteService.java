/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.rsp.service.repository;

import cz.cvut.fel.rsp.model.Match;
import cz.cvut.fel.rsp.model.Note;
import cz.cvut.fel.rsp.persistence.dao.GenericDao;
import cz.cvut.fel.rsp.persistence.dao.NoteDao;
import cz.cvut.fel.rsp.service.security.SecurityUtils;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Dan Nguyen
 */
@Service
public class NoteService extends AbstractRepositoryService<Note> {

    private SecurityUtils securityUtils;
    private NoteDao noteDao;

    @Autowired
    public NoteService(SecurityUtils securityUtils, NoteDao noteDao) {

        this.securityUtils = securityUtils;
        this.noteDao = noteDao;

    }

    @Override
    void prePersist(Note instance) {
        Objects.requireNonNull(instance.getAuthor());
        Objects.requireNonNull(instance.getDate());
        Objects.requireNonNull(instance.getContent());
    }
    
    
    
    @Override
    protected GenericDao<Note> getPrimaryDao() {
        return noteDao;
    }
}
