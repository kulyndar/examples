import React from "react";
import Game from "./Game.js";
import DBManager from "./game-components/Manager";


/*global variable with results*/
var results=[];
/*class that shows list of results from IndexedDB*/
export default class ResultList extends React.Component {
    constructor() {
      super();
      this.state = {
        results: [],
        waiting: true
      }
    }
    /*Gets all players from IndexedDB. Is made with timeout to give DB time to load*/
    componentDidMount = () => {
      setTimeout(()=>DBManager.getAll(this.getAllUsers, this.endCallback),1000);
    }
    /*Callback function*/
    getAllUsers = (user) => {
      results.push(user);

    }
    /*Callback function, that detecs, that all players were loaded*/
    endCallback = () => {
      this.setState({
        waiting: false,
        results
      });
      results=[];
    }

    /*Shows loader if players loading is in process, otherwise list of results*/
    render() {

      if (this.state.waiting) {
        if(isEdge){
          return <div className="loader-edge"></div>
        }
        return <img className="loader" src="/svg/spinner.svg" />;
      } else {
        var res = this.state.results;
        res.sort((a,b)=>{return a.score-b.score});
        res.reverse();
        const list = res.map((r) => <div className="res-row" key={r.username}>
          <div className="res-cell image"><span><img src={r.image} /></span></div>
          <div className="res-cell username"><div>{r.username}</div></div>
          <div className="res-cell score"><div>{r.score}</div></div>
        </div>);
          return <div className={"resultList"+(isEdge?" noSVG":"")}>{list}</div>;
        }

      }


    }
