import React from "react";
/*Tile respresentation*/
export default class Tile extends React.Component {
  constructor(props) {
    super(props);
    //props: value, direction, new
  }
  render() {
    var diss = this.props.direction?this.props.direction:"";
    var newTile = this.props.new?" new ":"";
    return (<div className={this.mapValue() + " tile "+diss+newTile}>{this.props.value}</div>);
  }
  /*Values to css class*/
  mapValue = () => {
    let map = {
      2: "pow1",
      4: "pow2",
      8: "pow3",
      16: "pow4",
      32: "pow5",
      64: "pow6",
      128: "pow7",
      256: "pow8",
      512: "pow9",
      1024: "pow10",
      2048: "pow11"
    }
    return map[this.props.value];
  }


}
