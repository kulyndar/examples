
var db;
/*Class that works with IndexedDB*/
class Manager{
  constructor(){
    this.openDB();
  }
  openDB=()=>{
    /*creates or openes scheme db SCORES*/
    var request = window.indexedDB.open("scores", 1);

    request.onerror = function(event) {
       console.log("error: ");
    };

    request.onsuccess = function(event) {
       db = request.result;
       console.log("success: "+ db);
    };
    request.onupgradeneeded = function(event) {
       db = event.target.result;
       /*Key is unique username*/
       var objectStore = db.createObjectStore("score", {keyPath: "username"});
    }

  }
  /*add or updates user in DB*/
  write=(user)=>{
    var request = db.transaction(["score"], "readwrite")
    .objectStore("score")
    .put(user);

    request.onsuccess = function(event) {

    };

    request.onerror = function(event) {

    }
  }
  /*Gets all users from DB*/
  getAll=(callback, end)=>{
    var objectStore = db.transaction("score").objectStore("score");

    objectStore.openCursor().onsuccess = function(event) {
       var cursor = event.target.result;

       if (cursor) {
          callback(cursor.value);
          cursor.continue();
       } else {
          end();
       }
    };
  }
  /*Gets user from DB*/
  getUser=(username, callback)=>{
    var transaction = db.transaction(["score"]);
    var objectStore = transaction.objectStore("score");
    var request = objectStore.get(username);

    request.onerror = function(event) {

    };

    request.onsuccess = function(event) {
       // Do something with the request.result!
       if(request.result) {
         callback(request.result);

       } else {
         callback(null);
       }
    };
  }
  /*Deletes user from DB*/
  delete=(username)=>{
    var request = db.transaction(["score"], "readwrite")
    .objectStore("score")
    .delete(username);

    request.onsuccess = function(event) {

    };
  }
}

const INSTANCE = new Manager();
export default INSTANCE;
