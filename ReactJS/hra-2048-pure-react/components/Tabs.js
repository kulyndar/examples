import React from "react";
import {
  count
} from "./Home.js";
import swal from "sweetalert";
import Link from "react-router-dom";

/*Tabs on the home page*/
export default class InputContent extends React.Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  render() {
    /*First tab content*/
    const firstTab = (<div className="input">
      <label htmlFor="username">Enter your name/nickname</label>
      <input type="text" id="username" placeholder="Name" autoFocus onChange={this.props.changeHandler} onKeyDown={this.handleSubmitFirst} />

    </div>);
    /*Second tab content*/
    const secondTab = (<div className="image-select">
      <h2>Choose image: </h2>

      <img src="/avatar/avatar-beard.png" className="avatar-chooser beard chosen" onClick={this.props.imageHandler} onDoubleClick={this.handleSubmitSecondNext} />
      <img src="/avatar/avatar-girl.png" className="avatar-chooser girl" onClick={this.props.imageHandler} onDoubleClick={this.handleSubmitSecondNext} />
      <img src="/avatar/avatar-king.png" className="avatar-chooser king" onClick={this.props.imageHandler} onDoubleClick={this.handleSubmitSecondNext} />
      <img src="/avatar/avatar-office.png" className="avatar-chooser office" onClick={this.props.imageHandler} onDoubleClick={this.handleSubmitSecondNext} />
      <img src="/avatar/avatar-old.png" className="avatar-chooser old" onClick={this.props.imageHandler} onDoubleClick={this.handleSubmitSecondNext} />
      <img src="/avatar/avatar-pilot.png" className="avatar-chooser pilot" onClick={this.props.imageHandler} onDoubleClick={this.handleSubmitSecondNext} />
      <img src="/avatar/avatar-student.png" className="avatar-chooser student" onClick={this.props.imageHandler} onDoubleClick={this.handleSubmitSecondNext} />
      <img src="/avatar/avatar-sw.png" className="avatar-chooser sw" onClick={this.props.imageHandler} onDoubleClick={this.handleSubmitSecondNext} />
      <img src="/avatar/avatar-wonderwoman.png" className="avatar-chooser wonderwoman" onClick={this.props.imageHandler} onDoubleClick={this.handleSubmitSecondNext} />

    </div>);
    /*Third tab content*/
    const thirdTab = (
      <div className="play">
        <button className="play-button" onClick={this.props.submit}>Play{"   "}<span className="glyphicon glyphicon-play" aria-hidden="true"></span></button>

        </div>
    );
    const content = (<div className="tabs">
      <div>

        <ul className="links">
          <span className="glyphicon glyphicon-chevron-left prev disable" onClick={this.handleNextorPrev} aria-hidden="true"></span>
          <li className="first link activeTab"><a href="#" onClick={this.handleClick}>1</a></li>
          <li className="second link"><a href="#" onClick={this.handleClick}>2</a></li>
          <li className="third link"><a href="#" onClick={this.handleClick}>3</a></li>
          <span className="glyphicon glyphicon-chevron-right next" onClick={this.handleNextorPrev} aria-hidden="true"></span>
        </ul>

      </div>
      <div className="tab">
        <div className="first display">{firstTab}</div>
        <div className="second">{secondTab}</div>
        <div className="third">{thirdTab}</div>
      </div>

    </div>);
    return content;
  }

  /*Autofocus*/
  componentDidMount() {
    var input = document.querySelector("#username");
    if (input) {
      setTimeout(() => input.focus(), 100);
    }

  }
  /*Next or prev click*/
  handleNextorPrev = (e) => {
    var arrow = e.currentTarget;
    var dir = arrow.classList[2];
    var active = null;
    for (var i = 0; i < arrow.parentNode.childNodes.length; i++) {
      if (arrow.parentNode.childNodes[i].classList.contains("activeTab")) {
        active = arrow.parentNode.childNodes[i];
        break;
      }
    }
    switch (active.classList[0]) {
      case "first":
        if (dir == "next") {
          this.handleSubmitFirst(e);
          document.querySelector(".prev").classList.remove("disable");
        } else {
          return;
        }
        break;
      case "second":
      if (dir == "next") {
        this.handleSubmitSecondNext();
        document.querySelector(".next").classList.add("disable");
      } else {
        this.handleSubmitSecondPrev();
        document.querySelector(".prev").classList.add("disable");
      }
        break;
      case "third":
      if (dir == "next") {
      return;
      } else {
        this.handleSubmitThirdPrev();
        document.querySelector(".next").classList.remove("disable");
      }
        break;
      default:

    }
  }
  /*Next button*/
  handleSubmitThirdPrev=()=>{
    var second = document.querySelector(".second.link");
    second.classList.add("activeTab");
    second.classList.remove("visited");
    var third = document.querySelector(".third.link");
    third.classList.remove("activeTab");
    document.querySelector(".tab > .second").classList.add("display");
    document.querySelector(".tab > .third").classList.remove("display");
  }
  handleSubmitSecondNext = () => {
    var second = document.querySelector(".second.link");
    second.classList.remove("activeTab");
    second.classList.add("visited");
    var third = document.querySelector(".third.link");
    third.classList.add("activeTab");
    document.querySelector(".tab > .second").classList.remove("display");
    document.querySelector(".tab > .third").classList.add("display");
  }
  /*Prev button*/
  handleSubmitSecondPrev = () => {
    var first = document.querySelector(".first.link");
    first.classList.add("activeTab");
    first.classList.remove("visited");
    var second = document.querySelector(".second.link");
    second.classList.remove("activeTab");
    document.querySelector(".tab > .first").classList.add("display");
    document.querySelector(".tab > .second").classList.remove("display");
  }
  /*Next button*/
  handleSubmitFirst = (e) => {
    if (e.key && e.key != "Enter") {
      return;
    }
    e.preventDefault();
    e.stopPropagation();
    if (document.querySelector("#username").value == "") {
      swal({
        title: "Are you sure?",
        text: "You will play as Anonymus. Do you want to continue?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {

          var first = document.querySelector(".first.link");
          first.classList.remove("activeTab");
          first.classList.add("visited");
          var second = document.querySelector(".second.link");
          second.classList.add("activeTab");
          document.querySelector(".tab > .first").classList.remove("display");
          document.querySelector(".tab > .second").classList.add("display");
        } else {
          document.querySelector("#username").focus();
          return;
        }
      });
    } else {
      var first = document.querySelector(".first.link");
      first.classList.remove("activeTab");
      first.classList.add("visited");
      var second = document.querySelector(".second.link");
      second.classList.add("activeTab");
      document.querySelector(".tab > .first").classList.remove("display");
      document.querySelector(".tab > .second").classList.add("display");
    }
  }
  /*Switch between tabs with links*/
  handleClick(e) {
    e.preventDefault();
    /*find target tab*/

    const to = e.target.parentNode.classList[0];
    document.querySelector(".display").classList.remove("display");
    /*changes for animation*/
    if (document.querySelector(".activeTab")) {
      document.querySelector(".activeTab").classList.remove("activeTab");
    }
    let current = e.target.parentNode;
    current.classList.add("activeTab");
    document.querySelectorAll(".visited").forEach(i => i.classList.remove("visited"));
    let previous = current.previousElementSibling;
    while (previous) {
      previous.classList.remove("activeTab");
      previous.classList.add("visited");
      previous = previous.previousElementSibling;
    }

    /*show target*/
    document.querySelector("." + to + ":not(.link)").classList.add("display");

    /*prevent Default*/
    return false;
  }

}
