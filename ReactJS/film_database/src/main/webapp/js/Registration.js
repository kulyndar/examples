import React from "react";
import {
  OverlayTrigger,
  Popover,
  Col,
  Form,
  FormGroup,
  ControlLabel,
  Button, Alert
} from "react-bootstrap";
import FormControl from "react-bootstrap/lib/FormControl";
import Ajax from "../Ajax/Ajax";
import Authentication from "../Ajax/Authentication";
import Cookie from "../Ajax/Cookie";
import { sha256 } from 'js-sha256';
import swal from 'sweetalert';

export default class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      surname: "",
      login: "",
      mail: "",
      password: "",
      confirmation: "",
      usernameStatus: null,
      passwordStatus: null,
      mailStatus:null,
      nameStatus:null,
      surnameStatus:null,
      alertShow: false
    };


  }

  /*Registration function. Sends registration data to backend*/
  registrate = () => {
    console.log("registration");
    const user = {
      name: this.state.name,
      surname: this.state.surname,
      mail: this.state.mail,
      login: this.state.login, //name like in Java
      password: sha256(this.state.password),
      role: "Guest"
    };

    //TODO map to Java
    Ajax.post("rest/users", user).end(
      function(body, resp) {
        if (resp.status === 201) {
          Authentication.login(
            user.login,
            user.password,
            this.errorCallback,
            this.callback
          );
          // this.props.history.push('/'); //Main Page
        }

      }.bind(this),
      function(err) {
        this.setState({
          alertShow: true
        });
        this.setState({
          login: ""
        });
        this.setState({
          mail: ""
        });
        this.setState({
          usernameStatus: "error"
        }); //TODO customize error
      }.bind(this)
    );
  };
  callback = () => {
    console.log("Login ok. Setting cookie and redirect");
    Cookie.setCookie("logged", "true");
    window.localStorage.setItem("username", this.state.login);
    this.props.history.push("/");
  };
  errorCallback = () => {
    console.log("Error"); //TODOerror callback
  };
  /*Validation. If everything is OK, calls registration, otherwise render error alert/popover*/
  validate = () => {
    console.log("validate");
    let ok = true;
    if (this.state.login.length < 6) {
      this.setState({
        usernameStatus: "error"
      });
      ok = false;
    }else{
      this.setState({usernameStatus:"success"});
    }
    if (this.state.password.length < 8) {
      this.setState({
        passwordStatus: "error"
      });
      ok = false;
    } else {
      this.setState({
        passwordStatus: "success"
      });
    }
    if (this.state.password != this.state.confirmation) {
      this.setState({
        passwordStatus: "error"
      });
      this.setState({
        password: ""
      });
      this.setState({
        confirmation: ""
      });
      ok = false;
    }
    if(!this.state.mail||!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.mail))){
      this.setState({mailStatus:"error"});
      ok=false;

    }else{
      this.setState({mailStatus:"success"});
    }
    if(!this.state.name){
      this.setState({nameStatus:"error"});
      ok=false;
    }else{
      this.setState({nameStatus:"success"});
    }
    if(!this.state.surname){

      this.setState({surnameStatus:"error"});
      ok=false;
    }else{
      this.setState({surnameStatus:"success"});
    }
    if (ok) {
        swal("It is ok!", "You are signed up", "ok");
      this.registrate();
    } else {
        swal("You are not signed up", "Try it again", "error");
    }
  };

  /*Handles changes in username, password and confirmation inputs*/
  handleChangeUsername = e => {
    this.setState({
      login: e.target.value
    });
    if (this.state.login.length > 4) {
      //it starts from 0
      this.setState({
        usernameStatus: "success"
      });
    } else {
      this.setState({
        usernameStatus: null
      });
    }
  };
  handleChangePassword = e => {
    this.setState({
      password: e.target.value
    });
    this.setState({
      passwordStatus: null
    });
  };
  hadndleChangeConfirm = e => {
    this.setState({
      confirmation: e.target.value
    });
    this.setState({
      passwordStatus: null
    });
  };
  handleChangeMail = e => {
    this.setState({
      mail: e.target.value
    });
  };
  handleChangeName = e => {
    this.setState({
      name: e.target.value
    });
  };
  handleChangeSurname = e => {
    this.setState({
      surname: e.target.value
    });
  };
  /*Utils to show error and tooltips*/
  getValidationState = type => {
    switch (type) {
      case "username":
        return this.state.usernameStatus;
      case "password":
      return this.state.passwordStatus;
      case "mail":
      return this.state.mailStatus;
      case "name":
      return this.state.nameStatus;
      case "surname":
      return this.state.surnameStatus;
    }

  };
  getAlert = () => {
        swal("This login or mail already exists", "Try another one please!", "error");
  }
  handleDismiss = () => {
    this.setState({
      alertShow: false
    });
  }
  getPopover = type => {
    switch (type) {
      case "username-default":
        return (
          <Popover id="popover-trigger-hover" title="Username">
            Username has to contain min 6 letters/numbers
          </Popover>
        );

        case "password-default":
        return (
          <Popover id="popover-trigger-hover" title="Password">
            Password has to contain min 8 letters/numbers
          </Popover>
        );
    }

    //TODO ADD ERROR POPOVERS (or alerts)
  };

  render() {

    return (
      <Form horizontal={true} className="forms">
        {this.state.alertShow?this.getAlert():<div></div>}
        <FormGroup
            
          controlId="name"
          validationState={this.getValidationState("name")}
        >
          <Col componentClass={ControlLabel} htmlFor="name" sm={2} md={3}>
            Name
          </Col>
          <Col sm={10} md={9}>
            <FormControl
              type="text"
              placeholder="Name"
              value={this.state.name}
              onChange={this.handleChangeName}
            />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="surname"
          validationState={this.getValidationState("surname")}
        >
          <Col componentClass={ControlLabel} htmlFor="surname" sm={2} md={3}>
            Surname
          </Col>
          <Col sm={10} md={9}>
            <FormControl
              type="surname"
              placeholder="Surname"
              value={this.state.surname}
              onChange={this.handleChangeSurname}
            />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="mail"
          validationState={this.getValidationState("mail")}
        >
          <Col componentClass={ControlLabel} htmlFor="mail" sm={2} md={3}>
            Email
          </Col>
          <Col sm={10} md={9}>
            <FormControl
              type="mail"
              placeholder="Email"
              value={this.state.mail}
              onChange={this.handleChangeMail}
            />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        
        <OverlayTrigger
          trigger="focus"
          placement="bottom"
          overlay={this.getPopover("username-default")}
        >
          <FormGroup
            controlId="username-reg"
            validationState={this.getValidationState("username")}
          >
            <Col componentClass={ControlLabel} htmlFor="username-reg" sm={2} md={3}>
              Username
            </Col>
            <Col sm={10} md={9}>
              <FormControl
                type="text"
                placeholder="Username"
                value={this.state.login}
                onChange={this.handleChangeUsername}
              />
              <FormControl.Feedback />
            </Col>
          </FormGroup>
        </OverlayTrigger>
        
        <OverlayTrigger
          trigger="focus"
          placement="bottom"
          overlay={this.getPopover("password-default")}
        >
        <FormGroup
          controlId="password-reg"
          validationState={this.getValidationState("password")}
        >
          <Col componentClass={ControlLabel} htmlFor="password-reg" sm={2} md={3}>
            Password
          </Col>
          <Col sm={10} md={9}>
            <FormControl
              type="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.handleChangePassword}
            />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        </OverlayTrigger>
        
        <FormGroup
          controlId="confirm"
          validationState={this.getValidationState("password")}
        >
          <Col componentClass={ControlLabel} htmlFor="confirm" sm={2} md={3}>
            Confirm Your Password
          </Col>
          <Col sm={10} md={9}>
            <FormControl
              type="password"
              placeholder="Confirm Your Password"
              value={this.state.confirmation}
              onChange={this.hadndleChangeConfirm}
            />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col smOffset={2} mdOffset={3} sm={8} md={10}>
            <Button id="regBut" type="submit" onClick={this.validate}>
              Sign up
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
