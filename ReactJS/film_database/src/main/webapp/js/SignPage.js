import React from "react";
import Login from "./Login"
import Registration from "./Registration";
import {Grid, Row, Col, Tab, Nav, NavItem} from "react-bootstrap";

export default class SignPage extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {

    const login = <Login history={this.props.history} />;
    const registration = <Registration history={this.props.history} />;
    const content = (
      <Grid>
        <Row>

          <Col md={6} sm={8} xs={12}>
            <Tab.Container id="sign" defaultActiveKey="first">
              <div>
                <Row className="clearfix tabs-content">

                  <Col sm={12} xs={12} md={12}>
                    <Tab.Content animation>
                      <Tab.Pane eventKey="first">
                        <h1>Sign in</h1>{login}</Tab.Pane>
                      <Tab.Pane  eventKey="second">
                        <h1>Sign up</h1>{registration}</Tab.Pane>
                    </Tab.Content>
                  </Col>


                </Row>
                <Row className="rowPills">
                  <Col sm={12} md={12}>
                    <Nav justified bsStyle="pills">
                      <NavItem className="trigger" eventKey="first" >Already have an account? Sign in</NavItem>
                      <NavItem className="trigger" eventKey="second">Create new account</NavItem>
                    </Nav>
                  </Col>
                </Row>
              </div>
            </Tab.Container>

          </Col>
        </Row>
    </Grid>);
    return <div className="sign-content">{content}</div>;

  }
}
