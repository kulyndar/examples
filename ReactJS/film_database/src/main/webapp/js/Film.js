import React from "react";
import Ajax from "../Ajax/Ajax";
import Cookie from "../Ajax/Cookie";
import {
  Grid,
  Row,
  Col,
  ControlLabel,
  Button,
  FormGroup,
  FormControl,
  Panel,
  Alert
} from "react-bootstrap";
import swal from 'sweetalert';

export default class Film extends React.Component {

  constructor(props) {
    super(props); //filmID
    this.state = {
      film: null,
      comment: "",
      user: null,
      rating: 0,
      favorite: false,
      commentAlert: false,
      ratingAlert: false


    }
    this.search();
    console.log(this.props.match.params.id);
  }
  countRating = (data) => {
    if (data.ratings.length) {
      let rat = 0;
      for (var i = 0; i < data.ratings.length; i++) {
        rat += data.ratings[i].stars;
      }
      rat = Math.round(rat / data.ratings.length);
      console.log("RATING: " + rat);
      this.setState({
        rating: rat
      });
    }
  }
  search = () => {
    Ajax.get("rest/films/" + this.props.match.params.id).end(
      (resp) => {
        if (!resp) {
          this.search();
        } else {
          this.countRating(resp);
          this.inFavorite();
          resp.comments.reverse();
          this.setState({
            film: resp
          });
        }
      }
    );
  }
  inFavorite = () => {

    Ajax.get("rest/users/current").end(
      (resp) => {
        this.setState({
          user: resp
        });
        let data = this.state.user.favoriteFilms;
        let inF = false;
        for (var i = 0; i < data.length; i++) {
          if (data[i].filmId == this.props.match.params.id) {
            inF = true;
          }
        }
        this.setState({
          favorite: inF
        });
      }
    );


  }
  handleDismiss = () => {
    this.setState({
      commentAlert: false
    });
  }
  getCommentAlert() {
    swal("Too long comment", "We are sorry, but max length of comment is 2000 characters!", "error");
  }

  render() {
    if (this.state.film) {
      const heart = (<svg className={"heart "+(this.state.favorite?"favorite":"")} onClick={this.handleFavorite} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 4.419c-2.826-5.695-11.999-4.064-11.999 3.27 0 7.27 9.903 10.938 11.999 15.311 2.096-4.373 12-8.041 12-15.311 0-7.327-9.17-8.972-12-3.27z"/></svg>);
      const stars = (
        <div className="stars" data-stars={this.state.rating}>
	<svg height="25" width="23" className="star rating" data-rating="1" onClick={this.handleRating}>
    <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" />
  </svg>
  <svg height="25" width="23" className="star rating" data-rating="2" onClick={this.handleRating}>
    <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" />
  </svg>
  <svg height="25" width="23" className="star rating" data-rating="3" onClick={this.handleRating}>
    <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" />
  </svg>
  <svg height="25" width="23" className="star rating" data-rating="4" onClick={this.handleRating}>
    <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" />
  </svg>
  <svg height="25" width="23" className="star rating" data-rating="5" onClick={this.handleRating}>
    <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" />
  </svg>
</div>
      );
      const image = this.state.film.logo ? <img className="poster" src={"http://image.tmdb.org/t/p/w185/"+this.state.film.logo} /> : <img className="poster" src="imgNotFound.jpg" />;
      const date = this.state.film.date.split("-");
      const d = date[2] + "." + date[1] + "." + date[0];
      const film = (
        <Grid>
          <Row className="show-grid">
            <Col sm={6} md={3}>
              {image}
            </Col>
            <Col sm={6} md={9}>
              <h1 className="film-header">{this.state.film.name+"     "}</h1>{Cookie.getCookie("logged")=="true"?heart:<div></div>}
              <br />
              {this.getRatingAlert()}
              {stars}

              <br />
              <br />
              <p><b>Countries: </b>{this.state.film.countries.substring(0,this.state.film.countries.length-2 )}</p>
              <p><b>Release date: </b>{d}</p>
              <p><b>Genres: </b>{this.state.film.genre.substring(0,this.state.film.genre.length-2 )}</p>
              <article>{this.state.film.info}</article>
              <br />
            </Col>
          </Row>
          <Row>
            {this.state.commentAlert? this.getCommentAlert():<div className="addComment"></div>}
          {this.getInputArea()}
        </Row>
        <Row className="comments">
          {this.getComments()}
        </Row>
      </Grid>
      );

      if (Cookie.getCookie("logged") == true) {
        /*with comments, rationg and add to favorite*/
        return film;
      } else {
        /*without comments, rating and add to favorite*/
        return film;
      }
    } else {
      return <div id="loader"></div>;
    }

  }
  getInputArea = () => {
    if (Cookie.getCookie("logged") == "true") {
      return (<FormGroup id="addCom" controlId="comments">
        <ControlLabel>Add comment:</ControlLabel>
        <FormControl componentClass="textarea" value={this.state.comment} placeholder="Start typing..."
          onChange={this.handleChangeTextarea} />
    <Button id="commBut" bsStyle="success" onClick={this.addComment}>Add comment</Button>
    </FormGroup>);
    } else {
      return <div></div>
    }
  }

  getComments = () => {
    if (this.state.film.comments.length) {
      // this.state.film.comments.reverse();
      const com = this.state.film.comments.map((data) => <Panel key={data.commentId}>
    <Panel.Heading><b className="user-comment">{data.personName+" "+data.personSurname+"   "}</b><i>{this.getDate(data.date)}</i></Panel.Heading>
    <Panel.Body>{data.text}</Panel.Body>
  </Panel>);
      return <div className="comment-content">{com}</div>
    } else {
      if (Cookie.getCookie("logged") == "true") {
        return (
          <div className="comment-alert"><strong>No comments</strong> here. Be the first!</div>
        );
      } else {
        return (
          <div className="comment-alert"><strong>No comments</strong> here. Login to write a comment</div>
        )
      }

    }
  }
  getDate = (value) => {
    let date = new Date(value);
    return date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
  }

  handleChangeTextarea = (e) => {
    if (e.target.value.length > 1999) {
      this.setState({
        commentAlert: true
      });
      return;
    }
    this.setState({
      comment: e.target.value
    });
  }
  addComment = () => {
    if (this.state.comment.length == 0) {
      console.log("Enpty");
      return;
    }
    if (this.state.user) {
      const comment = {
        personName: this.state.user.name,
        personSurname: this.state.user.surname,
        date: new Date(),
        text: this.state.comment,
        filmId: this.props.match.params.id
      }
      Ajax.post("rest/films/comment", comment).end(
        () => {
          console.log("Super");
          this.setState({
            comment: ""
          });
          this.search();
        },
        () => {
          console.log("Bad")
        }
      )
    } else {
      this.findUser();
    }
  }
  findUser = () => {
    Ajax.get("rest/users/current").end(
      (resp) => {
        this.setState({
          user: resp
        })
        this.addComment();
      }
    )
  }
  handleRating = (e) => {
    if (Cookie.getCookie("logged") != "true") {
      this.setState({
        ratingAlert: true
      });
      return;
    }
    for (var i = 0; i < this.state.film.ratings.length; i++) {
      if(this.state.film.ratings[i].personLogin==localStorage.getItem("username")){

              swal("You have already rated this film", "Unfortunately, you can rate the film only once.", "error");
          return;
      }
    }
    const rate = e.currentTarget.getAttribute("data-rating");
    console.log(rate);

    this.recountRating(rate);
    const rating = {
      filmId: this.props.match.params.id,
      stars: rate,
      personLogin:localStorage.getItem("username")
    }
    Ajax.post("rest/films/rating", rating).end(
      () => {
        console.log("Super")
        this.search();
      },
      () => {
        console.log("Bad")
      }
    );

  }
  getRatingAlert = () => {
    if (this.state.ratingAlert) {
      swal("You are not logged in", "Please, login to rate the film!", "error");
    }

  }
  handleFavorite = (e) => {
    console.log("FAVORITE");
    if (this.state.favorite) {
      return;
    }
    this.setState({
      favorite: true
    });
    Ajax.post("rest/films/favorite/" + this.state.user.userId, this.state.film.filmId).end(
      () => {
        console.log("Super");
        this.search();
      },
      () => {
        console.log("Bad")
      }
    )

  }
  handleDismissRating = () => {
    this.setState({
      ratingAlert: false
    });
  }
  recountRating = (rate) => {
    console.log("RERATE");
    let rat = 0;
    for (var i = 0; i < this.state.film.ratings.length; i++) {
      rat += this.state.film.ratings[i].stars;
    }
    console.log(rat)
    rat = rat + parseInt(rate);
    console.log(rat)
    console.log(this.state.film.ratings.length);
    rat = Math.round(rat / (this.state.film.ratings.length + 1));
    console.log("RATING: " + rat);
    this.setState({
      rating: rat
    });
  }


}
