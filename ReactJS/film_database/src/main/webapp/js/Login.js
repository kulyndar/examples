import React from "react";
import Authentication from "../Ajax/Authentication.js";
import {
  Alert,
  Col,
  Form,
  FormGroup,
  ControlLabel,
  Button
} from "react-bootstrap";
import FormControl from "react-bootstrap/lib/FormControl";
import Cookie from "../Ajax/Cookie";
import { sha256 } from 'js-sha256';
import swal from 'sweetalert';

export default class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      empty: "false",
      error: false
    };
  }
  /*Handlers of Username and password inputs*/
  onChangeUsername = e => {
    this.setState({ username: e.target.value });
    this.setState({ empty: "false" });
  };
  onChangePassword = e => {
    this.setState({ password: e.target.value });
    this.setState({ empty: "false" });
  };

  /*Login function, sends data to backend*/
  login = e => {
    e.preventDefault();
    if (this.state.username != "" && this.state.password != "") {
      Authentication.login(
        this.state.username,
        sha256(this.state.password),
        this.errorCallback,
        this.callback
      );
    } else {
      this.setState({ empty: "true" });
    }
  };
  callback = () => {
    console.log("Login ok. Setting cookie and redirect");
    Cookie.setCookie("logged", "true");
    window.localStorage.setItem("username", this.state.username);
    this.props.history.push("/");
  };
  /*Function is callback from login()*/
  errorCallback = () => {
    this.setState({ error: true });
  };

  /*Closes alert*/
  handleDismiss = () => {
    this.setState({ error: false });
  };
  /*Shos alert*/
  getAlert = () => {
      swal("Oh snap! You got an error!", "Wrong username or password", "error");
  };

  /*Util to show error*/
  getValidationState = () => {
    if (this.state.empty == "false") {
      return null;
    } else {
      return "error";
    }
  };

  render() {
    const form = (
      <Form horizontal className="forms">
        <FormGroup
          controlId="username"
          validationState={this.getValidationState()}
        >
          <Col className="colL" componentClass={ControlLabel} htmlFor="username" sm={2} md={3}>
            Username
          </Col>
          <Col sm={10} xs={12} md={9}>
            <FormControl
              type="text"
              placeholder="Username"
              value={this.state.username}
              onChange={this.onChangeUsername}
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="password"
          validationState={this.getValidationState()}
        >
          <Col componentClass={ControlLabel} htmlFor="password" sm={2} md={3}>
            Password
          </Col>
          <Col sm={10} xs={12} md={9}>
            <FormControl
              type="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.onChangePassword}
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col smOffset={2} mdOffset={3} sm={10} md={10} xs={12} >
            <Button id="logBut"type="submit" onClick={this.login}>
              Sign in
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
    if (this.state.error) {
      return (
        <div>
          {this.getAlert()}
          {form}
        </div>
      );
    }
    return form;
  }
}
