import React from "react";
import {
Alert,
        Col,
        Form,
        FormGroup,
        ControlLabel,
        Button,
        Grid,
        Row
        } from "react-bootstrap";
import Ajax from "../Ajax/Ajax";
import FormControl from "react-bootstrap/lib/FormControl";
import Cookie from "../Ajax/Cookie";

export default class FilmList extends React.Component {
    //props
    constructor() {
        super();
        this.state = {
            name: "",
            films: [],
            loaded: false
        };
        this.search();
    }

    search = () => {
        if (localStorage.getItem("search")) {
            Ajax.get('rest/films/name?name=' + localStorage.getItem("search")).end((data) => {
                this.setState({
                    films: data
                });
                this.setState({
                    loaded: true
                });
            });
        }
    }

    createLayout = () => {
        const filmlist = this.state.films.map((data) => <Row className="listFilms" key={data.id}>
        <Col md={4} sm={4}>
        <img className="list-poster" src={data.poster_path ? "http://image.tmdb.org/t/p/w185/" + data.poster_path : "imgNotFound.jpg"} />
        </Col>
        <Col md={6} sm={6} className="review">
        <h2 className="title">{data.title}</h2>
        <p className="overview">{data.overview}</p>
        <Button className="searchListBut" bsStyle="primary" id={data.id} onClick={this.showFilm} >More</Button>
        </Col>
    </Row>);
        return filmlist;
    }
    showFilm = (e) => {
        e.preventDefault();
        this.props.history.push("/film/" + e.target.id);
    }
    render() {
        const form = (<div className="movieList">
        
            <Grid>{this.createLayout()}</Grid>
        
        </div>);
        if (this.state.loaded) {
            if (!this.state.films.length) {
                return (
                    <div className="comment-alert"><strong>We are sorry, but no films were found for your request {localStorage.getItem("search")}</strong> <br />
                    Please, try to change your request</div>        
                   );
            }
            return form;
        } else {
            return (<div id="loader"></div>);
        }

    }

}
