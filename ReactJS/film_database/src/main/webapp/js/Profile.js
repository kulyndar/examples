import React from "react";
import Cookie from "../Ajax/Cookie";
import Authentication from "../Ajax/Authentication";
import Ajax from "../Ajax/Ajax";
import {Alert, Grid, Row, Col,Button} from "react-bootstrap";

export default class Profile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      film: null,
      user: null
    }

    this.getUser();
  }


  render() {
    if (this.state.user) {
      if (!this.state.film) {
        console.log(this.state.film);
        return (
            <div className="comment-alert"><strong>{"Dear "+this.state.user.name+" "+this.state.user.surname+"! "}</strong> You do not have any favorite films yet.
            </div>        
        );
      } else {
        if(!this.state.film.length){
          return (
            <div className="comment-alert"><strong>{"Dear "+this.state.user.name+" "+this.state.user.surname+"! "}</strong> You do not have any favorite films yet.
            </div>        
        );
        }
        return (
          <div className="favorite-films">
            <h1>Favorite films</h1>
            <Grid>{this.createLayout()}</Grid>
          </div>
        );
      }
    } else {
      return <div id="loader"></div>;
    }
  }

  createLayout = () => {
    const filmlist = this.state.film.map((data) => <Row  className="listFilms" key={data.filmId}>
      <Col md={4} sm={4}>
        <img className="list-poster" src={data.logo? "http://image.tmdb.org/t/p/w185/"+data.logo : "imgNotFound.jpg"} />
      </Col>
      <Col md={6} sm={6}>
        <h2 className="title">{data.name}</h2>
        <p className="overview">{data.info}</p>
        <Button className="searchListBut" bsStyle="primary" id={data.filmId} onClick={this.showFilm} >More</Button>
      </Col>
      </Row>);
    return filmlist;
  }
  showFilm = (e) => {
    e.preventDefault();
    this.props.history.push("/film/" + e.target.id);
  }
  getUser = () => {
    Ajax.get("rest/users/current").end(
      (resp) => {
        this.setState({
          user: resp
        });
        this.setState({
          film: this.state.user.favoriteFilms
        });

      }, (err) => {
        Authentication.logout();
        this.props.history.push("/login");
      }
    );
  }
}
