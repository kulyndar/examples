import React, {
  Component
} from "react";
import {
  Switch,
  Route
} from "react-router-dom";
import Welcome from "./Welcome";
import Login from "./Login";
import Registration from "./Registration";
import NotFound from "./NotFound404";
import NavBar from "./NavBar";
import Cookie from "../Ajax/Cookie";
import {
  Panel
} from "react-bootstrap";
import Film from "./Film";
import FilmList from "./FilmList";
import Profile from "./Profile";
import SignPage from "./SignPage";


export default class Main extends Component {


  getRouter() {

    if (Cookie.getCookie("logged") == "true") {
      return (
        <div>

          <Route component={NavBar} />
          <Switch>

            <Route exact  path="/" component={Welcome} />
            <Route path="/search" component={FilmList} />
            <Route path="/profile" component={Profile} />
            <Route path="/film/:id" component={Film} />
            <Route component={NotFound} />
          </Switch>
        </div>
      );
    } else {
      return (

        <div>
          <Route component={NavBar} />
          <Switch>
            <Route path="/login" component={SignPage} />

            <Route exact path="/" component={Welcome} />
              <Route path="/search" component={FilmList} />
              <Route path="/profile" component={Profile} />
              <Route path="/film/:id" component={Film} />
            <Route component={NotFound} />
          </Switch>
        </div>
      );
    }
  }

  render() {
    return this.getRouter();
  }
}
