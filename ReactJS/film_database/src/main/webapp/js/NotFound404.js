import React from "react";
import { Alert, Button } from "react-bootstrap";
import { Redirect } from "react-router-dom";

export default class NotFound extends React.Component {
    constructor() {
        super();
        this.state = {
            redirect: false
        };
    }
    onClick = () => {
        console.log("redirect");
        this.setState({redirect: true});
    }
    ;
            render() {
        if (this.state.redirect) {
            return <Redirect to="/" />;
        } else {
            return (
                    <div className="comment-alert">
                        <h4>Sorry! Page was not found</h4> <p>Redirect to the main page</p> <p>
                            <Button className="searchListBut" bsStyle="primary" onClick={this.onClick}>
                                Main Page
                            </Button>
                        </p>
                    </div>
                    );
        }
    }
}
