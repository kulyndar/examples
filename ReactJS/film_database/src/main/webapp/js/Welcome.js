import React from "react";
import { Button } from "react-bootstrap";
import Cookie from "../Ajax/Cookie";

export default class Welcome extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    const content = (
      <div className="welcome">
        <h1>Welcome to film database</h1>
        <br />
        <p>
         Movie database is useful for people who want to quickly search for a movie, read what other users think of them or express the opinion.
         The best movies you no longer need to keep searching, now you can quickly add it to your list of favorite movies and open directly from your profile. 
         In our application is always up to date information about movies, even the latest ones because of the public database of films that we use. 
         The responsive design of the application allows people using even on mobile devices.           
        </p>
        <p>
          Filmová databáze je užitečná pro lidí, kteří si chtějí rychle vyhledat film, přečist, co si o něm myslí ostatní uživatelé nebo vyjadřit svůj názor. 
          Nejlepší filmy už nemusíte pořád vyhledávat, můžete je rychle přidat do seznamu oblíbených filmů a otevřit přímo ze svého profilu. 
          Díky použivání veřejné databáze filmů, v naši aplikaci je vždy aktuální informace o filmech, dokonce i o těch nejnovějších.
          Responzivní design aplikace umožňuje jeji použivání i na mobilních zařízeních. 
        </p>

      </div>
    );
    return content;
  }
}
