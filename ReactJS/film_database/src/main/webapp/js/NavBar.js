import React from "react";
import {
  Link,
  Redirect
} from "react-router-dom";
import Cookie from "../Ajax/Cookie";
import {
  Navbar,
  NavItem,
  Nav,
  Button,
  FormGroup,
  FormControl,
  ControlLabel,
  Glyphicon
} from "react-bootstrap";
import Authentication from "../Ajax/Authentication";
import FilmList from "./FilmList";
import Profile from "./Profile";
import Ajax from "../Ajax/Ajax";
//společný pro všechny komponenty, krome Welcome
export default class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ""

    }
  }
  handleSearch = (e) => {
    this.setState({
      search: e.target.value
    });

  }
  handleKeySubmit = (e) => {
    if (e.key=="Enter") {
      this.handleSubmit(e);
    }
  }
  handleSubmit = (e) => {
    console.log("Submit. Search: " + this.state.search);
    e.preventDefault();
    if (!this.state.search) {
      return;
    }
    localStorage.setItem("search", this.state.search);
    this.setState({
      search: ""
    });
    if (location.hash == "#/search") {
      location.reload();
    } else {
      this.props.history.push("/search");
    };

  }

  render() {
    let navig = (<Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <a href="#">
            <img src="nss-logo.png" />
          </a>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav>
          <NavItem href="#/">Home
            {"       "}
            <Glyphicon glyph="home" /></NavItem>
        </Nav>
        <Navbar.Form pullLeft>
        <Button className="nav-button" type="submit" onClick={this.handleSubmit} ><Glyphicon glyph="search" /></Button>
          <FormGroup>
            <ControlLabel> </ControlLabel>
            
            <FormControl componentClass="input" type="text" value={this.state.search} placeholder="Search..." onKeyPress={this.handleKeySubmit} onChange={this.handleSearch} />
          </FormGroup>{' '}
          
        </Navbar.Form>
        <Nav pullRight>
          <NavItem  href="#/login">
            Login
            {"       "}
            <Glyphicon glyph="log-in" />
          </NavItem>
        </Nav>
      </Navbar.Collapse>

    </Navbar>);


    if (Cookie.getCookie("logged") == "true") {
      navig = (<Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="#">
              <img src="nss-logo.png" />
            </a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>

        <Navbar.Collapse>
          <Nav>
            <NavItem href="#/">Home
              {"       "}
              <Glyphicon glyph="home" />
            </NavItem>
          </Nav>
          <Nav>  <NavItem  href="#/profile">
            Profile
            {"       "}
            <Glyphicon glyph="user" />
          </NavItem></Nav>
          <Navbar.Form pullLeft>
            <FormGroup>
              <FormControl type="text" placeholder="Search" value={this.state.search}  onChange={this.handleSearch} onKeyPress={this.handleKeySubmit} />
            </FormGroup>{' '}
            <Button className="nav-button" type="submit" onClick={this.handleSubmit}> <Glyphicon glyph="search" /></Button>
          </Navbar.Form>
          <Nav pullRight>
            <NavItem href="#/" onClick={Authentication.logout}>
              Logout
              {"       "}  <Glyphicon glyph="log-out" />
            </NavItem>
          </Nav>
        </Navbar.Collapse>

      </Navbar>);

    }
    return navig;

  }



}
