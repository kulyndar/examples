package service.security;

import persistence.dao.UserDao;
import model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        final Person user = userDao.findByUsername(login);
        if (user == null) {
            throw new UsernameNotFoundException("User with login " + login + " not found.");
        }
        return new security.model.UserDetails(user);
    }
}
