package service.repository;

import model.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persistence.dao.FilmDao;
import persistence.dao.GenericDao;

@Service
public class FilmService extends AbstractRepositoryService<Film>{
    
    @Autowired
    private FilmDao filmDao;

    @Override
    protected GenericDao<Film> getPrimaryDao() {
        return filmDao;
    }
    
     public Film findByNumberRoom(int filmId) {
        return filmDao.findByFilmId(filmId);
    }
}
