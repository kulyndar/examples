package service.repository;

import model.Rating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persistence.dao.GenericDao;
import persistence.dao.RatingDao;

@Service
public class RatingService extends AbstractRepositoryService<Rating>{
    
    @Autowired
    private RatingDao ratingDao;

    @Override
    protected GenericDao<Rating> getPrimaryDao() {
        return ratingDao;
    }
}
