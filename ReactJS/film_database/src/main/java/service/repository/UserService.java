package service.repository;

import exception.AuthorizationException;
import exception.UsernameExistsException;
import java.util.Objects;
import model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import persistence.dao.GenericDao;
import persistence.dao.UserDao;
import service.security.SecurityUtils;

@Service
public class UserService extends AbstractRepositoryService<Person>{
    
    private final UserDao dao;
    
    private final PasswordEncoder passwordEncoder;

    private final SecurityUtils securityUtils;
    
    @Autowired
    public UserService(UserDao dao, PasswordEncoder passwordEncoder, SecurityUtils securityUtils) {
        this.dao = dao;
        this.passwordEncoder = passwordEncoder;
        this.securityUtils = securityUtils;
    }
    
    
    @Override
    protected GenericDao<Person> getPrimaryDao() {
        return dao;
    }
    
    @Override
    void prePersist(Person instance) {
        instance.encodePassword(passwordEncoder);
    }

    @Override
    void preUpdate(Person instance) {
        final Person current = securityUtils.getCurrentUser();
        if (!current.getUserId().equals(instance.getUserId())) {
            throw new AuthorizationException("Modifying other user\'s account is forbidden.");
        }
        verifyUniqueUsername(instance);
//        if (instance.getPassword() != null) {
//            instance.encodePassword(passwordEncoder);
//        } else {
//            instance.setPassword(current.getPassword());
//        }
    }

    private void verifyUniqueUsername(Person update) {
        final Person existing = dao.findByUsername(update.getLogin());
        if (existing != null && !existing.getUserId().equals(update.getUserId())) {
            throw new UsernameExistsException("Username " + update.getLogin()+ " already exists.");
        }
    }

    @Override
    void postUpdate() {
        securityUtils.updateCurrentUser();
    }
    
     public Person findByUsername(String username) {
        return dao.findByUsername(username);
    }

    /**
     * Checks whether an instance with the specified username exists.
     *
     * @param username The username to search for
     * @return Record existence status
     */
    public boolean exists(String username) {
        Objects.requireNonNull(username);
        return dao.exists(username);
    }
}
