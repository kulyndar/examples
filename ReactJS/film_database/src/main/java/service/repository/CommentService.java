package service.repository;

import model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persistence.dao.CommentDao;
import persistence.dao.GenericDao;

@Service
public class CommentService extends AbstractRepositoryService<Comment>{
    
    @Autowired
    private CommentDao commentDao;

    @Override
    protected GenericDao<Comment> getPrimaryDao() {
        return commentDao;
    }
}
