package config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

/**
 * Root configuration file of our project - it sets up basic Spring configuration and imports additional configuration files.
 * <p>
 * It is good to separate configuration of different components of the application, because they can then be configured
 * independently for example in tests.
 */
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@Import({PersistenceConfig.class, WebAppConfig.class, ServiceConfig.class})
public class AppConfig {
}
