package security.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.*;
import model.Person;

public class UserDetails implements org.springframework.security.core.userdetails.UserDetails {

    private static final String SIMPLE_ROLE = "ROLE_USER";
    private static final String ADMIN_ROLE = "ROLE_ADMIN";


    private Person user;

    protected final Set<GrantedAuthority> authorities;

    public UserDetails(Person user) {
        Objects.requireNonNull(user);
        this.user = user;
        this.authorities = new HashSet<>();
        String role = user.getRole();
        if (role.equals("Admin")){
            addAdminRole();
        }
        if (role.equals("Guest")){
            addSimpleRole();
        }
    }

    public UserDetails(Person user, Collection<GrantedAuthority> authorities) {
        Objects.requireNonNull(user);
        Objects.requireNonNull(authorities);
        this.user = user;
        this.authorities = new HashSet<>();
        addSimpleRole();
        this.authorities.addAll(authorities);
    }

    private void addSimpleRole() {
        authorities.add(new SimpleGrantedAuthority(SIMPLE_ROLE));
    }
    private void addAdminRole() {
        authorities.add(new SimpleGrantedAuthority(ADMIN_ROLE));
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.unmodifiableCollection(authorities);
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Person getUser() {
        return user;
    }
}

