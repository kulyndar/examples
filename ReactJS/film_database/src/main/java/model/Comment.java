package model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Comment")
public class Comment {

    @Id
    @GeneratedValue
    private Integer commentId;
    
    @Basic(optional = false)
    @Column(nullable = false)
    private String personName;
    
    @Basic(optional = false)
    @Column(nullable = false)
    private String personSurname;
    
    @Basic(optional = false)
    @Column(nullable = false)
    private int filmId;

    @Basic(optional = false)
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    public java.util.Date date;

    @Basic(optional = false)
    @Column(nullable = false, length = 2000)
    private String text;
}
