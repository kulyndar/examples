package model;

import java.util.Collection;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Getter
@Setter
@Entity
@Table(name = "Person")
@NamedQueries({
    @NamedQuery(name = "Person.findByUsername", query = "SELECT p FROM Person p WHERE p.login = :login")})
public class Person {

    @Id
    @GeneratedValue
    private Integer userId;

    @Basic(optional = false)
    @Column(nullable = false, length = 100)
    private String name;

    @Basic(optional = false)
    @Column(nullable = false, length = 100)
    private String surname;

    @Basic(optional = false)
    @Column(nullable = false, unique = true, length = 100)
    private String login;

    @Basic(optional = false)
    @Column(nullable = false, unique = true, length = 100)
    private String mail;

    @Basic(optional = false)
    @Column(nullable = false)
    private String password;

    @Basic(optional = false)
    @Column(nullable = false)
    private String role;
    
    @OneToMany
    private Collection<Film> favoriteFilms;
    
    public void addToFavorit(Film film){
        favoriteFilms.add(film);
    }

    public void encodePassword(PasswordEncoder encoder) {
        Objects.requireNonNull(encoder);
        if (password != null) {
            this.password = encoder.encode(password);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }

        Person user = (Person) o;

        if (name != null ? !name.equals(user.name) : user.name != null) {
            return false;
        }
        if (surname != null ? !surname.equals(user.surname) : user.surname != null) {
            return false;
        }
        return login != null ? login.equals(user.login) : user.login == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        return result;
    }
}
