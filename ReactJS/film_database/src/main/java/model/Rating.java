package model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Rating")
public class Rating {

    @Id
    @GeneratedValue
    private Integer ratingId;
    
    @Basic(optional = false)
    @Column(nullable = false)
    private int stars;
    
    @Basic(optional = false)
    @Column(nullable = false)
    private String personLogin;
    
    @Basic(optional = false)
    @Column(nullable = false)
    private int filmId;
}
