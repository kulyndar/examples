package model;

import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Film")
@NamedQueries({
    @NamedQuery(name = "Film.findById", query = "SELECT f FROM Film f WHERE f.filmId = :filmId")})
public class Film {

    @Id
    @GeneratedValue
    private Integer filmId;

    @OneToMany()
    private Collection<Rating> ratings;

    @OneToMany()
    private Collection<Comment> comments;

    @Basic(optional = false)
    @Column(nullable = false)
    private String name;

    @Basic(optional = false)
    @Column(nullable = false)
    private String genre;

    @Basic(optional = false)
    @Column(nullable = false, length = 2000)
    private String info;

    @Basic(optional = false)
    @Column(nullable = false)
    private String countries;

    @Basic(optional = false)
    @Column(nullable = false)
    private String date;

    @Basic(optional = false)
    @Column(nullable = false)
    private String logo;
    
    public void addComment(Comment comment){
        comments.add(comment);
    }
    
    public void addRating(Rating rating){
        ratings.add(rating);
    }
}
