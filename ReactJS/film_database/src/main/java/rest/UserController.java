package rest;

import java.security.Principal;
import java.util.List;
import model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import static rest.BaseController.LOG;
import rest.utils.RestUtils;
import service.repository.UserService;
import service.security.SecurityUtils;

@RestController
@RequestMapping("/users")
public class UserController extends BaseController {

    @Autowired
    private UserService personService;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private SecurityUtils securityUtils;
    
    @PreAuthorize("permitAll()")
    @RequestMapping(method = RequestMethod.GET, value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public Person getCurrent(Principal principal) {
        return securityUtils.getCurrentUser();
    }
    
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(method = RequestMethod.GET, value = "/all_users", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Person> getAllUsers() {
        return personService.findAll();
    }
    
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    @RequestMapping(method = RequestMethod.PUT, value = "/current", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCurrent(@RequestBody Person inputuser) {
        Person user = personService.find(inputuser.getUserId());
        if (!securityUtils.getCurrentUser().equals(user)) {
            return;
        }
        user.setPassword(inputuser.getPassword());
        personService.update(user);
    }
    
    @PreAuthorize("permitAll()")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> create(@RequestBody Person user) {
        personService.persist(user);
        if (LOG.isTraceEnabled()) {
            LOG.trace("User {} successfully registered.", user);
        }
        final HttpHeaders headers = RestUtils
                .createLocationHeaderFromCurrentUri("/{profile}", user.getLogin());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
