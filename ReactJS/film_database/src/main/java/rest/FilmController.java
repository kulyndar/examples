package rest;

import api.RestHelper;
import api.model.FilmTheMovieDB;
import api.model.InformationAboutFilm;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import model.Comment;
import model.Film;
import model.Person;
import model.Rating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import static rest.BaseController.LOG;
import rest.utils.RestUtils;
import service.repository.CommentService;
import service.repository.FilmService;
import service.repository.RatingService;
import service.repository.UserService;
import utils.Constants;

@RestController
@RequestMapping("/films")
public class FilmController {

    @Autowired
    private FilmService filmService;
    
    @Autowired
    private RatingService ratingService;
    
    @Autowired
    private CommentService commentService;
    
    @Autowired
    private UserService userService;

    @PreAuthorize("permitAll()")
    @RequestMapping(method = RequestMethod.GET, value = "/name", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FilmTheMovieDB> getFilmsByName(@RequestParam(name = "name") String name) throws IOException {
        return RestHelper.getInterface()
                .getFilmList(Constants.API_KEY, name)
                .execute().body().getFilms();
    }

    @PreAuthorize("permitAll()")
    @RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Film getFilm(@PathVariable("id") Integer id) throws IOException {
        Film film = filmService.find(id);
        if (film == null) {
            InformationAboutFilm aboutFilm = RestHelper.getInterface()
                    .getFilmInformation(id, Constants.API_KEY).execute().body();
            Film f = new Film();
            f.setFilmId(aboutFilm.getId());
            f.setRatings(new ArrayList<>());
            f.setComments(new ArrayList<>());
            f.setName(aboutFilm.getTitle());
            String genres = "";
            for (int i = 0; i < aboutFilm.getGenres().size(); i++) {
                genres += aboutFilm.getGenres().get(i).getName() + ", ";
            }
            f.setGenre(genres);
            f.setInfo(aboutFilm.getOverview());
            String productionCountries = "";
            for (int i = 0; i < aboutFilm.getProduction_countries().size(); i++) {
                productionCountries += aboutFilm.getProduction_countries().get(i).getName() + ", ";
            }
            f.setCountries(productionCountries);
            f.setDate(aboutFilm.getRelease_date());
            f.setLogo(aboutFilm.getPoster_path());
            filmService.persist(f);
            return f;
        } else {
            return film;
        }
    }
    
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/comment", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createComment(@RequestBody Comment comment){
        commentService.persist(comment);
        Film f = filmService.find(comment.getFilmId());
        f.addComment(comment);
        filmService.update(f);
        if (LOG.isTraceEnabled()) {
            LOG.trace("Comment {} successfully created.", comment);
        }
        final HttpHeaders headers = RestUtils
                .createLocationHeaderFromCurrentUri("/{id}", comment.getCommentId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
    
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/rating", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createRating(@RequestBody Rating rating){
        ratingService.persist(rating);
        Film f = filmService.find(rating.getFilmId());
        f.addRating(rating);
        filmService.update(f);
        if (LOG.isTraceEnabled()) {
            LOG.trace("Rating {} successfully created.", rating);
        }
        final HttpHeaders headers = RestUtils
                .createLocationHeaderFromCurrentUri("/{id}", rating.getRatingId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
    
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/favorite/{idUser}", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addToFavorit(@RequestBody Integer idFilm,
            @PathVariable("idUser") Integer id){
        Film f = filmService.find(idFilm);
        Person p = userService.find(id);
        p.addToFavorit(f);
        userService.update(p);
        if (LOG.isTraceEnabled()) {
            LOG.trace("Film {} successfully added.", f);
        }
        final HttpHeaders headers = RestUtils
                .createLocationHeaderFromCurrentUri("/{id}", f.getFilmId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
    
}
