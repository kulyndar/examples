package api.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class FilmListResponse {

    @SerializedName("results")
    private List<FilmTheMovieDB> films;

    public List<FilmTheMovieDB> getFilms() {
        return films;
    }

    public void setFilms(List<FilmTheMovieDB> films) {
        this.films = films;
    }

}
