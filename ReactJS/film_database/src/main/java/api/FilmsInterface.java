package api;

import api.model.FilmListResponse;
import api.model.InformationAboutFilm;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FilmsInterface {
    @GET("search/movie")
    Call<FilmListResponse> getFilmList(@Query("api_key") String api_key, @Query("query") String query);
    
    @GET("movie/{movie_id}")
    Call<InformationAboutFilm> getFilmInformation(@Path("movie_id") int movie_id, @Query("api_key") String api_key);
}
