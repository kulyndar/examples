package persistence.dao;

import model.Rating;
import org.springframework.stereotype.Repository;


@Repository
public class RatingDao extends BaseDao<Rating> {
    
    public RatingDao() {
        super(Rating.class);
    }
    
}
