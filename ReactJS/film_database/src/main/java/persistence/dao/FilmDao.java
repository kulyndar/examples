package persistence.dao;

import java.util.List;
import javax.persistence.NoResultException;
import model.Film;
import org.springframework.stereotype.Repository;


@Repository
public class FilmDao extends BaseDao<Film> {
    
    public FilmDao() {
        super(Film.class);
    }
    
    public Film findByFilmId(int filmId) {
        try {
            return em.createNamedQuery("Film.findById", Film.class).setParameter("filmId", filmId)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public boolean exists(int filmId) {
        final List result = em.createNativeQuery("SELECT 1 FROM film WHERE filmId=?")
                              .setParameter(1, filmId).getResultList();
        return !result.isEmpty();
    }
}
