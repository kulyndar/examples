package persistence.dao;

import java.util.List;
import javax.persistence.NoResultException;
import model.Person;
import org.springframework.stereotype.Repository;


@Repository
public class UserDao extends BaseDao<Person> {
    
    public UserDao() {
        super(Person.class);
    }
    
    public Person findByUsername(String login) {
        try {
            return em.createNamedQuery("Person.findByUsername", Person.class).setParameter("login", login)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public boolean exists(String login) {
        final List result = em.createNativeQuery("SELECT 1 FROM person WHERE login=?")
                              .setParameter(1, login).getResultList();
        return !result.isEmpty();
    }
}
