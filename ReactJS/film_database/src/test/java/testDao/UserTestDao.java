package testDao;

import java.util.ArrayList;
import java.util.Date;
import model.Comment;
import model.Film;
import model.Person;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import persistence.dao.*;

public class UserTestDao extends BaseDaoTestRunner {

    private String name = "sasha";
    private String surname = "sasha";
    private String login = "sasha";
    private String mail = "sasha";
    private String password = "sasha";

    @Autowired
    private UserDao dao;

    @Autowired
    private FilmDao filmDao;

    @Autowired
    private RatingDao ratingDao;

    @Autowired
    private CommentDao commentDao;

    @Test
    public void findByUsernameReturnsPersonWithMatchingUsername() {
        final Person user = new Person();
        user.setName(name);
        user.setSurname(surname);
        user.setPassword(password);
        user.setLogin(login);
        user.setMail(mail);
        user.setRole("ADMIN");
        dao.persist(user);

        final Person result = dao.findByUsername(user.getLogin());
        assertNotNull(result);
        assertEquals(user.getUserId(), result.getUserId());
        dao.remove(result);
    }

    @Test
    public void findByUsernameReturnsNullForUnknownUsername() {
        assertNull(dao.findByUsername("unknownUsername"));
    }

    @Test
    public void existsReturnsTrueForExistingUsername() {
        final Person user = new Person();
        user.setName(name);
        user.setSurname(surname);
        user.setPassword(password);
        user.setLogin(login);
        user.setMail(mail);
        user.setRole("GUEST");
        dao.persist(user);
        assertTrue(dao.exists(user.getLogin()));
        dao.remove(user);
    }

    @Test
    public void existsReturnsFalseForUnknownUsername() {
        assertFalse(dao.exists("someArbitraryUsername"));
    }

    @Test
    public void testCommentAndRating() {
        Person u = new Person();
        u.setName("sosi");
        u.setSurname("sosi");
        u.setPassword("sosi");
        u.setLogin("sosi");
        u.setMail("sosi");
        u.setRole("GUEST");
        dao.persist(u);

        Film f = new Film();
        f.setFilmId(120);
        f.setRatings(new ArrayList<>());
        f.setComments(new ArrayList<>());
        f.setName("sosi");
        f.setGenre("sosi");
        f.setInfo("sosi");
        f.setCountries("sosi");
        f.setDate("sosi");
        f.setLogo("sosi");
        filmDao.persist(f);

//        Comment c = new Comment();
//        c.setFilmComment(f);
//        Film film1 = filmDao.find(f.getFilmId());
//        film1.addComment(c);
//        filmDao.update(film1);
//        c.setUser(u);
//        c.setText("dsfdsf");
//        c.setDate(new Date());
//        commentDao.persist(c);

        Film film = filmDao.find(f.getFilmId());
        if(film.getComments().isEmpty()){
            assertTrue(false);
        }
        assertNotNull(film);
        dao.remove(u);
//        commentDao.remove(c);
        filmDao.remove(film);
        
    }
}
