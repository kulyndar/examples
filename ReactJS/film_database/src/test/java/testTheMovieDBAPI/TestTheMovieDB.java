package testTheMovieDBAPI;

import api.RestHelper;
import api.model.FilmListResponse;
import api.model.FilmTheMovieDB;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.Constants;

public class TestTheMovieDB {

    private List<FilmTheMovieDB> result = new ArrayList<>();

    @Test
    public void findByUsernameReturnsPersonWithMatchingUsername() throws InterruptedException, IOException {
//        RestHelper.getInterface().getFilmList(Constants.API_KEY, "lord of the rings").enqueue(new Callback<FilmListResponse>() {
//            @Override
//            public void onResponse(Call<FilmListResponse> call, Response<FilmListResponse> response) {
//                if (result == null) {
//                    result = new ArrayList<>();
//                }
//                result.clear();
//                result.addAll(response.body().getFilms());
//                if (result.isEmpty()) {
//                    assertTrue(false);
//                } else {
//                    assertNotNull(result);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<FilmListResponse> call, Throwable t) {
//            }
//        });

        result = RestHelper.getInterface()
                .getFilmList(Constants.API_KEY, "lord of the rings")
                .execute().body().getFilms();
        assertNotNull(result);
    }
}
