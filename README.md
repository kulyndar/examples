# Examples

## Úvod

Všechny projekty byly udělány jako semestrální práce z různých předmětů. Jsou to spíše prototypy, než plně funkční aplikace.

## Struktura

### Složka Java

Ve složce jsou projekty, ve kterých jsem psala backend v Javě + frontend (pokud je v aplikaci)

### Složka ReactJS

Ve složce jsou projekty, ve kterých jsem psala pouze frontend pomoci ReactJS.