/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Broken;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Empty;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.UsesVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserAdult;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexander
 */
public class FridgeTest {
    
    

    /**
     * Test of use method, of class Fridge.
     */
    @Test
    public void testUse() {
        System.out.println("use");
        User u = new UserPet(1, "Sharik", "Novak", "Haf");
        User u2 = new UserAdult(2, "Marie", "Novakova");
        Device instance = new Fridge(new TypeOfConsume(0, "null", "null"),0,10);

        instance.use(u);
        assertEquals(instance.user, null);
        assertEquals(instance.funkcionality, 100);
        instance.use(u2);
        assertEquals(instance.user, u2);
        assertEquals(instance.funkcionality, 90);
    }


    /**
     * Test of crash method, of class Fridge.
     */
    @Test
    public void testCrash() {
        System.out.println("crash");
        Fridge instance = new Fridge(new TypeOfConsume(1, "0.6 km/h", "Electrisity"), 1, 10);
        instance.crash();
        assertEquals(instance.getState().getState(), new Broken(instance).getState());
    }


    /**
     * Test of empty method, of class Fridge.
     */
    @Test
    public void testEmpty() {
        System.out.println("empty");
        Fridge instance = new Fridge(new TypeOfConsume(1, "0.6 km/h", "Electrisity"), 1, 10);
        instance.empty();
        assertEquals(instance.getState().getState(), new Empty(instance).getState());
    }

    /**
     * Test of emptyRepair method, of class Fridge.
     */
    @Test
    public void testEmptyRepair() {
        System.out.println("emptyRepair");
        Fridge instance = new Fridge(new TypeOfConsume(1, "0.6 km/h", "Electrisity"), 1, 10);
        instance.emptyRepair();
        assertEquals(instance.food, 100);
    }

    
    
}
