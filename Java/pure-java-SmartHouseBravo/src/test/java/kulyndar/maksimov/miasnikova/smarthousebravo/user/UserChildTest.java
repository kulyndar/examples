/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.user;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Fridge;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.TypeOfConsume;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexander
 */
public class UserChildTest {

    /**
     * Test of react method, of class UserChild.
     */
    @Test
    public void testReact() {
        System.out.println("react");
        Device obs = new Fridge(new TypeOfConsume(1, "0.6 km/h", "Electrisity"), 1, 6);;
        UserChild instance = new UserChild(1, "Ivan", "Hus");
        
        instance.react(obs);
    }

    /**
     * Test of reactEmpty method, of class UserChild.
     */
    @Test
    public void testReactEmpty() {
        System.out.println("reactEmpty");
        Device obs = new Fridge(new TypeOfConsume(1, "0.6 km/h", "Electrisity"), 1, 6);
        UserChild instance = new UserChild(1, "Ivan", "Hus");
        instance.reactEmpty(obs);
    }
    
    
}
