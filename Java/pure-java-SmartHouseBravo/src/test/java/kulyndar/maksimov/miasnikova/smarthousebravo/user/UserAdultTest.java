/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.user;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Cooker;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Fridge;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.TypeOfConsume;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexander
 */
public class UserAdultTest {
    
    
    /**
     * Test of getRight method, of class UserAdult.
     */
    @org.junit.Test
    public void testGetRight() {
        System.out.println("getRight");
        UserAdult instance = new UserAdult(1, "Ivan", "Romanov");
        UserRight expResult = UserRight.ALL;
        UserRight result = instance.getRight();
        assertEquals(expResult, result);
    }

    /**
     * Test of react method, of class UserAdult.
     */
    @org.junit.Test
    public void testReact() {
        System.out.println("react");
        Device obs = new Fridge(new TypeOfConsume(1, "0.6 km/h", "Electrisity"), 1, 6);;
        UserAdult instance = new UserAdult(1, "Ivan", "Hus");
        instance.react(obs);
    }

    /**
     * Test of reactEmpty method, of class UserAdult.
     */
    @org.junit.Test
    public void testReactEmpty() {
        System.out.println("reactEmpty");
        Device obs = new Fridge(new TypeOfConsume(1, "0.6 km/h", "Electrisity"), 1, 6);
        UserAdult instance = new UserAdult(1, "Ivan", "Hus");
        instance.reactEmpty(obs);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
}
