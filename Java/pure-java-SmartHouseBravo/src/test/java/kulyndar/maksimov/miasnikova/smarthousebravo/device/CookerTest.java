/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Broken;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.UsesVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserAdult;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexander
 */
public class CookerTest {
    


    /**
     * Test of use method, of class Cooker.
     */
    @Test
    public void testUse() {
        System.out.println("use");
        User u = new UserPet(1, "Sharik", "Novak", "Haf");
        User u2 = new UserAdult(2, "Marie", "Novakova");
        Device instance = new Cooker(new TypeOfConsume(0, "null", "null"),0,10);

        instance.use(u);
        assertEquals(instance.user, null);
        assertEquals(instance.funkcionality, 100);
        instance.use(u2);
        assertEquals(instance.user, u2);
        assertEquals(instance.funkcionality, 90);

        
    }

    /**
     * Test of crash method, of class Cooker.
     */
    @Test
    public void testCrash() {
        System.out.println("crash");
        Cooker instance = new Cooker(new TypeOfConsume(1, "0.6 km/h", "Electrisity"), 1, 10);
        instance.crash();
        assertEquals(instance.getState().getState(), new Broken(instance).getState());
    }


    
    
}
