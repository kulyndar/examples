/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.manual.ManualInterface;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Broken;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.*;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.*;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Room;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Bike;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Car;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Ski;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserAdult;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserChild;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class DeviceTest {
    
    public DeviceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }



    /**
     * Test of crash method, of class Device.
     */
    @Test
    public void testCrash() {
        System.out.println("crash");
        Device instance = new AirConditioning(new TypeOfConsume(1, "null", "null"), 1, 10);
        instance.crash();
        // TODO review the generated test code and remove the default call to fail.
        
        assertEquals(instance.getState().getState(), new Broken(instance).getState());
    }

    

    /**
     * Test of accept method, of class Device.
     */
    @Test
    public void testAccept_ActivityVisitor() {
        System.out.println("accept ActivityVisitor");
        ReportVisitor visitor = new ActivityVisitor();
        Device instance = new AirConditioning(new TypeOfConsume(0,"null","null"),1, 10);
        String expResult = "AirConditioning number 1 was used 0 times";
        String result = instance.accept(visitor);
        assertEquals(expResult, result);
    }
    /**
     * Test of accept method, of class Device.
     */
    @Test
    public void testAccept_ConfigurationVisitor() {
        System.out.println("accept ConfigurationVisitor");
        ReportVisitor visitor = new ConfReportVisitor();
        Device instance = new AirConditioning(new TypeOfConsume(0,"null","null"),1, 10);
        String expResult = "\t\t\tAirConditioning number 1:"+
                "\n\t\t\t\tConsumption: null.Device consumpted 0 null."+
                "\n\t\t\t\tAirConditioning was used 0 times."+ "Functionality: 100";
        String result = instance.accept(visitor);
        assertEquals(expResult, result);
    }
    /**
     * Test of accept method, of class Device.
     */
    @Test
    public void testAccept_ConsumptionVisitor() {
        System.out.println("accept ConsumptionVisitor");
        ReportVisitor visitor = new ConsumptionVisitor();
        Device instance = new AirConditioning(new TypeOfConsume(0,"null","null"),1, 10);
        String expResult = "AirConditioning number 1 consumpted 0 "
                + "null" + ".\n" + "\tIt costs 0" +" Kc";
        String result = instance.accept(visitor);
        assertEquals(expResult, result);
    }
    /**
     * Test of accept method, of class Device.
     */
    @Test
    public void testAccept_EventVisitor() {
        System.out.println("accept EventVisitor");
        ReportVisitor visitor = new EventVisitor();
        Device instance = new AirConditioning(new TypeOfConsume(0,"null","null"),1, 10);
        instance.room = new Room("kitchen");
        String str = "AirConditioning: " + System.lineSeparator() + "\t"+ "AirConditioning use null"+System.lineSeparator();
        str = str+ "\t"+"AirConditioning number 1 was used 0 times "+ System.lineSeparator();
        str = str+ "\t"+"AirConditioning number 1 is in kitchen"+ System.lineSeparator();
        str = str+ "\t"+"AirConditioning number 1 is used by no one"+ System.lineSeparator();
        str = str +"\t"+"Functionality of AirConditioning number 1 is 100%"+System.lineSeparator();
        str = str +"\t"+"AirConditioning is Turned Off";
        String result = instance.accept(visitor);
        assertEquals(str, result);
    }

    /**
     * Test of repair method, of class Device.
     */
    @Test
    public void testRepair() {
        System.out.println("repair");
        Device instance = new Battery(new TypeOfConsume(0, "null", "null"),1, 99);
        instance.use(new UserAdult(1, "Marie", "Barinova"));
        assertEquals(instance.funkcionality,1);
        instance.repair();
        assertTrue(instance.funkcionality == 100);
    }



    /**
     * Test of on method, of class Device.
     */
    @Test
    public void testOn() {
        System.out.println("on");
        Device instance = new Computer(new TypeOfConsume(0, "null", "null"), 1, 10);
        assertEquals(instance.getState().getState(), new TurnOff(instance).getState());
        instance.on();
        assertEquals(instance.getState().getState(), new TurnOn(instance).getState());
        instance.on();
        assertEquals(instance.getState().getState(), new TurnOn(instance).getState());

    }

    /**
     * Test of use method, of class Device.
     */
    @Test
    public void testUse() {
        System.out.println("use");
        User u = new UserPet(1, "Sharik", "Novak", "Haf");
        User u2 = new UserAdult(2, "Marie", "Novakova");
        Device instance = new Computer(new TypeOfConsume(0, "null", "null"),0,10);

        instance.use(u);
        assertEquals(instance.user, null);
        assertEquals(instance.funkcionality, 100);
        instance.use(u2);
        assertEquals(instance.user, u2);
        assertEquals(instance.funkcionality, 90);

    }

    /**
     * Test of endOfUse method, of class Device.
     */
    @Test
    public void testEndOfUse() {
        System.out.println("endOfUse");
        User u = new UserAdult(1,"Anna", "Novakova");
        Device instance = new Computer(new TypeOfConsume(0, "null", "null"), 0,10);
        instance.use(u);
        instance.endOfUse(u);
        assertEquals(instance.getState().getState(), "Turned Off");
        assertEquals(instance.user, null);

    }



    
}
