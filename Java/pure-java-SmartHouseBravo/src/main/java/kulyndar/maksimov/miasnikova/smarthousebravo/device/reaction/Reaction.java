/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.reaction;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */

/*realizace command pattern*/
public abstract class Reaction {
    protected final Device d;

    public Reaction(Device d) {
        this.d = d;
    }

    public abstract void execute();
}
