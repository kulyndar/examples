/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.*;

import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Bike;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Car;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Ski;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserAdult;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserChild;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class ConfReportVisitor implements ReportVisitor{

    @Override
    public String visit(AirConditioning device) {
        String str = "\t\t\tAirConditioning number "+device.getDeviceId()+":"+
                "\n\t\t\t\tConsumption: "+device.getConsumption().getName()+".Device consumpted "+
                device.getCounter()*device.getCoefOfCons()+" "+device.getConsumption().getMeasureUnit()+ "."+/*krat procento spotreby*/
                "\n\t\t\t\tAirConditioning was used "+device.getCounter()+" times."+
                "Functionality: "+device.getFunkcionality();
                
                
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Battery device) {
        String str = "\t\t\tBattery number "+device.getDeviceId()+":"+
                "\n\t\t\t\tConsumption: "+device.getConsumption().getName()+".Device consumpted "+
                device.getCounter()*device.getCoefOfCons()+" "+device.getConsumption().getMeasureUnit()+ "."+/*krat procento spotreby*/
                "\n\t\t\t\tBattery was used "+device.getCounter()+" times."+
                "Functionality: "+device.getFunkcionality();
                
                
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Computer device) {
        String str = "\t\t\tComputer number "+device.getDeviceId()+":"+
                "\n\t\t\t\tConsumption: "+device.getConsumption().getName()+".Device consumpted "+
                device.getCounter()*device.getCoefOfCons()+" "+device.getConsumption().getMeasureUnit()+ "."+/*krat procento spotreby*/
                "\n\t\t\t\tComputer was used "+device.getCounter()+" times."+
                "Functionality: "+device.getFunkcionality();
                
                
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Cooker device) {
        String str = "\t\t\tCooker number "+device.getDeviceId()+":"+
                "\n\t\t\t\tConsumption: "+device.getConsumption().getName()+".Device consumpted "+
                device.getCounter()*device.getCoefOfCons()+" "+device.getConsumption().getMeasureUnit()+ "."+/*krat procento spotreby*/
                "\n\t\t\t\tCooker was used "+device.getCounter()+" times."+
                "Functionality: "+device.getFunkcionality();
                
                
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Fridge device) {
        String str = "\t\t\tFridge number "+device.getDeviceId()+":"+
                "\n\t\t\t\tConsumption: "+device.getConsumption().getName()+".Device consumpted "+
                device.getCounter()*device.getCoefOfCons()+" "+device.getConsumption().getMeasureUnit()+ "."+/*krat procento spotreby*/
                "\n\t\t\t\tFridge was used "+device.getCounter()+" times."+
                "Functionality: "+device.getFunkcionality();
                
                
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Kettle device) {
        String str = "\t\t\tKettle number "+device.getDeviceId()+":"+
                "\n\t\t\t\tConsumption: "+device.getConsumption().getName()+".Device consumpted "+
                device.getCounter()*device.getCoefOfCons()+" "+device.getConsumption().getMeasureUnit()+ "."+/*krat procento spotreby*/
                "\n\t\t\t\tKettle was used "+device.getCounter()+" times."+
                "Functionality: "+device.getFunkcionality();
                
                
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Door device) {
        String str = "\t\t\tDoor number "+device.getDeviceId()+":"+
                "\n\t\t\t\tConsumption: "+device.getConsumption().getName()+".Device consumpted "+
                device.getCounter()*device.getCoefOfCons()+" "+device.getConsumption().getMeasureUnit()+ "."+/*krat procento spotreby*/
                "\n\t\t\t\tDoor was used "+device.getCounter()+" times."+
                "Functionality: "+device.getFunkcionality();
                
                
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Light device) {
        String str = "\t\t\tLight number "+device.getDeviceId()+":"+
                "\n\t\t\t\tConsumption: "+device.getConsumption().getName()+".Device consumpted "+
                device.getCounter()*device.getCoefOfCons()+" "+device.getConsumption().getMeasureUnit()+ "."+/*krat procento spotreby*/
                "\n\t\t\t\tLight was used "+device.getCounter()+" times."+
                "Functionality: "+device.getFunkcionality();
                
                
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(MotionSensor device) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(WarmSensor device) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public String visit(UserAdult user) {
        String str = "Adult user "+user.getName()+" "+user.getSurname()+
                "\nUser has right "+user.getRight().name();
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(UserChild user) {
        String str = "Child "+user.getName()+" "+user.getSurname()+
                "\nUser has right "+user.getRight().name();
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(UserPet user) {
        String str = "Pet "+user.getName()+". "+
                "\nUser has right "+user.getRight().name();
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Bike transport) {
        String str = "Bike "+transport.getBrand()+
                " owned by "+transport.getOwner().getName()+" "+transport.getOwner().getSurname()+".";
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Car transport) {
        String str = "Car "+transport.getBrand()+
                " owned by "+transport.getOwner().getName()+" "+transport.getOwner().getSurname()+".";
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Ski transport) {
        String str = "Ski "+transport.getBrand()+
                " owned by "+transport.getOwner().getName()+" "+transport.getOwner().getSurname()+".";
        return str; //To change body of generated methods, choose Tools | Templates.
    }


}
