/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public abstract class State {
    protected String state;
    protected final Device device;

    public State(Device device) {
        this.device = device;
    }
    
    /**
     * Change state to the next one.
     */
    public abstract void toNextState();
    /**
     * Get the name of the state
     * @return name of the state
     */
    public String getState(){
        return this.state;
    };
}
