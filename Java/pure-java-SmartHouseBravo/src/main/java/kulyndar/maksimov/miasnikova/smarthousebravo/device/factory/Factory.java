/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.factory;

import java.util.List;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public interface Factory {
 
    /**
     * Creates new instance of the device.
     * Available devices:
     * - AirConditioning, Battery, Computer, Door, ElectricKettle, GasKettle
     * 
     * @param name - the type of Device
     * @param id - id of the device
     * @return created instance if name is defined, otherwise null
     */
    public Device getDevice(String name, int id); 
    /**
     * Creates new devices. Number of created devices is a parameter of constructor.
     * Available devices:
     * - AirConditioning, Battery, Computer, Door, ElectricKettle, GasKettle
     * @param name - the type of Device
     * @param number - number of devices to be created
     * @param id - id of the first device 
     * @return the list of created devices.
     */
    public List<? extends Device> getDevices(String name, int number, int id);
    
}
