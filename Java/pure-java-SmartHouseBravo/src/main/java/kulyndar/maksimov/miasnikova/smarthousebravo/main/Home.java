/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.main;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.MotionSensor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.WarmSensor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.*;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Transport;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class Home {
    
    private List<Floor> floors;
    private List<User> users;
    private List<Transport> transports;

    private Home() {
        System.out.println("Create Home");
        this.floors = new ArrayList<>();
        this.users = new ArrayList<>();
        this.transports = new ArrayList<>();
    }

    public static Home getInstance() {
        return HomeHolder.INSTANCE;
    }

    private static class HomeHolder {

        private static final Home INSTANCE = new Home();
    }
    /**
     * Shows the number of floors in the house
     * @return the number of floors
     */
    public int floorNumber() {
        return floors.size(); 
    }
    /**
     * Add floor to the list of floors
     * @param f - floor to be added
     */
    public void addFloor(Floor f) {
        floors.add(f);
    }
    /**
     * Adds user to the list of users
     * @param u - user to be added
     */
    public void addUser(User u) {
        users.add(u);
    }
    /**
     * Adds transport to the list of transports
     * @param t  - transport to be added.
     */
    public void addTransport(Transport t) {
        transports.add(t);
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Transport> getTransports() {
        return transports;
    }
    /**
     * Simulation of life in the house. Users uses random devices or transports, in the end different reports are created
     */
    public void live() {
        for (int i = 0; i < 100; i++) {

            for (User user : users) {
                int a = getRandomAction();
                //int a = 1;
                if (a == 1) {/// be inside
                    user.useDevice(getRandomDevice(getRandomRoom(getRandomFloor())));
                } else if (a == 2) {///be outside - use transport
                    //Transport t = new Transport(getRand)
                    user.useTransport(Home.getInstance().getTransports().get(getRandomTransport()-1));
                    System.out.println("User "+user.getName() + " uses "+(user.getTransportInUsed()==null?"nothing":user.getTransportInUsed().getClass().getSimpleName()));
                }
            }
            WarmSensor.temperatureReact();
            MotionSensor.light();
            for (Floor floor : floors) {
                floor.accept(new UsesVisitor());
            }
            System.out.println(MotionSensor.whereAreUsers());
            
            System.out.println("---------------------");
        }
        for (Floor floor : floors) {
            floor.accept(new InformationVisitor());
        }
        try {
            new ConfigurationReport();
            new ActivityReport();
            new ConsumptionReport();
            new TimeReport();
            new EventReport();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Gets random floor from the list of floors
     * @return random floor
     */
    public Floor getRandomFloor() {
        int max = this.floorNumber();
        int min = 1;
        int rand = min + (int) (Math.random() * ((max - min) + 1));
        return getFloors().get(rand - 1);
    }
    /**
     * Gets random room, that is at definite floor
     * @param f - floor where room is
     * @return random room
     */
    public Room getRandomRoom(Floor f) {
        int min = 1;
        int max = f.roomsNumber();
        int rand = min + (int) (Math.random() * ((max - min) + 1));
        return f.getRooms().get(rand - 1);
    }
    /**
     * Gets the random device in the definite room.
     * @param r - room, where the device is.
     * @return - random device
     */
    public Device getRandomDevice(Room r) {
        int min = 1;
        int max = r.getDevices().size();
        int rand = min + (int) (Math.random() * ((max - min) + 1));
        return r.getDevices().get(rand - 1);
    }
    /**
     * Define, what will user do. 1 means, that user will use device, 2- will use transport
     * @return random integer between [1,2];
     */
    public int getRandomAction() {
        int min = 1;
        int max = 2;
        int rand = min + (int) (Math.random() * ((max - min) + 1));
        return rand;

    }
    /**
     * Gets random transport from the list of transports
     * @return random transport
     */
    public int getRandomTransport(){
        int min = 1;
        int max = Home.getInstance().getTransports().size();
        int rand = min + (int) (Math.random() * ((max - min) + 1));
        return rand;
    }
    /**
     * Gets the temperature in the house
     * @return integer between [15, 35].
     */
    public int getTemperature(){
        int min = 15;
        int max = 35;
        int rand = min + (int) (Math.random() * ((max - min) + 1));
        return rand;
        
    }
}
