/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Broken;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.TurnOff;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.TurnOn;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Floor;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Home;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Room;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class WarmSensor extends Sensor {

    public WarmSensor(TypeOfConsume consumption, int deviceId, int procent) {
        super(consumption, deviceId, procent);
    }

    @Override
    public void accept(DeviceVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void crash() {
        this.setState(new Broken(this));
        notifyAllObservers();
    }

    @Override
    public void notify(Observer o) {
        System.out.println("WarmSensor is broken!");
        o.react(this);
    }

    @Override
    public String accept(ReportVisitor visitor) {
        return visitor.visit(this); //To change body of generated methods, choose Tools | Templates.
    }
    /**
     * Checks the temperature in the house. If the temperature is too low, turns on all batteries and turns off all air conditioning. If the temperature is too high, turns off all batteries and turns on all air conditioning.
     */
    public static void temperatureReact() {
        int t = Home.getInstance().getTemperature();
        if (t < 20) {
            System.out.println("Too cold in house!");
            for (Floor floor : Home.getInstance().getFloors()) {
                for (Room room : floor.getRooms()) {
                    for (Device device : room.getDevices()) {
                        if (device instanceof Battery) {
                            device.setState(new TurnOn(device));
                        } else if (device instanceof AirConditioning) {
                            device.setState(new TurnOff(device));
                        }
                    }
                }
            }
            System.out.println("I turned off all air conditioning and turned on all batteries!");
        } else if (t > 25) {
            System.out.println("Too hot in house!");
            for (Floor floor : Home.getInstance().getFloors()) {
                for (Room room : floor.getRooms()) {
                    for (Device device : room.getDevices()) {
                        if (device instanceof Battery) {
                            device.setState(new TurnOff(device));
                        } else if (device instanceof AirConditioning) {
                            device.setState(new TurnOn(device));
                        }
                    }
                }
            }
            System.out.println("I turned on all air conditioning and turned off all batteries!");
        } else {
            System.out.println("Temperature is between 20 and 25 degrees - OK");
        }
    }
}
