/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public abstract class Observable {
    List<Observer> observers = new ArrayList<>();
    /**
     * Notify observer about an action
     * @param o - observer to be notified
     */
    public abstract void notify(Observer o);
    /**
     * Notifies all observers
     */
    public void notifyAllObservers(){
        for (Observer observer : observers) {
            notify(observer);
        }
    }
    /**
     * Add an observer to the list of observers
     * @param observer  - observer to be added
     */
    public void attach(Observer observer){
        observers.add(observer);
    }
    
}
