/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.transport;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public abstract class Transport {

    User owner;
    String brand;
    User inUse;

    public Transport(User owner, String brand) {
        this.owner = owner;
        this.brand = brand;
        inUse = null;
    }

    /**
     * This function change value of using device if this device not in use or if this user has rights for use this device
     *
     * @param user
     */
    public void use(User user) {
        if (UserPet.class.isInstance(user)) {
            System.out.println("Pet can`t use transport");
            System.out.println("Gav Gav :(");
            System.out.println("");
            return;
        }
        if (inUse == null) {
            inUse = user;
        } else {
            System.out.println("Device is being used by someone else");
        }
    }

    /**
     * This function change value of using device to null (it means nobody use that device)
     */
    public void endUse(User u) {
        if (inUse == u) {
            inUse = null;
        }
    }

    /**
     * This function return owner of transport
     *
     * @return owner
     */
    public User getOwner() {
        return owner;
    }

    /**
     * This function return brand of transport
     *
     * @return brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Accept visitor is write out some information about transport
     *
     * @param visitor
     * @return
     */
    public abstract String accept(ReportVisitor visitor);

}
