package kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.*;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Bike;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Car;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Ski;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserAdult;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserChild;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;

/**
 * Created by kater on 11.01.2018.
 */
public class EventVisitor implements ReportVisitor {

    @Override
    public String visit(AirConditioning device) {
        String str = "AirConditioning: " + System.lineSeparator() + "\t"+ "AirConditioning use "+device.getConsumption().getName()+System.lineSeparator();
        str = str+ "\t"+"AirConditioning number "+device.getDeviceId()+" was used "+device.getCounter()+" times "+ System.lineSeparator();
        str = str+ "\t"+"AirConditioning number "+device.getDeviceId()+" is in "+device.getRoom().getName()+ System.lineSeparator();
        str = str+ "\t"+"AirConditioning number "+device.getDeviceId()+ " is used by "+(device.getUser()==null?"no one":device.getUser().getName())+ System.lineSeparator();
        str = str +"\t"+"Functionality of AirConditioning number "+device.getDeviceId()+" is "+device.getFunkcionality()+"%"+System.lineSeparator();;
        str = str +"\t"+"AirConditioning is "+device.getState().getState();
        return str;
    }

    public String visit(Battery device) {
        String str = "Battery: " + System.lineSeparator() + "\t"+ "Battery use "+device.getConsumption().getName()+System.lineSeparator();
        str = str+ "\t"+"Battery number "+device.getDeviceId()+" was used "+device.getCounter()+" times "+ System.lineSeparator();
        str = str+ "\t"+"Battery number "+device.getDeviceId()+" is in "+device.getRoom().getName()+ System.lineSeparator();
        str = str+ "\t"+"Battery number "+device.getDeviceId()+ " is used by "+(device.getUser()==null?"no one":device.getUser().getName())+ System.lineSeparator();
        str = str +"\t"+"Functionality of Battery number "+device.getDeviceId()+" is "+device.getFunkcionality()+"%"+System.lineSeparator();
        str = str +"\t"+"Battery is "+device.getState().getState();
        return str;
    }

    @Override
    public String visit(Computer device) {
        String str = "Computer: " + System.lineSeparator() + "\t"+ "Computer use "+device.getConsumption().getName()+System.lineSeparator();
        str = str+ "\t"+"Computer number "+device.getDeviceId()+" was used "+device.getCounter()+" times "+ System.lineSeparator();
        str = str+ "\t"+"Computer number "+device.getDeviceId()+" is in "+device.getRoom().getName()+ System.lineSeparator();
        str = str+ "\t"+"Computer number "+device.getDeviceId()+ " is used by "+(device.getUser()==null?"no one":device.getUser().getName())+ System.lineSeparator();
        str = str +"\t"+"Functionality of Computer number "+device.getDeviceId()+" is "+device.getFunkcionality()+"%"+System.lineSeparator();
        str = str +"\t"+"Computer is "+device.getState().getState();
        return str;
    }

    @Override
    public String visit(Cooker device) {
        String str = "Cooker: " + System.lineSeparator() + "\t"+ "Cooker use "+device.getConsumption().getName()+System.lineSeparator();
        str = str+ "\t"+"Cooker number "+device.getDeviceId()+" was used "+device.getCounter()+" times "+ System.lineSeparator();
        str = str+ "\t"+"Cooker number "+device.getDeviceId()+" is in "+device.getRoom().getName()+ System.lineSeparator();
        str = str+ "\t"+"Cooker number "+device.getDeviceId()+ " is used by "+(device.getUser()==null?"no one":device.getUser().getName())+ System.lineSeparator();
        str = str +"\t"+"Functionality of Cooker number "+device.getDeviceId()+" is "+device.getFunkcionality()+"%"+System.lineSeparator();
        str = str +"\t"+"Cooker is "+device.getState().getState();
        return str;
    }

    @Override
    public String visit(Fridge device) {
        String str = "Fridge: " + System.lineSeparator() + "\t"+ "Fridge use "+device.getConsumption().getName()+System.lineSeparator();
        str = str+ "\t"+"Fridge number "+device.getDeviceId()+" was used "+device.getCounter()+" times "+ System.lineSeparator();
        str = str+ "\t"+"Fridge number "+device.getDeviceId()+" is in "+device.getRoom().getName()+ System.lineSeparator();
        str = str+ "\t"+"Fridge number "+device.getDeviceId()+ " is used by "+(device.getUser()==null?"no one":device.getUser().getName())+ System.lineSeparator();
        str = str +"\t"+"Functionality of Fridge number "+device.getDeviceId()+" is "+device.getFunkcionality()+"%"+System.lineSeparator();
        str = str +"\t"+"Fridge is "+device.getState().getState();
        return str;
    }

    @Override
    public String visit(Kettle device) {
        String str = "Kettle: " + System.lineSeparator() + "\t"+ "Kettle use "+device.getConsumption().getName()+System.lineSeparator();
        str = str+ "\t"+"Kettle number "+device.getDeviceId()+" was used "+device.getCounter()+" times "+ System.lineSeparator();
        str = str+ "\t"+"Kettle number "+device.getDeviceId()+" is in "+device.getRoom().getName()+ System.lineSeparator();
        str = str+ "\t"+"Kettle number "+device.getDeviceId()+ " is used by "+(device.getUser()==null?"no one":device.getUser().getName())+ System.lineSeparator();
        str = str +"\t"+"Functionality of Kettle number "+device.getDeviceId()+" is "+device.getFunkcionality()+"%"+System.lineSeparator();
        str = str +"\t"+"Kettle is "+device.getState().getState();
        return str;
    }

    @Override
    public String visit(Door device) {
        String str = "Door: " + System.lineSeparator() + "\t"+ "Door use no one consumption"+ System.lineSeparator();
        str = str+ "\t"+"Door number "+device.getDeviceId()+" was used "+device.getCounter()+" times "+ System.lineSeparator();
        str = str+ "\t"+"Door number "+device.getDeviceId()+" is in "+device.getRoom().getName()+ System.lineSeparator();
        str = str+ "\t"+"Door number "+device.getDeviceId()+ " is used by "+(device.getUser()==null?"no one":device.getUser().getName())+ System.lineSeparator();
        str = str +"\t"+"Functionality of Door number "+device.getDeviceId()+" is "+device.getFunkcionality()+"%"+System.lineSeparator();
        return str;
    }

    @Override
    public String visit(Light device) {
        String str = "Light: " + System.lineSeparator() + "\t"+ "Light use "+device.getConsumption().getName()+System.lineSeparator();
        str = str+ "\t"+"Light number "+device.getDeviceId()+" was used "+device.getCounter()+" times "+ System.lineSeparator();
        str = str+ "\t"+"Light number "+device.getDeviceId()+" is in "+device.getRoom().getName()+ System.lineSeparator();
        str = str+ "\t"+"Light number "+device.getDeviceId()+ " is used by "+(device.getUser()==null?"no one":device.getUser().getName())+ System.lineSeparator();
        str = str +"\t"+"Functionality of Light number "+device.getDeviceId()+" is "+device.getFunkcionality()+"%"+System.lineSeparator();
        str = str +"\t"+"Light is "+device.getState().getState();
        return str;
    }

    @Override
    public String visit(MotionSensor device) {
        String str = "MotionSensor " + System.lineSeparator() + "\t"+ "MotionSensor use "+device.getConsumption().getName()+System.lineSeparator();
        str = str+ "\t"+"MotionSensor "+device.getDeviceId()+" was used "+device.getCounter()+" times "+ System.lineSeparator();
        str = str+ "\t"+"MotionSensor "+device.getDeviceId()+" is in "+device.getRoom().getName()+ System.lineSeparator();
        str = str+ "\t"+"MotionSensor "+device.getDeviceId()+ " is used by "+(device.getUser()==null?"no one":device.getUser().getName())+ System.lineSeparator();
        str = str +"\t"+"Functionality of Light number "+device.getDeviceId()+" is "+device.getFunkcionality()+"%"+System.lineSeparator();
        str = str +"\t"+"MotionSensor is "+device.getState().getState();
        return str;

    }

    @Override
    public String visit(WarmSensor device) {
        String str = "WarmSensor " + System.lineSeparator() + "\t"+ "WarmSensor use "+device.getConsumption().getName()+System.lineSeparator();
        str = str+ "\t"+"WarmSensor "+device.getDeviceId()+" was used "+device.getCounter()+" times "+ System.lineSeparator();
        str = str+ "\t"+"WarmSensor "+device.getDeviceId()+" is in "+device.getRoom().getName()+ System.lineSeparator();
        str = str+ "\t"+"WarmSensor "+device.getDeviceId()+ " is used by "+(device.getUser()==null?"no one":device.getUser().getName())+ System.lineSeparator();
        str = str +"\t"+"WarmSensor of Light number "+device.getDeviceId()+" is "+device.getFunkcionality()+"%"+System.lineSeparator();
        str = str +"\t"+"WarmSensor is "+device.getState().getState();
        return str;
    }


    @Override
    public String visit(UserAdult user) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(UserChild user) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(UserPet user) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Bike transport) {
        String str = "Bike "+ System.lineSeparator()+ "\t"+"Bike is use by"+ transport.getOwner().getName()+System.lineSeparator();
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Car transport) {
        String str = "Car "+ System.lineSeparator()+ "\t"+"Car is use by"+ transport.getOwner().getName()+System.lineSeparator();
        return str;

    }

    @Override
    public String visit(Ski transport) {
        String str = "Ski "+ System.lineSeparator()+ "\t"+"Car is use by"+ transport.getOwner().getName()+System.lineSeparator();
        return str;
    }

}