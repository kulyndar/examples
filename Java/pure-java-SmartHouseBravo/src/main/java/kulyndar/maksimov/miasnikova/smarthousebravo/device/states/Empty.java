package kulyndar.maksimov.miasnikova.smarthousebravo.device.states;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Fridge;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.State;

/**
 * Created by kater on 08.01.2018.
 */
public class Empty extends State {

    public Empty(Device device) {
        super(device);
        this.state = "Empty";
    }

    @Override
    public void toNextState() {
        if (this.device instanceof Fridge) {
            this.device.setState(new TurnOn(this.device));
        } else {
            this.device.setState(new TurnOff(this.device));
        }

    }

}
