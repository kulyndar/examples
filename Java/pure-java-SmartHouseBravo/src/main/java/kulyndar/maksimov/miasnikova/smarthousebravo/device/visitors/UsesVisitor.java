/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.AirConditioning;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Battery;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Computer;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Cooker;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Door;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Fridge;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Kettle;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Light;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.MotionSensor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.WarmSensor;


/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class UsesVisitor implements DeviceVisitor{

    @Override
    public void visit(AirConditioning device) {
         System.out.println("AirConditioning now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
         
    }

    @Override
    public void visit(Battery device) {
        System.out.println("Battery now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
        
    }

    @Override
    public void visit(Computer device) {
        System.out.println("Computer now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
       
    }

    @Override
    public void visit(Cooker device) {
        System.out.println("Cooker now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
        
    }

    @Override
    public void visit(Fridge device) {
        System.out.println("Fridge now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
       
    }

    @Override
    public void visit(Kettle device) {
        System.out.println("Kettle now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
        
    }

    @Override
    public void visit(Door device) {
        
    }

    @Override
    public void visit(Light device) {
    }

    @Override
    public void visit(MotionSensor device) {
    }

    @Override
    public void visit(WarmSensor device) {
    }

    
}
