/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.factory;

import java.util.ArrayList;
import java.util.List;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.AirConditioning;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Battery;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Computer;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Door;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Kettle;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.TypeOfConsume;

/**
 *
 * @author kulyndar;
 */
public class DeviceFactory implements Factory {

    @Override
    public Device getDevice(String name, int id) {
        switch (name) {
            case "AirConditioning":
                return new AirConditioning(new TypeOfConsume(0, "kW/hour", "Elektricity"), id, 15);       
            case "Battery":
                
                return new Battery(new TypeOfConsume(0, "kW/hour", "Elektrcity"), id, 5);
            case "Computer":

                return new Computer(new TypeOfConsume(0, "kW/hour", "Elektricity"), id ,10 );
            case "Door":
                return new Door(new TypeOfConsume(0, "null", "null"), id, 0);
                
            case "ElectricKettle":
                TypeOfConsume El = new TypeOfConsume(0, "kW/hour", "Elektricity");
                return new Kettle(El, id, 15);
            case "GasKettle":
                TypeOfConsume Gas = new TypeOfConsume(1, "m^3/hour", "Gas");
return new Kettle(Gas, id, 5);

            default:
                return null;
        }
       
    }

    @Override
    public List<? extends Device> getDevices(String name, int number, int id) {
        List<Device> list = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            list.add(this.getDevice(name, id));
            id++;
        }
        return list;
       //To change body of generated methods, choose Tools | Templates.
    }

}
