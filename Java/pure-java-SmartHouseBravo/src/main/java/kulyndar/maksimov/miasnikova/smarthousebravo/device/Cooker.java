/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Broken;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.TurnOff;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserAdult;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class Cooker extends Device {


    public Cooker(TypeOfConsume consumption, int deviceId, int procent) {
        super(consumption, deviceId, procent);
        this.state = new TurnOff(this);//nastavit default state - kdy vytvorime ruzne staty
        this.coefOfCons =3;
    }

    @Override
    public void accept(DeviceVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void use(User u) {
        if (UserAdult.class.isInstance(u)) {
            super.use(u);
        } else {
            System.out.println("You can`t use this device, you don`t have enough rights, "+u.getName()+" "+u.getSurname());
        } //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public void crash() {
        this.setState(new Broken(this));
        notifyAllObservers();
    }

    @Override
    public void notify(Observer o) {
        System.out.println("Cooker is broken!");
        o.react(this);
    }

    @Override
    public String accept(ReportVisitor visitor) {
        return visitor.visit(this); //To change body of generated methods, choose Tools | Templates.
    }

}
