/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;

import kulyndar.maksimov.miasnikova.smarthousebravo.main.Floor;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Home;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Room;

import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Transport;

import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class ConfigurationReport {
    private final PrintWriter writer;
    
    public ConfigurationReport() throws FileNotFoundException {
        File dir = new File("../docs/reports/");
        if(!dir.exists()){
            dir.mkdir();
        }
        this.writer = new PrintWriter(new File("../docs/reports/config-report.txt"));
        this.createReport();
        this.writer.close();
        
    }
    
    private void createReport(){
        Home home = Home.getInstance();
        writer.println("Home configuration: ");
        for (Floor floor : home.getFloors()) {
            writer.println("\tFloor "+floor.getNumber()+" contains "+floor.roomsNumber()+" rooms");
            for (Room room : floor.getRooms()) {
                writer.println("\t\t"+room.getName()+ " has devices: ");
                for (Device device : room.getDevices()) {
                    writer.println(device.accept(new ConfReportVisitor()));
                    writer.println();
                }
                writer.println();
            }
            writer.println();
        }
        writer.println("Information about householders:");
        for (User user : home.getUsers()) {
            writer.println(user.accept(new ConfReportVisitor()));
            writer.println();
        }
        writer.println("Information about transport:");
        for (Transport transport : home.getTransports()) {
            writer.println(transport.accept(new ConfReportVisitor()));
            writer.println();
        }
    }
  
}
