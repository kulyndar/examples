/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;


/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public interface Observer {
    /**
     * Observer reacts on action
     * @param obs - device, that gives an action
     */
    public void react(Device obs);
    /**
     * Observer reacts, when there is no food in fridge or no water in kettle
     * @param obs - device, that gives an action
     */
    public void reactEmpty(Device obs);
    //this is senzor
}
