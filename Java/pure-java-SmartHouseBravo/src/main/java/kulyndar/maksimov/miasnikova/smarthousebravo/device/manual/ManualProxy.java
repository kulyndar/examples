/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.manual;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class ManualProxy implements ManualInterface{
    private final Device device;

    public ManualProxy(Device device) {
        this.device = device;
    }
    
    
    @Override
    public void read() {
        new Manual(device).read();
    }
    
}
