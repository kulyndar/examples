/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.user;

/**
 *
 * @author kulyndar;
 */
public class UsingState extends UserState{

    public UsingState(User user) {
        super(user);
        this.state = "Using";
    }

    @Override
    public void toNextState() {
        user.setState(new IdleState(user));
    }
    
}
