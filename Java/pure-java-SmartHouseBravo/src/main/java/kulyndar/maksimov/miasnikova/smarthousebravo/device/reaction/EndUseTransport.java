/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.reaction;

import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Transport;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class EndUseTransport {
    private final Transport t;
    private final User u;

    public EndUseTransport(Transport t, User u) {
        this.t = t;
        this.u = u;
    }
    
    /**
     * Force transport to be put of use
     */
    public void execute(){
        if(t!=null){
        t.endUse(u);}
    }
    
}
