/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.reaction;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class OnReaction extends Reaction{

    public OnReaction(Device d) {
        super(d);
    }

    @Override
    /**
     * Force device to turn on
     */
    public void execute() {
        this.d.on();
    }
    
}
