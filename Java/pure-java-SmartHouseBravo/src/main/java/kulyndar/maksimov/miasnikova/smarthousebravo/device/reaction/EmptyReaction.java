package kulyndar.maksimov.miasnikova.smarthousebravo.device.reaction;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Fridge;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Kettle;

/**
 * Created by kater on 08.01.2018.
 */
public class EmptyReaction extends Reaction {
    public EmptyReaction(Device d) {
        super(d);
    }

    @Override
    /**
     * Add water to the kettle or food to the fridge
     */
    public void execute() {
        if (Fridge.class.isInstance(d))
            ((Fridge)this.d).emptyRepair();
        if (Kettle.class.isInstance(d))
            ((Kettle)this.d).emptyRepair();
    }
}
