/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.manual.ManualInterface;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.manual.ManualProxy;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.*;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Room;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public abstract class Device extends Observable {

    TypeOfConsume consumption;
    protected int funkcionality = 100; //procent funkcnosti
    State state;
    int deviceId;
    protected Room room;
    protected final int prOfFunc;
    User user;
    ManualInterface manual;
    int counter = 0;
    int coefOfCons = 0;

    public Device(TypeOfConsume consumption, int deviceId, int procent) {
        this.consumption = consumption;
        this.deviceId = deviceId;
        this.prOfFunc = procent;
        manual = new ManualProxy(this);
    }

    public void setState(State s) {
        this.state = s;
    }

    public State getState() {
        return this.state;
    }

    /**
     * Function is called, when the functionality is 0. Set state of device to Broken and notify all observers, that this device
     * is broken
     */
    public abstract void crash();

    /**
     * Device accepts visitor, that takes information about device and write it to standard output.
     *
     * @param visitor - visitor, that should take information
     */
    public abstract void accept(DeviceVisitor visitor);

    /**
     * Device accepts visitor, that takes information about device and write it to report.
     *
     * @param visitor - visitor, that should take information
     * @return string to be written to report
     */
    public abstract String accept(ReportVisitor visitor);

    //Realizace Command patternu
    /**
     * Function is called, when the device is broken. Switches off the device and repairs it(set functionality to 100).
     */
    public void repair() {
        this.state.toNextState();
        //change funcionality
        this.funkcionality = 100;
    }

    /**
     * Switches off the device.
     */
    public void off() {
        if (this.state instanceof TurnOn) {
            this.state.toNextState();
        }
        //change funcionality

        //set cumsumption in off state
    }

    /**
     * Switches on the device.
     */
    public void on() {
        if (this.state instanceof TurnOff) {
            this.state.toNextState();
        }
        //change funcionality

        //set consumption in idle state
    }

    /**
     * Function is called, when someone wants to use the device.
     *
     * @param u - user, that wants to use the device
     */
    public void use(User u) {
        if (UserPet.class.isInstance(u)) {
            System.out.println("Pet can`t use anything");
            System.out.println(((UserPet) u).getToSay());
            System.out.println("");
            return;
        }
        this.user = u;
        if ((this.funkcionality - this.prOfFunc) <= 0) {
            crash();
            //broken
        } else {
            this.funkcionality -= this.prOfFunc;
            this.counter++;
        }
    }

    /**
     * Function is called, when someone wants to end using that device.
     *
     * @param u - user that use this device.
     */
    public void endOfUse(User u) {
        if (this.user == u) {
            this.user = null;
            if (Fridge.class.isInstance(this) || Battery.class.isInstance((this)) || Light.class.isInstance(this)) {

            } else {
                this.state = new TurnOff(this);
            }
        }
    }

    public TypeOfConsume getConsumption() {
        return consumption;
    }

    public int getFunkcionality() {
        return funkcionality;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public Room getRoom() {
        return room;
    }

    public int getPrOfFunc() {
        return prOfFunc;
    }

    public User getUser() {
        return user;
    }

    public int getCounter() {
        return counter;
    }

    public int getCoefOfCons() {
        return coefOfCons;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public ManualInterface getManual() {
        return manual;
    }

}
