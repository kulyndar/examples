/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.reaction;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class EndUseReaction extends Reaction {

    User user;

    public EndUseReaction(Device d, User user) {
        super(d);
        this.user = user;
    }

    @Override
    /**
     * Force device to be out of use
     */
    public void execute() {
        if (d != null) {
            d.endOfUse(user);
        }
    }

}
