/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.user;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.reaction.EmptyReaction;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class UserChild extends User {
    
    public UserChild(int idUser, String name, String surname) {
        super(idUser, name, surname);
        this.right = UserRight.LIGHT;
    }

    @Override
    public void react(Device obs) {
        System.out.println("Ahaaaaaaaaaaaahaaaaaa");
    }

    @Override
    public  void reactEmpty(Device obs){

        System.out.println("-----So sad ------");

    }

    @Override
    public String accept(ReportVisitor visitor) {
        return visitor.visit(this);//To change body of generated methods, choose Tools | Templates.
    }
    

}
