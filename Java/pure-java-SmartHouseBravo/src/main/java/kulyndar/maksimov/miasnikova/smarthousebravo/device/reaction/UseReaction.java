/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.reaction;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class UseReaction extends Reaction{
    User user;
    public UseReaction(Device d, User u) {
        super(d);
        this.user = u;
    }

    @Override
    /**
     * Force device to be used
     */
    public void execute() {
        this.d.use(user);
    }
    
}
