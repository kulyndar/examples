/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.states;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.State;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class TurnOff extends State{

    public TurnOff(Device device) {
        super(device);
        this.state = "Turned Off";
    }

   

    @Override
    public void toNextState() {
        this.device.setState(new TurnOn(device));
    }
    
}
