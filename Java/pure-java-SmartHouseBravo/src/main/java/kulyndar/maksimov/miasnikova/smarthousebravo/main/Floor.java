/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.main;

import java.util.ArrayList;
import java.util.List;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class Floor {
    private List<Room> rooms;
    private final int number;

    public Floor(int number) {
        this.number = number;
        rooms = new ArrayList<>();
    }
    
    
    
    /**
     * Adds room to the list of rooms of the floor.
     * @param r - room to be added.
     */
    public void addRoom(Room r){
        rooms.add(r);
        r.setFloor(this);
    }
    /**
     * Adds all rooms from the list to the list of rooms. 
     * @param room  - list of rooms to be added.
     */
    public void addAll(List<Room> room){
        for (Room r : room) {
            addRoom(r);
        }
    }
    /**
     * The number of rooms at that floor.
     * @return - the number of rooms.
     */
    public int roomsNumber(){
        return rooms.size();
    }
    
    public List<Room> getRooms() {
        return rooms;
    }

    public int getNumber() {
        return number;
    }
    /**
     * Forces each room at that floor to accept Device visitor
     * @param visitor - visitor to be accepted.
     */
    public void accept(DeviceVisitor visitor){
        rooms.forEach(r->r.accept(visitor));
    }
}
