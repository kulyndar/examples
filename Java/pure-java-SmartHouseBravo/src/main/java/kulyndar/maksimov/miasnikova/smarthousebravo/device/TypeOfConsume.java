/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class TypeOfConsume {
    private int id;
    private String measureUnit;
    private String name;

    public TypeOfConsume(int id, String measureUnit, String name) {
        this.id = id;
        this.measureUnit = measureUnit;
        this.name = name;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMeasureUnit() {
        return measureUnit;
    }

    public void setMeasureUnit(String measureUnit) {
        this.measureUnit = measureUnit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
