/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.user;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Observable;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.reaction.EmptyReaction;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.reaction.RepairReaction;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class UserAdult extends User {

    public UserAdult(int idUser, String name, String surname) {
        super(idUser, name, surname);
        this.right = UserRight.ALL;
    }

    public UserRight getRight() {
        return UserRight.ALL;
    }

    @Override
    public void react(Device obs) {
        System.out.println("-------------");
        System.out.println("-----");
        obs.getManual().read();
        System.out.println("-----");
        System.out.println("----");
        System.out.println("----");
        System.out.println("repaired by "+this.getName());
        System.out.println("----");
        System.out.println("----");
        System.out.println("-----");
        System.out.println("-------------");
        new RepairReaction(obs).execute();
    }

    @Override
    public  void reactEmpty(Device obs){

        System.out.println("-----");
        System.out.println("----");
        System.out.println("---");
        System.out.println("fill by "+this.getName());
        System.out.println("---");
        System.out.println("----");
        System.out.println("-----");
        new EmptyReaction(obs).execute();

    }

    @Override
    public String accept(ReportVisitor visitor) {
        return visitor.visit(this); //To change body of generated methods, choose Tools | Templates.
    }
    

    
}
