/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.manual;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public interface ManualInterface {
    /**
     * Function is called. when user wants to repair a device.
     */
    public void read();
}
