/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.*;



/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class InformationVisitor implements DeviceVisitor{

    @Override
    public void visit(AirConditioning device) {
        System.out.println("AirConditioning number "+device.getDeviceId()+":");
        System.out.println("Consumption: "+device.getConsumption().getName());
        System.out.println("Functionality: "+device.getFunkcionality());
        System.out.println("AirConditioning is placed in "+device.getRoom().getName());
        System.out.println("Now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
        System.out.println("AirConditioning is "+device.getState().getState());
        System.out.println("This device was used "+device.getCounter()+" times");
        System.out.println("---------------------------------------------");
        
    }

    @Override
    public void visit(Battery device) {
        System.out.println("Battery number "+device.getDeviceId()+":");
        System.out.println("Consumption: "+device.getConsumption().getName());
        System.out.println("Functionality: "+device.getFunkcionality());
        System.out.println("Battery is placed in "+device.getRoom().getName());
        System.out.println("Now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
        System.out.println("Battery is "+device.getState().getState());
        System.out.println("This device was used "+device.getCounter()+" times");
        System.out.println("---------------------------------------------");
        
    }

    @Override
    public void visit(Computer device) {
        System.out.println("Computer number "+device.getDeviceId()+":");
        System.out.println("Consumption: "+device.getConsumption().getName());
        System.out.println("Functionality: "+device.getFunkcionality());
        System.out.println("Computer is placed in "+device.getRoom().getName());
        System.out.println("Now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
        System.out.println("Computer is "+device.getState().getState());
        System.out.println("This device was used "+device.getCounter()+" times");
        System.out.println("---------------------------------------------");
    }

    @Override
    public void visit(Cooker device) {
        System.out.println("Cooker number "+device.getDeviceId()+":");
        System.out.println("Consumption: "+device.getConsumption().getName());
        System.out.println("Functionality: "+device.getFunkcionality());
        System.out.println("Cooker is placed in "+device.getRoom().getName());
        System.out.println("Now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
        System.out.println("Cooker is "+device.getState().getState());
        System.out.println("This device was used "+device.getCounter()+" times");
        System.out.println("---------------------------------------------");
    }

    @Override
    public void visit(Fridge device) {
        System.out.println("Fridge number "+device.getDeviceId()+":");
        System.out.println("Consumption: "+device.getConsumption().getName());
        System.out.println("Functionality: "+device.getFunkcionality());
        System.out.println("Fridge is placed in "+device.getRoom().getName());
        System.out.println("Now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
        System.out.println("Fridge is "+device.getState().getState());
        System.out.println("This device was used "+device.getCounter()+" times");
        System.out.println("---------------------------------------------");
    }

    @Override
    public void visit(Kettle device) {
        System.out.println("Kettle number "+device.getDeviceId()+":");
        System.out.println("Consumption: "+device.getConsumption().getName());
        System.out.println("Functionality: "+device.getFunkcionality());
        System.out.println("Kettle is placed in "+device.getRoom().getName());
        System.out.println("Now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
        System.out.println("Kettle is "+device.getState().getState());
        System.out.println("This device was used "+device.getCounter()+" times");
        System.out.println("---------------------------------------------");
    }

    @Override
    public void visit(Door device) {
        System.out.println("Door number "+device.getDeviceId()+":");
        System.out.println("Consumption: "+device.getConsumption().getName());
        System.out.println("Functionality: "+device.getFunkcionality());
        System.out.println("Door is placed in "+device.getRoom().getName());
        System.out.println("Now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
        System.out.println("Door is "+device.getState().getState());
        System.out.println("This device was used "+device.getCounter()+" times");
        System.out.println("---------------------------------------------");
    }

    @Override
    public void visit(Light device) {
        System.out.println("Light number "+device.getDeviceId()+":");
        System.out.println("Consumption: "+device.getConsumption().getName());
        System.out.println("Functionality: "+device.getFunkcionality());
        System.out.println("Light is placed in "+device.getRoom().getName());
        System.out.println("Now is in use by "+(device.getUser()==null?"no one":device.getUser().getName()));
        System.out.println("Light is "+device.getState().getState());
        System.out.println("This device was used "+device.getCounter()+" times");
        System.out.println("---------------------------------------------");
    }

    @Override
    public void visit(MotionSensor device) {
    }

    @Override
    public void visit(WarmSensor device) {
    }

    
}
