/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.main;

import java.util.Scanner;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.*;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.TypeOfConsume;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.factory.*;

import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Bike;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Car;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Ski;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Transport;

import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserAdult;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserChild;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("Enter the number of configuration that you want to start [1 or 2]:");
        Scanner scan = new Scanner(System.in);
        int config = scan.nextInt();
        switch (config) {
            case 1:
                createConfigurationOne();
                Home.getInstance().live();
                break;
            case 2:
                createConfigurationTwo();
                Home.getInstance().live();
                break;
            default:
                System.out.println("Please, enter correct number of configuration!");
        }
    }

    public static void createConfigurationOne() {

        Home home = Home.getInstance();
        System.out.println("Create home 1");
        /*6 users*/
        UserAdult mother = new UserAdult(1, "Marie", "Novakova");
        UserAdult father = new UserAdult(2, "Jiri", "Novak");
        UserChild daughter = new UserChild(3, "Anna", "Novakova");
        UserChild son = new UserChild(4, "Jan", "Novak");
        UserAdult uncle = new UserAdult(5, "Dominik", "Smith");
        UserChild cousine = new UserChild(6, "Lenny", "Smith");
        /*3 pets*/
        UserPet dog = new UserPet(1, "Sharik", "Novak", "Haf Haf Haf");
        UserPet cat = new UserPet(2, "Oliver", "Superstar", "Mnau Mnau");
        UserPet cat2 = new UserPet(3, "Tjoma", "Smith", "Mrrrrrrrrrr");

        home.addUser(mother);
        home.addUser(father);
        home.addUser(daughter);
        home.addUser(son);
        home.addUser(uncle);
        home.addUser(cousine);
        home.addUser(dog);
        home.addUser(cat);
        home.addUser(cat2);

        TypeOfConsume gas = new TypeOfConsume(1, "m^3/hour", "Gas");
        TypeOfConsume electricity = new TypeOfConsume(2, "kW/hour", "Elektricity");
        TypeOfConsume nothing = new TypeOfConsume(3, "null", "null");

        /*20 devices*/
        Factory f = new DeviceFactory();

        Device cooker = new Cooker(gas, 1, 10);
        Device fridge = new Fridge(electricity, 1, 5);
        Device light = new Light(electricity, 1, 2);
        Device light2 = new Light(electricity, 2, 2);
        Device light3 = new Light(electricity, 3, 2);
        Device light4 = new Light(electricity, 4, 2);
        Device light5 = new Light(electricity, 5, 2);
        Device light6 = new Light(electricity, 6, 2);

        /*6 rooms*/
        Room kitchen = new Room("Kitchen");
        Room living = new Room("Living room");
        Room bedroom = new Room("Bedroom");
        Room wc = new Room("WC");
        Room bedroom2 = new Room("Bedroom");
        Room cabinet = new Room("Cabinet");

        kitchen.addDevice(cooker);
        kitchen.addDevice(fridge);
        kitchen.addDevice(light);
        kitchen.addDevice(f.getDevice("GasKettle", 1));
        kitchen.addDevice(f.getDevice("ElectricKettle", 2));
        kitchen.addAllDevices(f.getDevices("Battery", 3, 1));
        kitchen.addDevice(f.getDevice("Door", 2));
        kitchen.addDevice(f.getDevice("AirConditioning", 2));

        living.addAllDevices(f.getDevices("Battery", 3, 4));
        living.addDevice(f.getDevice("Computer", 1));
        living.addDevice(f.getDevice("AirConditioning", 1));
        living.addDevice(f.getDevice("Door", 1));
        living.addDevice(light2);

        bedroom.addDevice(f.getDevice("Computer", 2));
        bedroom.addDevice(f.getDevice("Door", 3));
        bedroom.addDevice(light);
        bedroom.addDevice(light3);

        bedroom2.addDevice(light4);
        bedroom2.addDevice(f.getDevice("Computer", 3));
        bedroom2.addDevice(f.getDevice("Door", 4));

        wc.addAllDevices(f.getDevices("Battery", 3, 7));
        wc.addDevice(f.getDevice("Door", 5));
        wc.addDevice(light5);


        cabinet.addDevice(light6);
        cabinet.addDevice(f.getDevice("Computer", 4));
        cabinet.addDevice(f.getDevice("Door", 6));

        Floor fl = new Floor(1);
        fl.addRoom(living);
        fl.addRoom(kitchen);
        fl.addRoom(bedroom);

        Floor f2 = new Floor(2);
        f2.addRoom(wc);
        f2.addRoom(bedroom2);
        f2.addRoom(cabinet);

        home.addFloor(fl);
        home.addFloor(f2);

        kitchen.getDevices().forEach(d -> d.attach(mother));
        kitchen.getDevices().forEach(d -> d.attach(dog));
        living.getDevices().forEach(dev -> dev.attach(father));
        living.getDevices().forEach(d -> d.attach(daughter));
        cabinet.getDevices().forEach(d -> d.attach(father));
        wc.getDevices().forEach(d -> d.attach(uncle));
        bedroom.getDevices().forEach(d -> d.attach(mother));
        bedroom2.getDevices().forEach(d -> d.attach(uncle));

        Transport bDad = new Bike(father, "Java");
        Transport bMom = new Bike(mother, "Blickle");
        Transport bDtr = new Bike(daughter, "Java");
        Transport car = new Car(father, "Opel");
        Transport ski = new Ski(daughter, "Sporten");
        home.addTransport(bDad);
        home.addTransport(bMom);
        home.addTransport(bDtr);
        home.addTransport(car);
        home.addTransport(ski);
    }

    public static void createConfigurationTwo() {
        Home home = Home.getInstance();
        System.out.println("Create home 2");
        /*6 users*/
        UserAdult mother = new UserAdult(1, "Marie", "Novakova");
        UserAdult father = new UserAdult(2, "Jiri", "Novak");
        UserChild daughter = new UserChild(3, "Anna", "Novakova");
        UserChild son = new UserChild(4, "Jan", "Novak");
        UserAdult uncle = new UserAdult(5, "Dominik", "Smith");
        UserAdult aunt = new UserAdult(6, "Jenny", "Smith");
        /*3 pets*/
        UserPet dog = new UserPet(1, "Sharik", "Novak", "Haf Rrrrrr");
        UserPet cat = new UserPet(2, "Oliver", "Superstar", "Mnau I am superstar");
        UserPet tortile = new UserPet(3, "Tortile", "Fast", "Mur Mur Mur");

        home.addUser(mother);
        home.addUser(father);
        home.addUser(daughter);
        home.addUser(son);
        home.addUser(uncle);
        home.addUser(aunt);
        home.addUser(dog);
        home.addUser(cat);
        home.addUser(tortile);

        TypeOfConsume gas = new TypeOfConsume(1, "m^3/hour", "Gas");
        TypeOfConsume electricity = new TypeOfConsume(2, "kW/hour", "Elektricity");
        TypeOfConsume nothing = new TypeOfConsume(3, "null", "null");

        /*20 devices*/
        Factory f = new DeviceFactory();

        
        Device cooker = new Cooker(gas, 1, 10);
        Device fridge = new Fridge(electricity, 1, 5);
        Device light = new Light(electricity, 1, 2);
        Device light2 = new Light(electricity, 2, 2);
        Device light3 = new Light(electricity, 3, 2);
        Device light4 = new Light(electricity, 4, 2);
        Device light5 = new Light(electricity, 5, 2);
        Device light6 = new Light(electricity, 6, 2);
        Device light7 = new Light(electricity, 7, 2);
        Device light8 = new Light(electricity, 8, 2);
        Device light9 = new Light(electricity, 9, 2);
        Device light10 = new Light(electricity, 10, 2);

        /*6 rooms*/
        Room kitchen = new Room("Kitchen");
        Room living = new Room("Living room");
        Room bedroom = new Room("Bedroom");
        Room wc = new Room("WC");
        Room bedroom2 = new Room("Bedroom");
        Room cabinet = new Room("Cabinet");

        kitchen.addDevice(cooker);
        kitchen.addDevice(fridge);
        kitchen.addDevice(light);
        kitchen.addDevice(f.getDevice("GasKettle", 1));
        kitchen.addDevice(f.getDevice("ElectricKettle", 2));
        kitchen.addAllDevices(f.getDevices("Battery", 2, 1));
        kitchen.addDevice(f.getDevice("Door", 2));


        living.addAllDevices(f.getDevices("Battery", 3, 4));
        living.addDevice(f.getDevice("Computer", 1));
        living.addDevice(f.getDevice("AirConditioning", 1));
        living.addDevice(f.getDevice("Door", 1));
        living.addDevice(light2);

        bedroom.addDevice(f.getDevice("Door", 3));
        bedroom.addDevice(light);
        bedroom.addDevice(light3);

        bedroom2.addDevice(light4);
        bedroom2.addDevice(f.getDevice("Computer", 3));
        bedroom2.addDevice(f.getDevice("Door", 4));

        wc.addAllDevices(f.getDevices("Battery", 3, 7));
        wc.addDevice(f.getDevice("Door", 5));
        wc.addDevice(light5);

        cabinet.addDevice(light6);
        cabinet.addDevice(f.getDevice("Computer", 4));
        cabinet.addDevice(f.getDevice("Door", 6));

        Floor fl = new Floor(1);
        fl.addRoom(living);
        fl.addRoom(kitchen);
        fl.addRoom(bedroom);
        fl.addRoom(wc);

        Floor f2 = new Floor(2);
        f2.addRoom(bedroom2);
        f2.addRoom(cabinet);

        home.addFloor(fl);
        home.addFloor(f2);

        kitchen.getDevices().forEach(d -> d.attach(father));
        kitchen.getDevices().forEach(d -> d.attach(dog));
        living.getDevices().forEach(dev -> dev.attach(mother));
        living.getDevices().forEach(d -> d.attach(daughter));
        cabinet.getDevices().forEach(d -> d.attach(father));
        wc.getDevices().forEach(d -> d.attach(uncle));
        bedroom.getDevices().forEach(d -> d.attach(mother));
        bedroom2.getDevices().forEach(d -> d.attach(aunt));

        Transport bDad = new Bike(father, "Java");
        Transport bMom = new Bike(mother, "Blickle");
        Transport car = new Car(father, "Opel");
        Transport ski = new Ski(daughter, "Sporten");
        home.addTransport(bDad);
        home.addTransport(bMom);
        home.addTransport(car);
        home.addTransport(ski);
    }

}
