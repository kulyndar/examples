/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Broken;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Empty;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.TurnOff;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class Kettle extends Device{
    boolean open;
    int water =100;
    int procOfEmty = 35;

    public Kettle(TypeOfConsume consumption, int deviceId, int procent) {
        super(consumption, deviceId, procent);
        this.state = new TurnOff(this);//nastavit default state - kdy vytvorime ruzne staty
        open = false;
        this.coefOfCons = 3;
    }


    @Override
    public void use(User u){
        if(UserPet.class.isInstance(u)){
            System.out.println("Pet can`t use anything");
            System.out.println("Gav Gav :(");
            System.out.println("");
            return;
        }
        this.user = u;
        if((this.funkcionality-this.prOfFunc)<=0){
            crash();
            //broken
        }
        else if (isEmpty()){
            empty();
        }
        else{
            this.funkcionality-=this.prOfFunc;
            this.water-=this.procOfEmty;
            this.counter++;
        }
    }
    

    @Override
    public void accept(DeviceVisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void crash() {
        this.setState(new Broken(this));
        notifyAllObservers();
    }

    @Override
    public void notify(Observer o) {
        System.out.println("Kettle is broken!");
        o.react(this);
    }


    /**
     * Notify observers, if there is no water in kettle.
     * @param o - observer to be notified
     */
    public void notifyEmpty(Observer o) {
        System.out.println("Kettle is empty! Pour some water!");
          o.reactEmpty(this);
    }
    /**
     * Checks if there is any water in kettle.
     * @return true if there is no water, otherwise false;
     */
    public boolean isEmpty(){
        if (this.water <= 0){
            return true;
        }
        return false;
    }
    /**
     * Function is called, when there is no water. Sets state to Empty and notify users;
     */
    public void empty() {
        this.setState(new Empty(this));
        for (Observer o: observers)
            {this.notifyEmpty(o);
        }

    }
    /**
     * Adds water to the kettle;
     */
    public void emptyRepair() {
        this.state.toNextState();
        this.water = 100;
    }

    @Override
    public String accept(ReportVisitor visitor) {
        return visitor.visit(this); //To change body of generated methods, choose Tools | Template
    }
    
}
