/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.reaction;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class RepairReaction  extends Reaction{

    public RepairReaction(Device d) {
        super(d);
    }

    @Override
    /**
     * Force device to be repaired
     */
    public void execute() {
        this.d.repair();
    }
    
}
