/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Broken;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Empty;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.TurnOff;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.TurnOn;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.*;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class Fridge extends Device {

    boolean open;
    int food = 100;
    int procOfEmty = 20;


    public Fridge(TypeOfConsume consumption, int deviceId, int procent) {
        super(consumption, deviceId, procent);
        this.state = new TurnOn(this);//nastavit default state - kdy vytvorime ruzne staty
        open = false;
        this.coefOfCons = 3;
    }

    @Override
    public void use(User u){
        if(UserPet.class.isInstance(u)){
            System.out.println("Pet can`t use anything");
            System.out.println("Gav Gav :(");
            System.out.println("");
            return;
        }
        this.user = u;
        if((this.funkcionality-this.prOfFunc)<=0){
            crash();
            //broken
        }
        else if (isEmpty()){
            empty();
        }
        else{
            this.funkcionality-=this.prOfFunc;
            this.food-=this.procOfEmty;
            this.counter++;
        }
    }

    @Override
    public void accept(DeviceVisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void crash() {
        this.setState(new Broken(this));
        notifyAllObservers();
    }

    @Override
    public void notify(Observer o) {
        System.out.println("Fridge is broken!");
        o.react(this);
    }


    /**
     * Notify the observe, that there is no food in the fridge.
     * @param o - observer to be notified
     */
    public void notifyEmpty(Observer o) {
        System.out.println("Fridge is empty! Buy some food!");
        o.reactEmpty(this);
    }
    /**
     * Control if there is any food in the fridge.
     * @return true if is no food, otherwise false.
     */
    public boolean isEmpty(){
        if (this.food <= 0){
            return true;
        }
        return false;
    }
    /**
     * Function is called when the fridge is empty. Sets state to Empty and notify users.
     */
    public void empty() {
        this.setState(new Empty(this));
        for (Observer o: observers)
              {this.notifyEmpty(o);
        }

    }
    /**
     * Add food to the fridge.
     */
    public void emptyRepair() {
        this.state.toNextState();
        this.food = 100;
    }

    @Override
    public String accept(ReportVisitor visitor) {
       return visitor.visit(this); //To change body of generated methods, choose Tools | Templates.
    }

}
