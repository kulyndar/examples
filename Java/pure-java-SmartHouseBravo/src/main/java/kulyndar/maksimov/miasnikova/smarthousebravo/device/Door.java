/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Broken;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Room;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class Door extends Device{
    boolean open;

    public Door(TypeOfConsume consumption, int deviceId, int procent) {
        super(consumption, deviceId, procent);
        this.state = null;
        this.open = false;
        this.room = room;
        this.coefOfCons = 0;
    }

    @Override
    public void use(User u) {
        if(UserPet.class.isInstance(u)){
            System.out.println("Pet can use any door ^^");
            System.out.println("Gav Gav :)");
            System.out.println("");
            
        }
        this.user = u;
        if((this.funkcionality-this.prOfFunc)<=0){
            crash();
            //broken
        }else{
            this.funkcionality-=this.prOfFunc;
            this.counter++;
        }
    }
    
    @Override
    public void accept(DeviceVisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void crash() {
        this.setState(new Broken(this));
        notifyAllObservers();
    }

    @Override
    public void notify(Observer o) {
        System.out.println("Door is broken!");
        o.react(this);
    }

    @Override
    public String accept(ReportVisitor visitor) {
        return visitor.visit(this); //To change body of generated methods, choose Tools | Templates.
    }
}
