/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.*;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.*;

import kulyndar.maksimov.miasnikova.smarthousebravo.user.*;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public interface ReportVisitor {

    /**
     * This function write out some information about AirConditioning device
     * @param device
     * @return string to be used in report
     */
    String visit(AirConditioning device);

    /**
     * This function write out some information about Battery device
     * @param device
     * @return string to be used in report
     */
    String visit(Battery device);

    /**
     * This function write out some information about Computer device
     * @param device
     * @return string to be used in report
     */
    String visit(Computer device);

    /**
     * This function write out some information about Cooker device
     * @param device
     * @return string to be used in report
     */
    String visit(Cooker device);

    /**
     * This function write out some information about Fridge device
     * @param device
     * @return string to be used in report
     */
    String visit(Fridge device);


    /**
     * This function write out some information about Kettle device
     * @param device
     * @return string to be used in report
     */
    String visit(Kettle device);


    /**
     * This function write out some information about Door device
     * @param device
     * @return string to be used in report
     */
    String visit(Door device);

    /**
     * This function write out some information about Light device
     * @param device
     * @return string to be used in report
     */
    String visit(Light device);


    /**
     * This function write out some information about MotionSensor device
     * @param device
     * @return string to be used in report
     */
    String visit(MotionSensor device);


    /**
     * This function write out some information about WarmSensor device
     * @param device
     * @return string to be used in report
     */
    String visit(WarmSensor device);

    /**
     * This function write out some information about UserAdault
     * @param user
     * @return string to be used in report
     */
    String visit(UserAdult user);

    /**
     * This function write out some information about UserChild
     * @param user
     * @return string to be used in report
     */
    String visit(UserChild user);

    /**
     * This function write out some information about UserPet
     * @param user
     * @return string to be used in report
     */
    String visit(UserPet user);

    /**
     * This function write out some information about Bike transport
     * @param transport
     * @return string to be used in report
     */
    String visit(Bike transport);

    /**
     * This function write out some information about Car transport
     * @param transport
     * @return string to be used in report
     */
    String visit(Car transport);

    /**
     * This function write out some information about Ski transport
     * @param transport
     * @return  tring to be used in report
     */
    String visit(Ski transport);

}
