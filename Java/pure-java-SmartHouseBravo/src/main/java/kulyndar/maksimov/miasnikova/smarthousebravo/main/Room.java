/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.main;

import java.util.ArrayList;
import java.util.List;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class Room {

    private List<Device> devices;
    private final String name;
    private Floor floor;

    public Room(String name) {
        this.name = name;
        devices = new ArrayList<>();
    }
    /**
     * Add device to the list of devices in that room.
     * @param d - device to be added
     */
    public void addDevice(Device d) {
        devices.add(d);
        d.setRoom(this);
    }
    /**
     * Removes device from the list of devices .
     * @param d - device to be removed
     */
    public void removeDevice(Device d) {
        devices.remove(d);
    }

    public Floor getFloor() {
        return floor;
    }

    public String getName() {
        return name;
    }

    public List<Device> getDevices() {
        return devices;
    }
    /**
     * Forces all devices in that room to accept device visitor.
     * @param visitor - visitor ti be accepted
     */
    public void accept(DeviceVisitor visitor) {
        devices.forEach(d -> d.accept(visitor));
        
    }

    public void setFloor(Floor floor) {
        this.floor = floor;
    }
    /**
     * Add all devices from the list
     * @param list - list, from that devices will be added.
     */
    public void addAllDevices(List<? extends Device> list) {
        list.forEach((device) -> {
            addDevice(device);
        });
    }
    /**
     * Gets the first free device of that type(class)
     * @param c - class(type) of the device
     * @return instance of c class if any found, otherwise null.
     */
    public Device getDevice(Class c) {
        for (Device d : devices) {
            if (c.isInstance(d)) {
                if (d.getUser() == null) {
                    return d;
                }
            }
        }
        return null;
    }

}
