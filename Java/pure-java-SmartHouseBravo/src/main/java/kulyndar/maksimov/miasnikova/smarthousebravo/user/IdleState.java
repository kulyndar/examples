/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.user;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;


/**
 *
 * @author kulyndar;
 */
public class IdleState extends UserState{

    public IdleState(User user) {
        super(user);
        this.state = "Waiting";
    }

    @Override
    public void toNextState() {
        user.setState(new UsingState(user));
    }

   
    
}
