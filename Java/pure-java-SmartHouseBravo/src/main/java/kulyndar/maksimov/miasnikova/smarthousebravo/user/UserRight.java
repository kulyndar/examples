/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.user;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public enum UserRight {
    NOTHING, LIGHT, ALL
}
