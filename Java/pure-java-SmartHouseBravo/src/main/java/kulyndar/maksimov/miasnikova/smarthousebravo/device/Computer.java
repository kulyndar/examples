/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Broken;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.TurnOff;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;


/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class Computer extends Device{

    public Computer(TypeOfConsume consumption, int deviceId, int procent) {
        super(consumption, deviceId, procent);
        this.state = new TurnOff(this);
        this.coefOfCons = 3;
    }
    
    @Override
    public void crash(){
        this.setState(new Broken(this));
        notifyAllObservers();
        //////
    }
    @Override
    public void accept(DeviceVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void notify(Observer o) {
        System.out.println("Computer is broken.");
        o.react(this);
    }

    @Override
    public String accept(ReportVisitor visitor) {
        return visitor.visit(this); //To change body of generated methods, choose Tools | Templates.
    }
}
