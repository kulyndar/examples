package kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Floor;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Home;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Room;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;



/**
 * Created by kater on 09.01.2018.
 */
public class TimeReport {
    private final PrintWriter writer;

    public TimeReport() throws FileNotFoundException {
        File dir = new File("../docs/reports/");
        if(!dir.exists()){
            dir.mkdir();
        }
        this.writer = new PrintWriter(new File("../docs/reports/time-report.txt"));
        this.createReport();
        this.writer.close();

    }
    private void createReport(){
        Home home = Home.getInstance();
        for (Floor floor : home.getFloors()) {
            for (Room room : floor.getRooms()) {
                for (Device device : room.getDevices()) {
                    writer.println(device.accept(new TimeVisitor()));
                }

            }

        }
    }
}
