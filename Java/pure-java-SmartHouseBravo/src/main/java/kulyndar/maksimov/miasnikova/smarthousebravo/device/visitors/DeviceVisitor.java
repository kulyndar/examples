/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.*;



/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public interface DeviceVisitor {
    /**
     * This function write out some information about AirConditioning device
     * @param device
     */
    void visit(AirConditioning device);

    /**
     * This function write out some information about Battery device
     * @param device
     */
    void visit(Battery device);

    /**
     * This function write out some information about Computer device
     * @param device
     */
    void visit(Computer device);

    /**
     * This function write out some information about Cooker device
     * @param device
     */
    void visit(Cooker device);

    /**
     * This function write out some information about Fridge device
     * @param device
     */
    void visit(Fridge device);

    /**
     * This function write out some information about Kettle device
     * @param device
     */
    void visit(Kettle device);

    /**
     * This function write out some information about Door
     * @param device
     */
    void visit(Door device);

    /**
     * This function write out some information about Light device
     * @param device
     */
    void visit(Light device);

    /**
     * This function write out some information about MotionSensor device
     * @param device
     */
    void visit(MotionSensor device);

    /**
     * This function write out some information about WarmSEnsor device
     * @param device
     */
    void visit(WarmSensor device);

    //API na ovladani
}
