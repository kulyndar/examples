/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.manual;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class Manual  implements ManualInterface{
    private final  Device device;
    private String description;

    public Manual(Device device) {
        this.device = device;
        description = "This is Manual for "+device.getClass().getSimpleName()+" number "+device.getDeviceId()+"\nProbably, your device was broken. Read this manual carefully and repair your device. \nSteps......";
        
        System.out.println("A very, very big manual was downloaded");
    }
    
    

    @Override
    public void read() {
        System.out.println(description);
    }
    
}
