/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.*;

import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Bike;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Car;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Ski;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserAdult;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserChild;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class ConsumptionVisitor implements ReportVisitor {

    @Override
    public String visit(AirConditioning device) {
        String str = "AirConditioning number " + device.getDeviceId() + " consumpted " + device.getCounter()*device.getCoefOfCons() + " " + device.
                getConsumption().getMeasureUnit() + ".\n" + "\tIt costs " + (device.getConsumption().getName() == "Gas"
                ? Math.round(device.getCounter() * 6.28*device.getCoefOfCons()):Math.round(device.getCounter() * 3.82*device.getCoefOfCons()))+" Kc";
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Battery device) {
        String str = "Battery number " + device.getDeviceId() + " consumpted " + device.getCounter()*device.getCoefOfCons() + " " + device.
                getConsumption().getMeasureUnit() + ".\n" + "\tIt costs " + (device.getConsumption().getName() == "Gas"
                ? Math.round(device.getCounter() * 6.28*device.getCoefOfCons()):Math.round(device.getCounter() * 3.82*device.getCoefOfCons()))+" Kc";
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Computer device) {
        String str = "Computer number " + device.getDeviceId() + " consumpted " + device.getCounter()*device.getCoefOfCons() + " " + device.
                getConsumption().getMeasureUnit() + ".\n" + "\tIt costs " + (device.getConsumption().getName() == "Gas"
                ? Math.round(device.getCounter() * 6.28):Math.round(device.getCounter() * 3.82))+" Kc";
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Cooker device) {
        String str = "Cooker number " + device.getDeviceId() + " consumpted " + device.getCounter()*device.getCoefOfCons() + " " + device.
                getConsumption().getMeasureUnit() + ".\n" + "\tIt costs " + (device.getConsumption().getName() == "Gas"
                ? Math.round(device.getCounter() * 6.28*device.getCoefOfCons()):Math.round(device.getCounter() * 3.82*device.getCoefOfCons()))+" Kc";
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Fridge device) {
        String str = "Fridge number " + device.getDeviceId() + " consumpted " + device.getCounter()*device.getCoefOfCons() + " " + device.
                getConsumption().getMeasureUnit() + ".\n" + "\tIt costs " + (device.getConsumption().getName() == "Gas"
                ? Math.round(device.getCounter() * 6.28*device.getCoefOfCons()):Math.round(device.getCounter() * 3.82*device.getCoefOfCons()))+" Kc";
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Kettle device) {
       String str = "Kettle number " + device.getDeviceId() + " consumpted " + device.getCounter()*device.getCoefOfCons() + " " + device.
                getConsumption().getMeasureUnit() + ".\n" + "\tIt costs " + (device.getConsumption().getName() == "Gas"
                ? Math.round(device.getCounter() * 6.28*device.getCoefOfCons()):Math.round(device.getCounter() * 3.82*device.getCoefOfCons()))+" Kc";
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Door device) {
        String str = "Door number " + device.getDeviceId() + " consumpted " + device.getCounter()*device.getCoefOfCons() + " " + device.
                getConsumption().getMeasureUnit() + ".\n" + "\tIt costs " + (device.getConsumption().getName() == "Gas"
                ? Math.round(device.getCounter() * 6.28*device.getCoefOfCons()):Math.round(device.getCounter() * 3.82*device.getCoefOfCons()))+" Kc";
        return str;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Light device) {
        String str = "Light number " + device.getDeviceId() + " consumpted " + device.getCounter()*device.getCoefOfCons() + " " + device.
                getConsumption().getMeasureUnit() + ".\n" + "\tIt costs " + (device.getConsumption().getName() == "Gas"
                ? Math.round(device.getCounter() * 6.28*device.getCoefOfCons()):Math.round(device.getCounter() * 3.82*device.getCoefOfCons()))+" Kc";
        return str;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(MotionSensor device) {
        String str = "MotionSensor number " + device.getDeviceId() + " consumpted " + device.getCounter()*device.getCoefOfCons() + " " + device.
                getConsumption().getMeasureUnit() + ".\n" + "\tIt costs " + (device.getConsumption().getName() == "Gas"
                ? Math.round(device.getCounter() * 6.28*device.getCoefOfCons()):Math.round(device.getCounter() * 3.82*device.getCoefOfCons()))+" Kc";
        return str;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(WarmSensor device) {
        String str = "WarmSensor number " + device.getDeviceId() + " consumpted " + device.getCounter()*device.getCoefOfCons() + " " + device.
                getConsumption().getMeasureUnit() + ".\n" + "\tIt costs " + (device.getConsumption().getName() == "Gas"
                ? Math.round(device.getCounter() * 6.28*device.getCoefOfCons()):Math.round(device.getCounter() * 3.82*device.getCoefOfCons()))+" Kc";
        return str;//To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public String visit(UserAdult user) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(UserChild user) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(UserPet user) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Bike transport) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Car transport) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Ski transport) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

}
