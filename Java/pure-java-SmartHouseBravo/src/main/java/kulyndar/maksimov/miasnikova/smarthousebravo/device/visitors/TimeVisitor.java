package kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors;


import kulyndar.maksimov.miasnikova.smarthousebravo.device.*;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Bike;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Car;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Ski;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserAdult;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserChild;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.UserPet;

/**
 * Created by kater on 09.01.2018.
 */
public class TimeVisitor implements ReportVisitor {

    int a = 0;
    int b = 23;
    int c = 59;
    int temp = 0;
    String strTime ="";


    @Override
    public String visit(AirConditioning device) {
        temp = a + (int)(Math.random() * c);
        strTime = Integer.toString(temp);
        if (strTime.length() == 1){
            strTime = "0"+ strTime;
        }
        String str = "AirConditioning number "+device.getDeviceId()+" was used at "+ (a + (int) (Math.random() * b))+":"+strTime;
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Battery device) {
        temp = a + (int)(Math.random() * c);
        strTime = Integer.toString(temp);
        if (strTime.length() == 1){
            strTime = "0"+ strTime;
        }
        String str = "Battery number "+device.getDeviceId()+" was used at "+ (a + (int) (Math.random() * b))+":"+strTime;
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Computer device) {
        temp = a + (int)(Math.random() * c);
        strTime = Integer.toString(temp);
        if (strTime.length() == 1){
            strTime = "0"+ strTime;
        }
        String str = "Computer number "+device.getDeviceId()+" was used at "+ (a + (int) (Math.random() * b))+":"+strTime;
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Cooker device) {
        temp = a + (int)(Math.random() * c);
        strTime = Integer.toString(temp);
        if (strTime.length() == 1){
            strTime = "0"+ strTime;
        }
        String str = "Cooker number "+device.getDeviceId()+" was used at "+ (a + (int) (Math.random() * b))+":"+strTime;
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Fridge device) {
        temp = a + (int)(Math.random() * c);
        strTime = Integer.toString(temp);
        if (strTime.length() == 1){
            strTime = "0"+ strTime;
        }
        String str = "Fridge number "+device.getDeviceId()+" was used at "+ (a + (int) (Math.random() * b))+":"+strTime;
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Kettle device) {
        temp = a + (int)(Math.random() * c);
        strTime = Integer.toString(temp);
        if (strTime.length() == 1){
            strTime = "0"+ strTime;
        }
        String str = "Kettle number "+device.getDeviceId()+" was used at "+ (a + (int) (Math.random() * b))+":"+strTime;
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Door device) {
        temp = a + (int)(Math.random() * c);
        strTime = Integer.toString(temp);
        if (strTime.length() == 1){
            strTime = "0"+ strTime;
        }
        String str = "Door number "+device.getDeviceId()+" was used at "+ (a + (int) (Math.random() * b))+":"+strTime;
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Light device) {
        temp = a + (int)(Math.random() * c);
        strTime = Integer.toString(temp);
        if (strTime.length() == 1){
            strTime = "0"+ strTime;
        }
        String str = "Light number "+device.getDeviceId()+" was used at "+ (a + (int) (Math.random() * b))+":"+strTime;
        return str; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(MotionSensor device) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(WarmSensor device) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public String visit(UserAdult user) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(UserChild user) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(UserPet user) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Bike transport) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Car transport) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visit(Ski transport) {
        return null; //To change body of generated methods, choose Tools | Templates.
    }
}
