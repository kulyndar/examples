/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.user;

/**
 *
 * @author kulyndar;
 */
public abstract class UserState {
     protected String state;
    protected final User user;

    public UserState(User user) {
        this.user = user;
    }
    
    /**
     * Change state to the next one.
     */
    public abstract void toNextState();
    /**
     * Get the name of the state
     * @return name of the state
     */
    public String getState(){
        return this.state;
    };
}
