/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Floor;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Home;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Room;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class ConsumptionReport{
    private final PrintWriter writer;

    public ConsumptionReport() throws FileNotFoundException {
        File dir = new File("../docs/reports/");
        if(!dir.exists()){
            dir.mkdir();
        }
        this.writer = new PrintWriter(new File("../docs/reports/consumption-report.txt"));
        this.createReport();
        this.writer.close();
    }
    
    private void createReport(){
        Home home = Home.getInstance();
        for (Floor floor : home.getFloors()) {
            for (Room room : floor.getRooms()) {
                writer.println(room.getName()+":");
                for (Device device : room.getDevices()) {
                    writer.print("\t");
                    writer.println(device.accept(new ConsumptionVisitor()));
                }
                
            }
            
        }
    }
}
