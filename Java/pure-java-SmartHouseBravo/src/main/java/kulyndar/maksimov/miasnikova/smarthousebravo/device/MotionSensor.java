/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.device;

import java.util.List;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.Broken;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.TurnOn;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.DeviceVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Floor;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Home;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Room;
import kulyndar.maksimov.miasnikova.smarthousebravo.user.User;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public class MotionSensor extends Sensor {
    
    public MotionSensor(TypeOfConsume consumption, int deviceId, int procent) {
        super(consumption, deviceId, procent);
        
    }
    
    
    @Override
    public void accept(DeviceVisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void crash() {
        this.setState(new Broken(this));
        notifyAllObservers();
    }

    @Override
    public void notify(Observer o) {
        System.out.println("MotionSensor is broken!");
        o.react(this);
    }

    @Override
    public String accept(ReportVisitor visitor) {
        return visitor.visit(this); //To change body of generated methods, choose Tools | Templates.
    }
    /**
     * Switches On all lights in the room, if there is any user there
     */
    public static  void light(){
        for (User u : Home.getInstance().getUsers()) {
           Room r = u.getWhereAmI();
            for (Floor floor : Home.getInstance().getFloors()) {
                if(floor.getRooms().contains(r)){
                    List<Device> dev = floor.getRooms().get(floor.getRooms().indexOf(r)).getDevices();
                    for (Device device : dev) {
                        if(device instanceof Light){
                            System.out.println("I turn on Light number "+device.getDeviceId()+" in "+device.getRoom().getName()+".");
                            device.setState(new TurnOn(device));
                        }
                    }
                }
            }
        }
    }
    /**
     * Checks where are users.
     * @return string that describe, where are all users
     */
    public static String whereAreUsers(){
        String str = "";
        for (User u : Home.getInstance().getUsers()) {
            str+="User "+u.getName()+" "+u.getSurname()+" is "+(u.getWhereAmI()==null?"outside":"in "+u.getWhereAmI().getName())+".\n";
        }
        return str;
    }
}
