/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksimov.miasnikova.smarthousebravo.user;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.Device;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Light;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Observable;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.Observer;

import kulyndar.maksimov.miasnikova.smarthousebravo.device.reaction.*;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.TurnOff;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.states.TurnOn;
import kulyndar.maksimov.miasnikova.smarthousebravo.device.visitors.ReportVisitor;
import kulyndar.maksimov.miasnikova.smarthousebravo.main.Room;
import kulyndar.maksimov.miasnikova.smarthousebravo.transport.Transport;

/**
 *
 * @author kulyndar, miasneka, maksiale;
 */
public abstract class User implements Observer {

    UserRight right;

    int idUser;
    String name;
    String surname;

    Room whereAmI;
    Device deviceInUse;
    Transport transportInUse;
    UserState state;

    //TODO - metody
    public User(int idUser, String name, String surname) {
        this.idUser = idUser;
        this.name = name;
        this.surname = surname;
        whereAmI = null; // outside
        deviceInUse = null; //uses nothing
        transportInUse = null;//uses no transport
        state = new IdleState(this);

    }

    /**
     * Change the room where user is.
     *
     * @param r - room, where user moves
     */
    public void goToRoom(Room r) {
        this.whereAmI = r;

    }

    /**
     * Changes user position to null, that means, that user is outside
     */
    public void goOutside() {
        this.whereAmI = null;

    }

    /**
     * Function is called, when user wants to use device. If user already uses any device or transport, he/she finish using that
     * device or transport. User moves to the room, where the device is.
     *
     * @param d - device to be used by user
     */
    public void useDevice(Device d) {
        if (this.state.getState() == "Using") {
            new EndUseTransport(transportInUse, this).execute();
            transportInUse = null;
            endUseDevice(deviceInUse);
            this.state.toNextState();
        }
        this.state.toNextState();
        if (d.getState() instanceof TurnOff) {
            new OnReaction(d).execute();
        }
        deviceInUse = d;
        whereAmI = d.getRoom();
        new UseReaction(d, this).execute();
    }

    /**
     * Function is called, when user wants to finish using device(if uses any). User switches off all lights in the room, where
     * he/she was.
     *
     * @param d - device, that user already use
     */
    public void endUseDevice(Device d) {

        deviceInUse = null;
        if(d!=null){
        new EndUseReaction(d, this).execute();}
        if(whereAmI==null){
            return;
        }
        for (Device device : whereAmI.getDevices()) {
            if (device instanceof Light) {
                device.setState(new TurnOff(device));
            }
        }
    }

    /**
     * Function is called, when user wants to use transport. If user already uses any device or transport, he/she finish using
     * that device or transport. User moves outside.
     *
     * @param t - transport to be used.
     */
    public void useTransport(Transport t) {
        
        if (this.state.getState() == "Using") {
            new EndUseTransport(transportInUse, this).execute();
            transportInUse = null;
            endUseDevice(deviceInUse);
            this.state.toNextState();
        }
        this.state.toNextState();
        if (UserPet.class.isInstance(this)) {
            new UseTransportReaction(t, this).execute();
            return;
        }
        transportInUse = t;
        whereAmI = null;
        new UseTransportReaction(t, this).execute();

    }

    /**
     * Function is called, when user wants to finish using transport(if uses any).
     *
     * @param t - transport, that user already use
     */
    public void endUseTransport(Transport t) {
        if (transportInUse == t) {
            transportInUse = null;
            new EndUseTransport(t, this).execute();
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public UserRight getRight() {
        return right;
    }

    public Transport getTransportInUsed() {
        return transportInUse;
    }

    /**
     * Accepts visitor, that will write down information about user to report
     *
     * @param visitor - visitor to be accepted
     * @return string contained information about user
     */
    public abstract String accept(ReportVisitor visitor);

    public Room getWhereAmI() {
        return whereAmI;
    }

    public UserState getState() {
        return state;
    }

    public void setState(UserState state) {
        this.state = state;
    }

}
