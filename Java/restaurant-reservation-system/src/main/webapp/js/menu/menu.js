/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import React from 'react';
//import styles from '../../newcss.css';


        const menu = (
<div>
    <h1 className="text-center">Menu</h1>
    <div id="t">
        <table className="table" id="ttt">
            <thead>
                <tr>
                    <th></th><th className="text-center tablename">Polévky</th><th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>200g</td><td>Bramborová polévka</td><td>35 Kč</td></tr>
                <tr>
                    <td>150g</td><td>Čočková polévka</td><td>30 Kč</td></tr>
                <tr><td>200g</td><td>Polévka dle denní nabídky</td><td>35 Kč</td></tr>


                <tr>
                    <th></th><th className="text-center tablename">Maso</th><th></th>
                </tr>
                <tr><td>800g</td><td>Vepřové koleno</td><td>250 Kč</td></tr>
                <tr><td>300g</td><td>Kachna</td><td>230 Kč</td></tr>
                <tr><td>500g</td><td>Maďárský huláš </td><td>300 Kč</td></tr>
                <tr>
                    <th></th><th className="text-center tablename">Přílohy</th><th></th>
                </tr>
                <tr><td>300g</td><td>Těstoviny</td><td>60 Kč</td></tr>
                <tr><td>300g</td><td>Ryže</td><td>60 Kč</td></tr>
                <tr><td>300g</td><td>Hranolky</td><td>60 Kč</td></tr>
                <tr>
                    <th></th><th className="text-center tablename">Nápoje</th><th></th>
                </tr>
                <tr><td>0,3l</td><td>Pivo</td><td>40 Kč</td></tr>
                <tr><td>0,25l</td><td>Víno</td><td>60 Kč</td></tr>
                <tr><td>0,04l</td><td>Becherovka</td><td>60 Kč</td></tr>


            </tbody>

        </table>
    </div>
</div>
                );
        
        
        
        export class Menu extends React.Component{

        render(){
        return menu;
        }
        }