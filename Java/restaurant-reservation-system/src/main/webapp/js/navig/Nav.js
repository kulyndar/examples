/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import React from 'react';
import {Link} from "react-router-dom";
import {Logged} from '../js-log/logged';
import Authentication from '../js-log/Authentication';



export class Nav extends React.Component {

    render() {
        const nav = (<nav id="navig" className="navbar navbar-expand-lg navbar-light">
            <span id="name" className="navbar-brand" >Light in the Dark</span>
        
            <div className="navbar-collapse" id="navbarText">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <Link className="nav-link" to="/">Menu</Link>
                    </li>
                    <li className="nav-item">
                        {Nav.renderReserv()}
                    </li>
                    <li className="nav-item">
                        {Nav.renderMyRes()}
                    </li>
                </ul>
                <span className="navbar-text nav-item ">
                    {Nav.renderButton()}
        
                </span>
            </div></nav>);

        return nav;

    }

    static renderButton() {
        if (Nav.getCookie("logged") === "true") {
            return <Link className="nav-link" onClick={Authentication.logout} to="/">LogOut</Link>;
        } else {
            return <Link className="nav-link" to="/login">LogIn</Link>;
        }
    }
    static renderReserv() {
        if (Nav.getCookie("role") === "WA") {
            return <Link className="nav-link" to="/wa/reservation">Reservations</Link>;
        } else if (Nav.getCookie("role") === "UN") {
            return <Link className="nav-link" to="/reservation">New Reservation</Link>;
        } else {
            return <Link className="nav-link" onClick={()=>{alert('Only registrated yousers can make reservation. Please, sign in or sign up');}} to="/">Reservations</Link>;
        }
    }
    static renderMyRes(){
        if (Nav.getCookie("role") === "UN") {
            return <Link className="nav-link" to="/my/reservation">My Reservations</Link>;
        }
        
    }
    
    static getCookie(name) {
        var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

}
;