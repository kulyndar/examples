/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import React from 'react';
import {Link} from "react-router-dom";
import Authentication from "./Authentication";


export class Login extends React.Component{
        constructor(props) {
    super(props);
    this.state = {password: '',
                    login:'' 
                   };
    this.handleChangePass = this.handleChangePass.bind(this);
    this.handleChangeLogin = this.handleChangeLogin.bind(this);
  }
  handleChangePass(event){
      this.setState({password:event.target.value});   
  }
  handleChangeLogin(event){
      this.setState({login:event.target.value});  
  }
    render(){
        const login  = (
            <div>
            <h1 className="text-center">Login</h1>
            <br/>
        <form className="log">
  
    <div className="form-group col-md-12">
      <label htmlFor="login">Login</label>
      <input type="text" name="j_username" onChange={this.handleChangeLogin} className="form-control" id="login" placeholder="Login" />
    </div>
    <div className="form-group col-md-12">
      <label htmlFor="pass">Password</label>
      <input type="password" name="j_password" onChange={this.handleChangePass} className="form-control" id="pass" placeholder="Password" />
    </div>
            <div className="form-row col-md-12 ">
      <div className="form-group col-md-6 ">
      <button type="button" onClick={()=>{Authentication.login(this.state.login, this.state.password);}} className="btn btn-primary">Sign in</button>
    </div>
            <div className="form-group col-md-6 ">
      <Link className="btn" to="/registrate">Sign up</Link>
    </div>
            </div>
</form>
        
        </div>
        );
        return login;   
    };
};