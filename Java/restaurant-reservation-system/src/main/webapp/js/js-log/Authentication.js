'use strict';

import Ajax from "./Ajax";



export default class Authentication {

    static login(username, password) {
        Ajax.post('j_spring_security_check', null, 'form')
                .send('username=' + username).send('password=' + password)
                .end(function (err, resp) {
                    if (err) {
                        alert('Error login');
                        return;
                    }
                    const status = JSON.parse(resp.text);
                    if (!status.success || !status.loggedIn) {
                        alert('Error login');
                        return;
                    }

                    document.cookie = "logged=true";
                    



                    window.location.replace("/ear");
                }.bind(this));
    }

    static logout() {
        Ajax.post('j_spring_security_logout').end(function (err) {
            if (err) {
//                Logger.error('Logout failed. Status: ' + err.status);
            } else {
//                Logger.log('User successfully logged out.');




            }
//            Routing.transitionTo(Routes.login);
            document.cookie = "role=0";
            document.cookie = "logged=false";
            window.location.replace("/ear");
        });
    }
}
