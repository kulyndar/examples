/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import React from 'react';
import Authentication from "./Authentication";
import Ajax from "./Ajax";
//import styles from '../../newcss.css';



export class Registration extends React.Component{
    constructor(props) {
    super(props);
    this.state = {password: '',
                    confirm: '',
                    firstname:'',
                    surname:'',
                    phone:'',
                    mail:'',
                    login:'', 
                    phonePr:'+420'};
    this.handleChangePass = this.handleChangePass.bind(this);
    this.handleChangePasswordC = this.handleChangePasswordC.bind(this);
    this.handleChangeLogin = this.handleChangeLogin.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeSurname = this.handleChangeSurname.bind(this);
    this.handleChangePhone = this.handleChangePhone.bind(this);
    this.handleChangeMail = this.handleChangeMail.bind(this);
    this.handleChangePrefix = this.handleChangePrefix.bind(this);
    this.registrate = this.registrate.bind(this);

    

  }
  handleChangePass(event){
      this.setState({password:event.target.value});   
  }
  handleChangePasswordC(event){
      this.setState({confirm:event.target.value});  
  }
  handleChangeLogin(event){
      this.setState({login:event.target.value});  
  }
  handleChangeName(event){
      this.setState({firstname:event.target.value});  
  }
  handleChangeSurname(event){
      this.setState({surname:event.target.value});  
  }
  handleChangePhone(event){
      this.setState({phone:event.target.value});  
  }
  handleChangeMail(event){
      this.setState({mail:event.target.value});  
  }
  handleChangePrefix(event){
      this.setState({phonePr:event.target.value});  
  }
    render(){
        const reg=(<div>
            <h1 className="text-center">Registration</h1>
            <br/>
        <form className="log">
  <div className="form-row">
    <div className="form-group col-md-4">
      <label htmlFor="login">Login*</label>
      <input type="text" className="form-control" id="login" onChange={this.handleChangeLogin} placeholder="Login" />
    </div>
    <div className="form-group col-md-4">
      <label htmlFor="pass">Password*</label>
      <input type="password" className="form-control" onChange={this.handleChangePass} id="pass" placeholder="Password" />
    </div>
      <div className="form-group col-md-4">
      <label htmlFor="pass2">Password Confirm*</label>
      <input type="password" className="form-control" id="pass2" onChange={this.handleChangePasswordC} placeholder="Password Confirm" />
    </div>
  </div >
    <div className="form-row">
  <div className="form-group col-md-6">
    <label htmlFor="firstname">Name*</label>
    <input type="text" className="form-control" id="fisrtname" onChange={this.handleChangeName} placeholder="Name" />
        </div>
        <div className="form-group col-md-6">
    <label htmlFor="lastname">Surname*</label>
    <input type="text" className="form-control" id="lastname" onChange={this.handleChangeSurname} placeholder="Surname" />    </div>
  </div>
  
            
            <div className="form-row">
  <div className="form-group col-md-6">
    <label htmlFor="mail">Email*</label>
    <input type="email" className="form-control" id="mail" onChange={this.handleChangeMail} placeholder="Email" />
        </div>
       
    <div className="form-group col-md-2">
      <label htmlFor="choose">Prefix</label>
      <select id="choose" onChange={this.handleChangePrefix} className="form-control">
        <option selected>+420</option>
        <option>+421</option>
      </select>
    </div><div className="form-group col-md-4">
    <label htmlFor="phone">Phone*</label>     
     <input type="tel" className="form-control" id="phone" onChange={this.handleChangePhone} placeholder="Phone" />
    </div>
            </div>       
  
  <button onClick={()=>{this.validate()}} className="btn btn-primary">Sign up</button>
</form>
        
        </div>);
        return reg;   
    };
    validate(){
       var validation = true;
            //regular expressions
       var nameRegex = /^[A-Z]{1}[a-z]{1,}$/g;
       var loginPassRegex= /^[A-Za-z0-9]{6,}$/g;
       var mailRegex= /^[A-Za-z0-9]+@[A-Za-z0-9]+\.[a-z]{2,3}$/g;
       var phoneRegex = /^[0-9]{9}$/g;
       
       //find matches
       var nameCheck = this.state.firstname.match(nameRegex);
       var surnameCheck = this.state.surname.match(nameRegex);
       var mailCheck = this.state.mail.match(mailRegex);
       var loginCheck = this.state.login.match(loginPassRegex);
       var passCheck = this.state.login.match(loginPassRegex);
       var phoneCheck = this.state.phone.match(phoneRegex);
        //check
        if(loginCheck===null){
            alert('Login has a wrong format! It can contain lower-case and capital letters or numbers and at least 6.');
            validation=false;
        }
        if(passCheck===null){
            alert('Password has a wrong format! It can contain lower-case and capital letters or numbers and at least 6.');
            validation=false;    
        }
            
        if(this.state.password!==this.state.confirm){
                alert('Password is not the same as confirmation. ');
                validation=false;
        }
        if(nameCheck===null){
           alert('Name has a wrong format! First letter must be capital and at least one lower-case after it.');
           validation=false;
       }
        if(surnameCheck===null){
            alert('Surname has a wrong format! First letter must be capital and at least one lower-case after it.'); 
            validation=false;
        }
        
        if(mailCheck===null){
            alert('E-mail has a wrong format! Please, enter mail in format: aaa@aaa.aa');
            validation=false;
        }
        if(phoneCheck===null){
            alert('Phone has a wrong format! Write down 9 numbers, please.');
            validation=false;
        }
        
        //OK - call function AJAX   
       if(validation){         
                this.registrate();
       }
    }
    
    registrate(){
        //hash password
        
        const userData = {
            username: this.state.firstname+" "+this.state.surname,
            login: this.state.login,
            pass: this.state.password,
            phone: (this.state.phonePr+""+this.state.phone),
            email: this.state.mail
        };
        var exist = false;
        Ajax.post('rest/persons', userData).end(function (body, resp) {
            if (resp.status === 201) {
                
                Authentication.login(userData.login, userData.pass);
            }
        }.bind(this), function (err) {
            alert('SMTH is wrong');
        }.bind(this));
        
        
    }
};

