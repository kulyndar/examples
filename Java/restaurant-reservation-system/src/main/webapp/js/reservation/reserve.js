/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import React from 'react';
        import Ajax from "../js-log/Ajax";
        export class Reserve extends React.Component{


        constructor(props){
        super(props);
                const d = new Date();
                this.state = {dateD:(new Date()).toISOString().substring(0, 10),
                        time:'',
                        restaurant:'Koněvova',
                        tables:[],
                        checked:[]};
                this.handleChangeDate = this.handleChangeDate.bind(this);
                this.handleEmpty = this.handleEmpty.bind(this);
                this.handleChangeTime = this.handleChangeTime.bind(this);
                this.handleChangeRest = this.handleChangeRest.bind(this);
                this.giveMeTables = this.giveMeTables.bind(this);
                this.renderTables = this.renderTables.bind(this);
                this.handleCheckbox = this.handleCheckbox.bind(this);
                this.createReservation = this.createReservation.bind(this);
        }
        handleChangeDate(event){
        if (event.target.value === ""){

        event.target.value = (new Date).toISOString().substring(0, 10);
        }
        this.setState({dateD:event.target.value});
                this.giveMeTables();
        }
        handleChangeTime(event){
        this.setState({time:event.target.value});
                if (event.target.value !== ""){
        this.giveMeTables(); }
        }
        handleChangeRest(event){
        this.setState({restaurant:event.target.value});
                this.giveMeTables();
        }
        handleEmpty(event){
        if (event.target.value === ""){
        event.target.value = (new Date).toISOString().substring(0, 10);
        }

        }
        giveMeTables(){
        if (this.state.time === ""){}
        else{
        const Data = {
        date: this.state.dateD,
                time: this.state.time,
                restaurant: this.state.restaurant
        };
                Ajax.post('rest/reservation/tables', Data).end(function (body, resp) {

        if (body === null){
        } else if (body.length === 0){
        alert('Nic nenajdeno'); }
        else{
                this.renderTables(body);
        }
        }.bind(this), function (err) {
        alert('SMTH is wrong');
        }.bind(this));
        }

        }
        renderTables(body){
        var rows = [];
                for (var i = 0; i < body.length; i++){
        
        rows.push(<div key={i}><label className="containerr">{" "+body[i].description+" Počet míst "+body[i].seats}
    <input type="checkbox" id={body[i].number} onChange={this.handleCheckbox}  />
    <span className="checkmark"></span></label></div>); 
}
this.setState({tables:rows});

}
handleCheckbox(event){
    for(var i=0; i<this.state.checked.length;i++){
        if(this.state.checked[i]===event.target.id){
            this.state.checked.splice(i,1);
            return;
        }
    }
    this.state.checked.push(event.target.id);
    
}
createReservation(){
    if(this.state.checked.length===0){
        alert('You have to choose the table!');
        return;  
    }

    const Data = {
        date: this.state.dateD,
        time: this.state.time,
        restaurant: this.state.restaurant,
        tables:this.state.checked
        };
    Ajax.post('rest/reservation/create', Data).end(function (body, resp) {

        if(resp.status===201){
                alert('Reservation was sucessfully created. You can see it in My Reservations page');
               location.reload();}
        }.bind(this), function (err) {
        alert('SMTH is wrong');
        }.bind(this));
    
}

render(){
                const d = new Date();
                const d2 = new Date(d.getTime() + 604800000 * 2);
                const r = (<div>
    <h1 className="text-center">Reservation</h1>
    <br/>
    <form className="log">
    <div className="form-group col-md-12">
            <label htmlFor="restaurant">Restaurant</label>
            <select id="restaurant" onChange={this.handleChangeRest} className="form-control">
                <option defaultValue>Koněvova</option>
                <option>Invalidovna</option>
            </select>
        </div>
        <div className="form-group col-md-12">
            <label htmlFor="date">Date</label>
            <input type="date" className="form-control" id="date" value={this.state.dateD} onChange={this.handleChangeDate} min={d.toISOString().substring(0, 10)} onClick={this.handleChangeDate} max={d2.toISOString().substring(0, 10)} />
        </div>
        
        
        <div className="form-group col-md-12">
            <label htmlFor="time">Time</label>
            <input type="time" onChange={this.handleChangeTime} onClick={this.handleChangeTime} className="form-control" id="time" min="08:00" max="20:00" />
        </div>
        <div id="tables" >
            {this.state.tables}
        </div>   


        <div className="form-row col-md-12 ">
            <div className="form-group col-md-6 ">
                <button type="submit" onClick={() => {this.createReservation(); }} className="btn btn-primary">Reserve</button>
            </div>

        </div>
    </form>

</div>);
                        return r;
};
}