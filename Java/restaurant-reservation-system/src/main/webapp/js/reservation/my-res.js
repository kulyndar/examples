/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import React from 'react';
import Ajax from '../js-log/Ajax';
import Authentication from '../js-log/Authentication';

export class MyReservations extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            reservations: []
        };
        this.findMy = this.findMy.bind(this);
        this.find = this.find.bind(this);

    }

    findMy() {
        
        Ajax.post('rest/my/reservation', null).end(function (body, resp) {

            if (body === null) {

            } else if (body.length === 0) {
                alert('Nic nenajdeno');
            } else {
                var item = [];
                for (var i = 0; i < body.length; i++) {
                    let status = "";
                    if (body[i].status === 1) {
                        status="alert alert-warning";
                    } else if (body[i].status === 2) {
                        status="alert alert-success";
                    } else {
                        status="alert alert-danger";
                    }
                    var str = body[i].date;
                    item.push(
                            <li key={i} id={str} className="list-group-item"><div className="float-left" ><span  className={status}>{str}</span><span className="text" >{"Restaurant: " + body[i].restaurant + ". Max number of customers: " + body[i].number+". Time of reservation: 2 hours"}</span></div></li>
                            );

                }
                this.setState({reservations: item});

            }
        }.bind(this), function (err) {
            Authentication.logout();
        }.bind(this));

    }
    find(){
        setInterval(this.findMy, 30000);   
    }
    
    render() {
        
        if(this.state.reservations.length===0){
            this.findMy();
        }else{
            this.find();
        }
        return (<div><h1 className="text-center">My reservations</h1><ul className="list-group res">{this.state.reservations}</ul></div>);


    }
    static rerender(){
       location.reload();
        
    }
}
