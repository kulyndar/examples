/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import React from 'react';
import Ajax from "../js-log/Ajax";
import {Nav} from '../navig/Nav';
import {Link} from "react-router-dom";
import {MyReservations} from './my-res';


export class AllReservations extends React.Component{
    
    constructor(props){
        super(props);
        this.findAll = this.findAll.bind(this);
        this.handleCame = this.handleCame.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.state={
            reservations:[]
        };
    }
    render(){
        
        if(Nav.getCookie("role")==="WA"){
            if(this.state.reservations.length===0){
            this.findAll();}
        return (<div><h1 className="text-center">Active reservations</h1><ul className="list-group res">{this.state.reservations}</ul></div>);
        }else{
            return ( <div className="container">
    <div className="row">
        <div className="col-md-12">
            <div className="error-template">
                <h2 className="text-center">
                    Oops!</h2>
                <h2 className="text-center">
                    403 Forbidden</h2>
                <div className="text-center error-details">
                    Sorry, an error has occured, You  cannot access this page!
                </div>
                <div className="text-center error-actions">
                    <Link  className="text-center btn btn-primary btn-lg" to="/">
                        Take Me Home </Link>
                </div>
            </div>
        </div>
    </div>
</div>);
            
        }
        
    }
    handleCame(event){
        alert('Customer came '+event.target.parentElement.parentElement.id);
        var str = event.target.parentElement.parentElement.id;
        var splitted = str.split(" ");
        var Data = {
            date: splitted[0],
            time: splitted[1],
            customer: splitted[2],
            status: 2
        };
        Ajax.put('rest/wa/reservation/update', Data).end(function (body, resp) {
            if(resp.status===200){
                location.reload();  
            }
        }.bind(this), function (err) {
        alert('SMTH is wrong');
        }.bind(this));
        
       
        
    }
    handleCancel(event){
        alert('Cancelled '+event.target.parentElement.parentElement.id);
        var str = event.target.parentElement.parentElement.id;
        var splitted = str.split(" ");
        var Data = {
            date: splitted[0],
            time: splitted[1],
            customer: splitted[2],
            status: 3
        };
        Ajax.put('rest/wa/reservation/update', Data).end(function (body, resp) {
            if(resp.status===200){
                location.reload();   
            }
        }.bind(this), function (err) {
        alert('SMTH is wrong');
        }.bind(this));
        
    }
    findAll(){
        Ajax.post('rest/wa/reservation', null).end(function (body, resp) {

        if (body === null){
        } else if (body.length === 0){
        alert('Nic nenajdeno'); }
        else{
            var item = [];
        for(var i=0; i <body.length; i++){
            
            var str = body[i].timeFrom;
            
            str+=" "+body[i].login;
            item.push(
                    <li key={i} id={str} className="list-group-item"><div className="who float-left" ><span  className="alert alert-info">{body[i].timeFrom}</span><span className="text" >{"Customer: "+body[i].customer+". Tables: "+body[i].tables}</span></div><div className="float-right"><button onClick={this.handleCame} className="btn btn-success">
                Customer came</button><button onClick={this.handleCancel} className="btn btn-danger">Cancell</button></div></li>
                            );
            
        }
        this.setState({reservations: item});
        
        }
        }.bind(this), function (err) {
        alert('SMTH is wrong');
        }.bind(this));
        
    }
}