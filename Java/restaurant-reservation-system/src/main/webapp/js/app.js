/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import {Menu} from './menu/menu';
import {Registration} from './js-log/reg';
import {Login} from './js-log/login';
import {Reserve} from './reservation/reserve';
import {Route, HashRouter, Switch} from "react-router-dom";
import {IntlProvider} from "react-intl";
//import {Link} from 'react-router';
import {Nav} from './navig/Nav';
import {AllReservations} from './reservation/wa-res';
import {MyReservations} from './reservation/my-res';

class App extends React.Component {
    constructor(props) {
        super(props);

    } 

    
    render() {
        return (
                <IntlProvider>
                
                <HashRouter>
                <div>
                <Nav/>
                    <Switch> 
                    <Route exact path="/" component={Menu}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/registrate" component={Registration}/>
                    <Route path="/reservation" component={Reserve}/>
                    <Route path="/wa/reservation" component={AllReservations} />
                    <Route path="/my/reservation" component={MyReservations} />
                    </Switch>
                    </div>
                </HashRouter>
                </IntlProvider>
                );
    }
    ;
}
;


ReactDOM.render(<App/>, document.getElementById('middle'));
