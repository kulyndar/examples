/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.rest;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kulyndar;
 */
@XmlRootElement
class ReservationToSendMy implements Serializable {
    
    String restaurant;
    int number;
    String date;
    int status;

    public ReservationToSendMy(String restaurant, int number, String date, int status) {
        this.restaurant = restaurant;
        this.number = number;
        this.date = date;
        this.status = status;
    }

    public ReservationToSendMy() {
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
}
