/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.rest;

import java.net.URI;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import kulyndar.maksiale.restaurant.model.Place;
import kulyndar.maksiale.restaurant.model.UserNormal;
import kulyndar.maksiale.restaurant.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author kulyndar;
 */
@RestController
@RequestMapping("/reservation/tables")
public class GiveMeTables {

    @Autowired
    ReservationService service;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<PlaceToSend> giveTables(@RequestBody GiveTable t) {
        String target = t.date + " " + t.time;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date result = new Date();
        try {
            result = df.parse(target);

        } catch (ParseException ex) {
            Logger.getLogger(GiveMeTables.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<Place> tables = service.giveTable(result, t.restaurant);
        List<PlaceToSend> toSend = new ArrayList<>();
        for (Place table : tables) {
            toSend.add(new PlaceToSend(table.getTablenumber(), table.getDescription(), table.getNumberofseats()));
        }
//        return tables;
        return toSend;


    }

//    private void sendAnswer(List<Place> p, HttpServletResponse response){
//        
//    }
}

class GiveTable {

    String date;
    String time;
    String restaurant;

    public GiveTable(String date, String time, String restaurant) {
        this.date = date;
        this.time = time;
        this.restaurant = restaurant;
    }

    public GiveTable() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

}
