/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have
 * received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package kulyndar.maksiale.restaurant.service.authentication;

import kulyndar.maksiale.restaurant.model.UserNormal;
import kulyndar.maksiale.restaurant.dao.UserNormalDao;
import kulyndar.maksiale.restaurant.dao.WaiterDao;
import kulyndar.maksiale.restaurant.model.Waiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final UserNormalDao personDao;
    private final WaiterDao waiterDao;

    @Autowired
    public UserDetailsService(UserNormalDao personDao, WaiterDao waiterDao) {
        this.personDao = personDao;
        this.waiterDao = waiterDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (personDao.existsUser(username)) {
            final UserNormal person = personDao.findByLogin(username);
            if (person == null) {
                throw new UsernameNotFoundException("User with username " + username + " not found.");
            }
            return new kulyndar.maksiale.restaurant.service.model.UserDetails(person);
        } else if (waiterDao.existsWaiter(username)) {
            final Waiter person = waiterDao.findWaiter(username);
            if (person == null) {
                throw new UsernameNotFoundException("User with username " + username + " not found.");
            }
            return new kulyndar.maksiale.restaurant.service.model.UserDetails(person);
        } else {
            throw new UsernameNotFoundException("User with username " + username + " not found.");
        }
    }
}
