/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import kulyndar.maksiale.restaurant.dao.PlaceDao;
import kulyndar.maksiale.restaurant.dao.RestaurantDao;
import kulyndar.maksiale.restaurant.model.Place;
import kulyndar.maksiale.restaurant.model.Reservation;
import kulyndar.maksiale.restaurant.model.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kulyndar, maksiale;
 */
@Service
public class PlaceService {

    @Autowired
    PlaceDao dao;

    @Autowired
    RestaurantDao daoRest;

    @Autowired
    ReservationService service;

    @Transactional
    public void createNewTable(String name, int seats, Restaurant r, String desc) {
        Place p = new Place();
        p.setDescription(desc);
        p.setNumberofseats(seats);
        p.setRestaurant(r);
        p.setTablenumber(name);
        dao.addPlace(p);
    }

    @Transactional(readOnly = true)
    public List<Place> findPlaceByRestaurant(Restaurant r) {
        if (daoRest.existsRestaurant(r.getNameofrestaurant())) {
            return dao.findPlaceInRestaurant(r);
        } else {
            //exception
            return null;
        }
    }

    @Transactional(readOnly = true)
    public List<Place> findBySeatsAndRestaurant(int s, Restaurant r) {
        return dao.findPlaceByRestaurantAndSeats(r, s);

    }

    @Transactional(readOnly = true)
    public Place findPlace(String name) {
        Objects.requireNonNull(name);
        return dao.findPlaceByName(name);
    }

    public List<Place> findFree(Date timeFrom, int seats, String restaurant) {
        Restaurant r = daoRest.findRestaurantByName(restaurant);
        List<Place> places = findBySeatsAndRestaurant(seats, r);
        List<Reservation> reservations = service.findActive();
        List<Place> toReturn = new ArrayList<>();
        List<Reservation> res = reservations.stream().filter(t -> t.getReservationPK().getTimefrom() == timeFrom).collect(
                Collectors.toList());
        places.forEach((place) -> {
            if (res.stream().map(t -> t.getPlaceCollection()).anyMatch(col -> col.contains(place))) {

            } else {
                toReturn.add(place);
            }
        });

        return toReturn;
    }

}
