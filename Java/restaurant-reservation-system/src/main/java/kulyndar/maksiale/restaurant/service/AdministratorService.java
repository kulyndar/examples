/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.service;

import kulyndar.maksiale.restaurant.dao.AdministratorDao;
import kulyndar.maksiale.restaurant.model.Administrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kulyndar, maksiale;
 */
@Service
public class AdministratorService extends PassSalt{

    AdministratorDao dao;

    
    @Autowired
    public AdministratorService(AdministratorDao dao) {
        this.dao = dao;
    }
    
    @Transactional(readOnly = true)
    public boolean login(String login, String password) {
        if (dao.existsAdmin(login)) {
            password = getSHA512SecurePassword(password);
            Administrator a = dao.findByLoginPassword(login, password);
            if (a != null) {
                //current user - waiter
                return true;
            }
            return false;
        }
        return false;
    }

}
