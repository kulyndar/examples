/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;

import java.util.ArrayList;
import kulyndar.maksiale.restaurant.model.Waiter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import kulyndar.maksiale.restaurant.model.Place;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kulyndar, maksiale;
 */
@Repository
public class WaiterDao {

    @PersistenceContext
    EntityManager em;

    /**
     * Adds this waiter to DB
     * @param waiter that must be added
     */
    public void persistWaiter(Waiter waiter) {
        em.persist(waiter);
    }

    /**
     * Deletes this waiter from DB
     * @param waiter that must be removed
     */
    public void deleteWaiter(Waiter waiter) {
        em.remove(waiter);
    }

    public Waiter findWaiter(String login) {
        return em.find(Waiter.class, login);
    }

    /**
     * Finds out if this waiter exists
     * @param login that must be found
     * @return true if this waiter exists
     */
    public boolean existsWaiter(String login) {
        try {
            
            Waiter w = em.find(Waiter.class, login);
            return w != null;
        
        } catch (Exception e) {
            return false;
        }
    }
    
    
    /**
     * Finds a waiter with his login and password
     * @param login that must be found
     * @param password that must be found
     * @return waiter with this login and password
     */
    public Waiter findByLoginPassword(String login, String password){
        List<Waiter> users = em.createNamedQuery("Waiter.findByLoginPass").setParameter("login", login).setParameter(
                "pass", password).getResultList();
        if(!users.isEmpty()){
            return users.get(0);
        }
        return null;
    
    }
    
    /**
     * Find list of waiters by their places
     * @param place that must be found
     * @return list of waiters
     */
    public List<Waiter> findByPlace(String place){
        List<Waiter> waiters = em.createNamedQuery("Waiter.findAll").getResultList();
        List<Waiter> toReturn = new ArrayList<>();
        for (Waiter w: waiters) {
            List<Place> places = (List<Place>) w.getPlaceCollection();
            for (Place p : places) {
                if(p.getTablenumber().equals(place)){
                    toReturn.add(w);
                    break;
                }
            }
            
        }
        return toReturn;
    }
    

}
