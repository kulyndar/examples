/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kulyndar, maksiale;
 */
@Entity
@Table(name = "user_normal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserNormal.findAll", query = "SELECT u FROM UserNormal u")
    ,
        @NamedQuery(name = "UserNormal.findByLogin", query = "SELECT u FROM UserNormal u WHERE u.login = :login")
    ,
        @NamedQuery(name = "UserNormal.findByPass", query = "SELECT u FROM UserNormal u WHERE u.pass = :pass")
    ,
        @NamedQuery(name = "UserNormal.findByEmail", query = "SELECT u FROM UserNormal u WHERE u.email = :email")
    ,
        @NamedQuery(name = "UserNormal.findByPhone", query = "SELECT u FROM UserNormal u WHERE u.phone = :phone")
    ,
        @NamedQuery(name = "UserNormal.findByUsername", query = "SELECT u FROM UserNormal u WHERE u.username = :username"),
        @NamedQuery(name = "UserNormal.findByLoginPassword", query = "SELECT u FROM UserNormal u WHERE u.login = :login AND u.pass = :pass"),
})
public class UserNormal extends User implements Serializable {

    private static final long serialVersionUID = 1L;
    
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    
    @Column(name = "email")
    private String email;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
   
    @Column(name = "phone")
    private String phone;
    @Basic(optional = false)
   
    @Column(name = "username")
    private String username;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userNormal")
    private Collection<Reservation> reservationCollection;

    public UserNormal() {
    }

    public UserNormal(String login) {
        this.login = login;
    }

    public UserNormal(String login, String pass, String email, String phone, String username) {
        this.login = login;
        this.pass = pass;
        this.email = email;
        this.phone = phone;
        this.username = username;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @XmlTransient
    public Collection<Reservation> getReservationCollection() {
        return reservationCollection;
    }

    public void setReservationCollection(Collection<Reservation> reservationCollection) {
        this.reservationCollection = reservationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (login != null ? login.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserNormal)) {
            return false;
        }
        UserNormal other = (UserNormal) object;
        if ((this.login == null && other.login != null) || (this.login != null && !this.login.equals(other.login))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "kulyndarmaksiale.resteuraceear.model.UserNormal[ login=" + login + " ]";
    }
    
}
