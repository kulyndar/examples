/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.springframework.beans.factory.annotation.Configurable;

/**
 *
 * @author kulyndar, maksiale;
 */
@Entity
@Table(name = "restaurant")
@XmlRootElement
@Configurable
@NamedQueries({
    @NamedQuery(name = "Restaurant.findAll", query = "SELECT r FROM Restaurant r")
    ,
        @NamedQuery(name = "Restaurant.findByNameofrestaurant", query
            = "SELECT r FROM Restaurant r WHERE r.nameofrestaurant = :nameofrestaurant")
    ,
        @NamedQuery(name = "Restaurant.findByCity", query = "SELECT r FROM Restaurant r WHERE r.city = :city")
    ,
        @NamedQuery(name = "Restaurant.findByStreet", query = "SELECT r FROM Restaurant r WHERE r.street = :street")
    ,
        @NamedQuery(name = "Restaurant.findByPostindex", query = "SELECT r FROM Restaurant r WHERE r.postindex = :postindex")})
public class Restaurant implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    
    @Column(name = "nameofrestaurant")
    private String nameofrestaurant;
    @Basic(optional = false)
    
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
   
    @Column(name = "street")
    private String street;
    @Basic(optional = false)
    
    @Column(name = "postindex")
    private String postindex;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "restaurant")
    private Collection<Place> placeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "restaurant")
    private Collection<Waiter> waiterCollection;

    public Restaurant() {
    }

    public Restaurant(String nameofrestaurant) {
        this.nameofrestaurant = nameofrestaurant;
    }

    public Restaurant(String nameofrestaurant, String city, String street, String postindex) {
        this.nameofrestaurant = nameofrestaurant;
        this.city = city;
        this.street = street;
        this.postindex = postindex;
    }

    public String getNameofrestaurant() {
        return nameofrestaurant;
    }

    public void setNameofrestaurant(String nameofrestaurant) {
        this.nameofrestaurant = nameofrestaurant;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostindex() {
        return postindex;
    }

    public void setPostindex(String postindex) {
        this.postindex = postindex;
    }

    @XmlTransient
    public Collection<Place> getPlaceCollection() {
        return placeCollection;
    }

    public void setPlaceCollection(Collection<Place> placeCollection) {
        this.placeCollection = placeCollection;
    }

    @XmlTransient
    public Collection<Waiter> getWaiterCollection() {
        return waiterCollection;
    }

    public void setWaiterCollection(Collection<Waiter> waiterCollection) {
        this.waiterCollection = waiterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nameofrestaurant != null ? nameofrestaurant.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Restaurant)) {
            return false;
        }
        Restaurant other = (Restaurant) object;
        if ((this.nameofrestaurant == null && other.nameofrestaurant != null) ||
                (this.nameofrestaurant != null && !this.nameofrestaurant.equals(other.nameofrestaurant))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "kulyndarmaksiale.resteuraceear.model.Restaurant[ nameofrestaurant=" + nameofrestaurant + " ]";
    }
    
}
