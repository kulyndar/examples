/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kulyndar, maksiale;
 */
@Entity
@Table(name = "waiter")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Waiter.findAll", query = "SELECT w FROM Waiter w")
    ,
        @NamedQuery(name = "Waiter.findByLogin", query = "SELECT w FROM Waiter w WHERE w.login = :login")
    ,
        @NamedQuery(name = "Waiter.findByPass", query = "SELECT w FROM Waiter w WHERE w.pass = :pass")
    ,
        @NamedQuery(name = "Waiter.findByShift", query = "SELECT w FROM Waiter w WHERE w.shift = :shift")
    ,
        @NamedQuery(name = "Waiter.findByWaitername", query = "SELECT w FROM Waiter w WHERE w.waitername = :waitername"),
@NamedQuery(name = "Waiter.findByLoginPass", query = "SELECT w FROM Waiter w WHERE w.login = :login AND w.pass= :pass")})
public class Waiter extends User implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Basic(optional = false)
   
    @Column(name = "shift")
    private int shift;
    @Basic(optional = false)
    
    @Column(name = "waitername")
    private String waitername;
    @ManyToMany(mappedBy = "waiterCollection")
    private Collection<Place> placeCollection;
    @OneToMany(mappedBy = "waiter")
    private Collection<Reservation> reservationCollection;
    @JoinColumn(name = "restaurant", referencedColumnName = "nameofrestaurant")
    @ManyToOne(optional = false)
    private Restaurant restaurant;

    public Waiter() {
    }

    public Waiter(String login) {
        this.login = login;
    }

    public Waiter(String login, String pass, int shift, String waitername) {
        this.login = login;
        this.pass = pass;
        this.shift = shift;
        this.waitername = waitername;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getShift() {
        return shift;
    }

    public void setShift(int shift) {
        this.shift = shift;
    }

    public String getWaitername() {
        return waitername;
    }

    public void setWaitername(String waitername) {
        this.waitername = waitername;
    }

    @XmlTransient
    public Collection<Place> getPlaceCollection() {
        return placeCollection;
    }

    public void setPlaceCollection(Collection<Place> placeCollection) {
        this.placeCollection = placeCollection;
    }

    @XmlTransient
    public Collection<Reservation> getReservationCollection() {
        return reservationCollection;
    }

    public void setReservationCollection(Collection<Reservation> reservationCollection) {
        this.reservationCollection = reservationCollection;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (login != null ? login.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Waiter)) {
            return false;
        }
        Waiter other = (Waiter) object;
        if ((this.login == null && other.login != null) || (this.login != null && !this.login.equals(other.login))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "kulyndarmaksiale.resteuraceear.model.Waiter[ login=" + login + " ]";
    }
    
}
