/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.service;

import java.util.Objects;
import kulyndar.maksiale.restaurant.dao.AdministratorDao;
import kulyndar.maksiale.restaurant.dao.UserNormalDao;
import kulyndar.maksiale.restaurant.dao.WaiterDao;
import kulyndar.maksiale.restaurant.model.UserNormal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kulyndar, maksiale;
 */
@Service
public class UserNormalService extends PassSalt {

    @Autowired
    AdministratorDao daoAdmin;

    @Autowired
    WaiterDao daoWaiter;
    
    UserNormalDao dao;
    
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserNormalService(UserNormalDao dao) {
        this.dao = dao;
    }

    @Transactional
    public void createNewUser(String login, String pass, String name, String email, String phone) {

        Objects.requireNonNull(login);
        Objects.requireNonNull(pass);
        Objects.requireNonNull(name);
        Objects.requireNonNull(phone);
        Objects.requireNonNull(email);

        if (dao.existsEmail(email)) {
            //this email already exists
        }
        if (dao.existsPhone(phone)) {
            //this phone already exists
        }
        if (dao.existsUser(login)||daoAdmin.existsAdmin(login)||daoWaiter.existsWaiter(login)) {
            //exception

        }

        UserNormal user = new UserNormal();
        user.setLogin(login);

        pass = getSHA512SecurePassword(pass);//password salt

        user.setPass(pass);
        user.setPhone(phone);
        user.setEmail(email);
        user.setUsername(name);

        dao.persistUser(user);
    }
    public boolean existsUser(UserNormal user){
        return dao.existsUser(user.getLogin());
    }
    public boolean existsPhone(UserNormal user){
        return dao.existsPhone(user.getPhone());
    }
    public boolean existsMail(UserNormal user){
        return dao.existsEmail(user.getEmail());
    }
    @Transactional
    public void createNewUser(UserNormal user) {
//        String pass = getSHA512SecurePassword(user.getPass());//password salt
//
        user.setPass(passwordEncoder.encode(user.getPass()));
        

        dao.persistUser(user);
    }
    

    @Transactional(readOnly = true)
    public boolean login(String login, String password) {
        if (dao.existsUser(login)) {
            password = getSHA512SecurePassword(password);
            UserNormal u = dao.findByLoginPassword(login, password);
            if (u != null) {
                //current user - waiter
                return true;
            }
            return false;
        }
        return false;
    }

    public void changePhone(String phone, String newPhone) {
        UserNormal u = dao.findByPhone(phone);
        u.setPhone(newPhone);
        dao.persistUser(u);
    }
    
    

}
