package kulyndar.maksiale.restaurant.rest;


import kulyndar.maksiale.restaurant.model.Place;
import kulyndar.maksiale.restaurant.model.Restaurant;
import kulyndar.maksiale.restaurant.model.UserNormal;
import kulyndar.maksiale.restaurant.model.Waiter;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class DtoMapper {


    public abstract UserNormal userNormalDtoToUserNormal(UserNormalDto dto);
    public abstract Waiter waiterDtoToWaiter(WaiterDto dto);
    public abstract Place placeDtoToPlace(PlaceDto dto);
    public abstract Restaurant restaurantDtoToRestaurant(RestaurantDto dto);
}
