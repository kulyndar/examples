/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import kulyndar.maksiale.restaurant.service.ReservationService;
import kulyndar.maksiale.restaurant.service.authentication.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kulyndar;
 */
@RestController
@RequestMapping("/reservation/create")
public class createReservation {
    @Autowired
    ReservationService service;
    
    @Autowired
    private SecurityUtils securityUtils;
    
    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> reserve(@RequestBody Res res ){
        /*Setting time of reservation*/
        String target = res.date + " " + res.time;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date timeFrom = new Date();
        try {
            timeFrom = df.parse(target);
        } catch (ParseException ex) {
            Logger.getLogger(GiveMeTables.class.getName()).log(Level.SEVERE, null, ex);
        }
        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(timeFrom); // sets calendar time/date
        cal.add(Calendar.HOUR_OF_DAY, 2); // adds one hour
        Date timeTo = cal.getTime();
        ///
        if(service.createReservation(securityUtils.getCurrentUser(), timeFrom, timeTo, res.tables, res.restaurant)){
             return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }
}

class Res{
    String date;
    String time;
    String restaurant;
    String [] tables;

    public Res(String date, String time, String restaurant, String[] tables) {
        this.date = date;
        this.time = time;
        this.restaurant = restaurant;
        this.tables = tables;
    }

    public Res() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public String[] getTables() {
        return tables;
    }

    public void setTables(String[] tables) {
        this.tables = tables;
    }
    
    
}
