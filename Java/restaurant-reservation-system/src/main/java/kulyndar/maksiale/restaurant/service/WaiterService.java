/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kulyndar.maksiale.restaurant.dao.AdministratorDao;
import kulyndar.maksiale.restaurant.dao.RestaurantDao;
import kulyndar.maksiale.restaurant.dao.UserNormalDao;
import kulyndar.maksiale.restaurant.dao.WaiterDao;
import kulyndar.maksiale.restaurant.model.Place;
import kulyndar.maksiale.restaurant.model.Restaurant;
import kulyndar.maksiale.restaurant.model.Waiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kulyndar, maksiale;
 */
@Service
public class WaiterService extends PassSalt {
    
    @Autowired
    AdministratorDao daoAdmin;

   
    @Autowired
    UserNormalDao daoUser;
    @Autowired
    WaiterDao dao;
    
    @Autowired
    RestaurantDao daoR;
    
        
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Creates new waiter
     * @param login - waiter`s login, not null
     * @param pass - waiter`s password, not null
     * @param name - waiter`s name, not null
     * @param shift - even or odd shift, not null
     * @param restaurant - restaurant, in which waiter works
     */
    @Transactional
    public void createNewWaiter(String login, String pass, String name, int shift,
            Restaurant restaurant) {
        Objects.requireNonNull(login);
        Objects.requireNonNull(pass);
        Objects.requireNonNull(restaurant);
        Objects.requireNonNull(name);
        Objects.requireNonNull(shift);
        if (dao.existsWaiter(login)||daoUser.existsUser(login)||daoAdmin.existsAdmin(login)) {
            //exception
            throw new RuntimeException("Such login already exists");//would be change

        }
        Waiter w = new Waiter();
        w.setLogin(login);

        pass = getSHA512SecurePassword(pass);//password salt

        w.setPass(pass);
        w.setRestaurant(restaurant);
        w.setWaitername(name);
        w.setShift(shift);
        dao.persistWaiter(w);

    }
    @Transactional
    public boolean createNewWaiter(Waiter w) {
        if (dao.existsWaiter(w.getLogin())||daoUser.existsUser(w.getLogin())||daoAdmin.existsAdmin(w.getLogin())) {
            //exception
            return false;

        }
        

        w.setPass(passwordEncoder.encode(w.getPass()));

        dao.persistWaiter(w);
        return true;

    }
    /**
     * Deletes waiter with that login
     * @param login - login of waiter to delete
     */
    @Transactional
    public void deleteWaiter(String login) {
        Waiter w = dao.findWaiter(login);
        dao.deleteWaiter(w);
    }
    /**
     * Add a working place to waiter
     * @param login - login of waiter
     * @param place - place to be added
     */
    @Transactional
    public void addPlace(String login, Place place) {
        Waiter w = dao.findWaiter(login);
        w.setPlaceCollection(new ArrayList<>());
        w.getPlaceCollection().add(place);
        dao.persistWaiter(w);
    }
    /**
     * Authentication
     * @param login
     * @param password
     * @return true if authentication is successful, otherwise false
     */
    @Transactional(readOnly = true)
    public boolean login(String login, String password) {
        if (dao.existsWaiter(login)) {
            password = getSHA512SecurePassword(password);
            Waiter w = dao.findByLoginPassword(login, password);
            if (w != null) {
                //current user - waiter
                return true;
            }
            return false;
        }
        return false;
    }
    /**
     * Change waiter`s shift from even to odd, or from odd to even
     * @param login - login of waiter
     */
    @Transactional
    public void changeShift(String login) {
        Waiter w = dao.findWaiter(login);
        if (w.getShift() ==1) {
            w.setShift(2);
        } else {
            w.setShift(1);
        }
        dao.persistWaiter(w);
    }
    /**
     * Finds waiter by working place
     * @param place - place
     * @return list of waiters
     */
    @Transactional
    public List<Waiter> findByPlace(Place place){
       return dao.findByPlace(place.getTablenumber());
        
    }

}
