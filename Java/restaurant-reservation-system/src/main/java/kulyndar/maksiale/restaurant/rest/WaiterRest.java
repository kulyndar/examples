/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.rest;

import java.net.URI;
import kulyndar.maksiale.restaurant.model.UserNormal;
import kulyndar.maksiale.restaurant.model.Waiter;
import kulyndar.maksiale.restaurant.service.WaiterService;
import kulyndar.maksiale.restaurant.service.authentication.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author kulyndar;
 */
@RestController
@RequestMapping("/waiter")
public class WaiterRest {

    @Autowired
    private WaiterService waiterService;
    @Autowired
    private SecurityUtils securityUtils;
    @Autowired
    private DtoMapper dtoMapper;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createPerson(@RequestBody Waiter person) {

        if (waiterService.createNewWaiter(person)) {
            final URI location = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(
                    person.getLogin()).toUri();
            final HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.LOCATION, location.toASCIIString());

            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } else {
            final URI location = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(
                    person.getLogin()).toUri();
            final HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.LOCATION, location.toASCIIString());
            return new ResponseEntity<>(headers, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
