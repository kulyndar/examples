/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.rest;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kulyndar;
 */
@XmlRootElement
public class PlaceToSend implements Serializable{
    String number;
    String description;
    int seats;

    public PlaceToSend(String number, String description, int seats) {
        this.number = number;
        this.description = description;
        this.seats = seats;
    }

    public PlaceToSend() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
    
    
    
}
