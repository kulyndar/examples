/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.model;

import java.io.Serializable;
import java.lang.annotation.Native;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kulyndar, maksiale;
 */
@Entity
@Table(name = "place")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Place.findAll", query = "SELECT p FROM Place p")
    ,
        @NamedQuery(name = "Place.findByTablenumber", query = "SELECT p FROM Place p WHERE p.tablenumber = :tablenumber")
    ,
        @NamedQuery(name = "Place.findByDescription", query = "SELECT p FROM Place p WHERE p.description = :description")
    ,
        @NamedQuery(name = "Place.findByNumberofseats", query = "SELECT p FROM Place p WHERE p.numberofseats = :numberofseats"),
@NamedQuery(name = "Place.findByRestaurant", query = "SELECT p FROM Place p WHERE p.restaurant = :restaurant")
})

public class Place implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
   
    @Column(name = "tablenumber")
    private String tablenumber;
    @Basic(optional = false)
    
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
 
    @Column(name = "numberofseats")
    private int numberofseats;
    
    
    @ManyToMany(mappedBy = "placeCollection")
    @OrderBy("timeto ASC")
    private Collection<Reservation> reservationCollection;
    @JoinTable(name = "waiter_place", joinColumns = {
        @JoinColumn(name = "tablenumber", referencedColumnName = "tablenumber")}, inverseJoinColumns
            = {
                @JoinColumn(name = "waiter", referencedColumnName = "login")})
    @ManyToMany
    private Collection<Waiter> waiterCollection;
    @JoinColumn(name = "restaurant", referencedColumnName = "nameofrestaurant")
    @ManyToOne(optional = false)
    private Restaurant restaurant;

    public Place() {
    }

    public Place(String tablenumber) {
        this.tablenumber = tablenumber;
    }

    public Place(String tablenumber, String description, int numberofseats) {
        this.tablenumber = tablenumber;
        this.description = description;
        this.numberofseats = numberofseats;
    }

    public String getTablenumber() {
        return tablenumber;
    }

    public void setTablenumber(String tablenumber) {
        this.tablenumber = tablenumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberofseats() {
        return numberofseats;
    }

    public void setNumberofseats(int numberofseats) {
        this.numberofseats = numberofseats;
    }

    @XmlTransient
    public Collection<Reservation> getReservationCollection() {
        return reservationCollection;
    }

    public void setReservationCollection(Collection<Reservation> reservationCollection) {
        this.reservationCollection = reservationCollection;
    }

    @XmlTransient
    public Collection<Waiter> getWaiterCollection() {
        return waiterCollection;
    }

    public void setWaiterCollection(Collection<Waiter> waiterCollection) {
        this.waiterCollection = waiterCollection;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tablenumber != null ? tablenumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Place)) {
            return false;
        }
        Place other = (Place) object;
        if ((this.tablenumber == null && other.tablenumber != null) ||
                (this.tablenumber != null && !this.tablenumber.equals(other.tablenumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "kulyndarmaksiale.resteuraceear.model.Place[ tablenumber=" + tablenumber + " ]";
    }
    
}
