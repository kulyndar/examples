/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
package kulyndar.maksiale.restaurant.service.model;

import kulyndar.maksiale.restaurant.model.UserNormal;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.*;

public class UserDetails_2 {//implements org.springframework.security.core.userdetails.UserDetails {

//    private static final String DEFAULT_ROLE = "ROLE_USER";
//
//    private UserNormal person;
//
//    protected final Set<GrantedAuthority> authorities;
//
//    public UserDetails(UserNormal person) {
//        Objects.requireNonNull(person);
//        this.person = person;
//        this.authorities = new HashSet<>();
//        addDefaultRole();
//    }
//
//    public UserDetails(UserNormal person, Collection<GrantedAuthority> authorities) {
//        Objects.requireNonNull(person);
//        Objects.requireNonNull(authorities);
//        this.person = person;
//        this.authorities = new HashSet<>();
//        addDefaultRole();
//        this.authorities.addAll(authorities);
//    }
//
//    private void addDefaultRole() {
//        authorities.add(new SimpleGrantedAuthority(DEFAULT_ROLE));
//    }
//
//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return Collections.unmodifiableCollection(authorities);
//    }
//
//    @Override
//    public String getPassword() {
//        return person.getPass();
//    }
//
//    @Override
//    public String getUsername() {
//        return person.getLogin();
//    }
//
//    @Override
//    public boolean isAccountNonExpired() {
//        return false;
//    }
//
//    @Override
//    public boolean isAccountNonLocked() {
//        return true;
//    }
//
//    @Override
//    public boolean isCredentialsNonExpired() {
//        return true;
//    }
//
//    @Override
//    public boolean isEnabled() {
//        return true;
//    }
//
//    public UserNormal getUser() {
//        return person;
//    }
}
