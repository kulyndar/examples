/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.rest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import kulyndar.maksiale.restaurant.dao.UserNormalDao;
import kulyndar.maksiale.restaurant.model.Place;
import kulyndar.maksiale.restaurant.model.Reservation;
import kulyndar.maksiale.restaurant.model.User;
import kulyndar.maksiale.restaurant.service.ReservationService;
import kulyndar.maksiale.restaurant.service.UserNormalService;
import kulyndar.maksiale.restaurant.service.authentication.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kulyndar;
 */
@RestController
@RequestMapping("/wa/reservation")
public class AllReservations {

    @Autowired
    ReservationService service;

    @Autowired
    SecurityUtils utils;

    @Autowired
    UserNormalDao dao;
    
    @PreAuthorize("hasRole('ROLE_WAITER')")
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReservationToSend> getAllMyReservations() {
        //find current waiter
        User waiter = utils.getCurrentUser();
        //create lists
        List<Reservation> list = service.findActive();
        List<Reservation> toReturn = new ArrayList<>();
        List<ReservationToSend> toSend = new ArrayList<>();
        //find waiter`s reservations
        for (Reservation reservation : list) {
            if (reservation.getWaiter().getLogin() == waiter.getLogin()) {
                toReturn.add(reservation);
            }
        }
        //Compose reservation to send
        for (Reservation reservation : toReturn) {
            List<String> p = new ArrayList();
            //find customer`s name
            String login = reservation.getReservationPK().getCustomer();
            String name = dao.findByLogin(login).getUsername();
            //find tables numbers
//            p.addAll(reservation.getPlaceCollection().stream().map(pl -> pl.getTablenumber()).collect(Collectors.toList()));
            String[] tables = new String[reservation.getPlaceCollection().size()];
            int c = 0;
            for (Place place : reservation.getPlaceCollection()) {
                tables[c] = place.getTablenumber();
                ++c;
            }
            
            // Create an instance of SimpleDateFormat used for formatting 
// the string representation of date 
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

// Using DateFormat format method we can create a string 
// representation of a date with the defined format.
            String timeFrom = df.format(reservation.getReservationPK().getTimefrom());
            //create reservation to send
            ReservationToSend rts= new ReservationToSend(timeFrom, name, login, tables);
            toSend.add(rts);
        }
        toSend.sort(Comparator.comparing(ReservationToSend::getTimeFrom));
        return toSend;

    }
}
