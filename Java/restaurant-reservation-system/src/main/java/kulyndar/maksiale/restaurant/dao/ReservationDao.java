/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;

import java.util.Date;
import kulyndar.maksiale.restaurant.model.Reservation;
import kulyndar.maksiale.restaurant.model.ReservationPK;
import kulyndar.maksiale.restaurant.model.UserNormal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kulyndar, maksiale;
 */
@Repository
public class ReservationDao {

    @PersistenceContext
    EntityManager em;

    /**
     * Adds new reservation to DB
     *
     * @param r - reservation to add
     */
    public void addReservation(Reservation r) {
        em.persist(r);
    }

    /**
     * Finds such reservation with its primary key
     *
     * @param pk - primary key
     * @return true if exists
     */
    public boolean existsReservation(ReservationPK pk) {
        try {
            Reservation r = em.find(Reservation.class, pk);
            return r != null;

        } catch (Exception e) {
            return false;
        }
    }

    public void refresh() {

        em.getEntityManagerFactory().getCache().evictAll();
    }

    /**
     * Searches active reservations by status
     *
     * @return list of active reservations
     */
    public List<Reservation> findActive() {
        return em.createNamedQuery("Reservation.findByStatus").setParameter("status", 1).getResultList();
    }

    /**
     * Searches canceled reservations by status
     *
     * @return list of canceled reservations
     */
    public List<Reservation> findCanceled() {
        return em.createNamedQuery("Reservation.findByStatus").setParameter("status", 3).getResultList();
    }

    /**
     * Searches activated reservations by status
     *
     * @return list of activated reservations
     */
    public List<Reservation> findActivated() {
        return em.createNamedQuery("Reservation.findByStatus").setParameter("status", 2).getResultList();
    }

    /**
     * Searches reservations of this user
     *
     * @param user who has these reservations
     * @return list of reservations
     */
    public List<Reservation> findByUser(UserNormal user) {

        return em.createNamedQuery("Reservation.findByCustomer").setParameter("customer", user.getLogin()).getResultList();
    }
    
    public Reservation find(ReservationPK pk){
        return em.find(Reservation.class, pk);
    }
}
