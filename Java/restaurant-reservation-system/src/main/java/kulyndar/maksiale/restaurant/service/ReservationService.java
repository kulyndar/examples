/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import kulyndar.maksiale.restaurant.dao.ReservationDao;
import kulyndar.maksiale.restaurant.dao.RestaurantDao;
import kulyndar.maksiale.restaurant.dao.UserNormalDao;
import kulyndar.maksiale.restaurant.model.Place;
import kulyndar.maksiale.restaurant.model.Reservation;
import kulyndar.maksiale.restaurant.model.ReservationPK;
import kulyndar.maksiale.restaurant.model.Restaurant;
import kulyndar.maksiale.restaurant.model.User;
import kulyndar.maksiale.restaurant.model.UserNormal;
import kulyndar.maksiale.restaurant.model.Waiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kulyndar, maksiale;
 */
@Service
public class ReservationService {

    @Autowired
    ReservationDao dao;

    @Autowired
    WaiterService service;

    @Autowired
    RestaurantDao daoR;

    @Autowired
    PlaceService placeS;
    
    @Autowired
    UserNormalDao daoUN;

    @Transactional
    public void createReservation(UserNormal user, Date timeFrom, Date timeTo, List<Place> place, int numberOfCust) {
        Reservation r = new Reservation();
        ReservationPK pk = new ReservationPK(user.getLogin(), timeFrom);
        r.setReservationPK(pk);
        r.setPlaceCollection(place);
        r.setNumberofcustomers(numberOfCust);
        r.setTimeto(timeTo);
        List<Waiter> w = service.findByPlace(place.get(0));
//        int shift =  timeFrom.getDate()%2;
//        if(shift == 0){
//            shift = 2;
//        }
//        for (Waiter waiter : w) {
//            if(waiter.getShift()!=shift){
//                w.remove(waiter);
//            }
//        }
        r.setWaiter(w.get(0));
        dao.addReservation(r);

    }

    @Transactional
    public boolean createReservation(User user, Date timeFrom, Date timeTo, String[] places,String restaurant) {
        /*Find places*/
        List<Place> f = giveTable(timeFrom, restaurant);
        
        List<Place> p = new ArrayList<>();
        for (int i = 0; i < places.length; i++) {
            p.add(placeS.findPlace(places[i]));
        }
        for (Place place : p) {
            if(!f.contains(place)){
                return false;
            }
        }
        int numberOfCustomers = 0;
        for (Place place : p) {
            numberOfCustomers += place.getNumberofseats();
        }
        Reservation r = new Reservation();
        ReservationPK pk = new ReservationPK(user.getLogin(), timeFrom);
        r.setReservationPK(pk);
        r.setPlaceCollection(p);
        r.setTimeto(timeTo);
        r.setNumberofcustomers(numberOfCustomers);
        List<Waiter> w = service.findByPlace(p.get(0));
        r.setWaiter(w.get(0));
        r.setStatus(1);
        dao.addReservation(r);
        dao.refresh();

        return true;
    }

    @Transactional
    public void changeStatus(Reservation r, int status) {
        r.setStatus(status);
        dao.addReservation(r);
    }
    @Transactional
    public boolean changeStatus(String customer, Date timeFrom, int status){
       Reservation r = dao.find(new ReservationPK(customer, timeFrom));
       if(r!=null){
           r.setStatus(status);
           dao.addReservation(r);
//           dao.refresh();
           return true;
       }
       return false;
    }

    @Transactional(readOnly = true)
    public List<Reservation> findActive() {
        return dao.findActive();
    }

    @Transactional(readOnly = true)
    public List<Reservation> findCancelled() {
        return dao.findCanceled();
    }

    @Transactional(readOnly = true)
    public List<Reservation> findActivated() {
        return dao.findActivated();
    }

    @Transactional(readOnly = true)
    public List<Place> giveTable(Date timeFrom, String restaurant) {
        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(timeFrom); // sets calendar time/date
        cal.add(Calendar.HOUR_OF_DAY, 2); // adds one hour
        Date timeTo = cal.getTime();
        Restaurant rest = daoR.findRestaurantByName(restaurant);

        List<Place> places = placeS.findPlaceByRestaurant(rest);
        List<Reservation> reservations = findActive();
        List<Place> toReturn = new ArrayList<>(places);

        for (Place p : places) {
            Collection<Reservation> r = p.getReservationCollection();
//            if (r == null) {
//                continue;
//            }
//            if (r.isEmpty()) {
//                continue;
//            }
            for (Reservation reservation : r) {/*TimeFrom 14:00 TimeTo 16:00  from 12:00 to 14:00*/
                Date from = reservation.getReservationPK().getTimefrom();
                Date to = reservation.getTimeto();
                if (((timeFrom.after(to) || to.getTime() == timeFrom.getTime()) || (timeTo.before(from) || from.getTime() == timeTo.getTime())) && (timeFrom != from
                        && timeTo != to)) {

                } else {
                    if (reservation.getStatus() == 1) {
                        toReturn.remove(p);
                        break;
                    }
                }
            }

        }

        return toReturn;
    }
    
    @Transactional(readOnly=true)
    public List<Reservation> findByUser(String login){
        UserNormal user = daoUN.findByLogin(login);
        return dao.findByUser(user);
    }

}
