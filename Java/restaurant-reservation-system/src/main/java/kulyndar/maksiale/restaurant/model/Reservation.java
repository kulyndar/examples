/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kulyndar, maksiale;
 */
@Entity
@Table(name = "reservation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reservation.findAll", query = "SELECT r FROM Reservation r")
    ,
        @NamedQuery(name = "Reservation.findByCustomer", query
            = "SELECT r FROM Reservation r WHERE r.reservationPK.customer = :customer")
    ,
        @NamedQuery(name = "Reservation.findByTimefrom", query
            = "SELECT r FROM Reservation r WHERE r.reservationPK.timefrom = :timefrom")
    ,
        @NamedQuery(name = "Reservation.findByTimeto", query = "SELECT r FROM Reservation r WHERE r.timeto = :timeto")
    ,
        @NamedQuery(name = "Reservation.findByStatus", query = "SELECT r FROM Reservation r WHERE r.status = :status")
    ,
        @NamedQuery(name = "Reservation.findByNumberofcustomers", query
            = "SELECT r FROM Reservation r WHERE r.numberofcustomers = :numberofcustomers")})
public class Reservation implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ReservationPK reservationPK;
    @Basic(optional = false)
   
    @Column(name = "timeto")
    @Temporal(TemporalType.TIMESTAMP)
    @OrderBy
    private Date timeto;
    @Basic(optional = false)
    
    @Column(name = "status")
    private int status;
    @Basic(optional = false)
    
    @Column(name = "numberofcustomers")
    private int numberofcustomers;
    @JoinTable(name = "place_reservation", joinColumns
            = {
                @JoinColumn(name = "customer", referencedColumnName = "customer")
                ,
            @JoinColumn(name = "timefrom", referencedColumnName = "timefrom")}, inverseJoinColumns
            = {
                @JoinColumn(name = "tablenumber", referencedColumnName = "tablenumber")})
    @ManyToMany
    private Collection<Place> placeCollection;
    @JoinColumn(name = "customer", referencedColumnName = "login", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private UserNormal userNormal;
    @JoinColumn(name = "waiter", referencedColumnName = "login")
    @ManyToOne
    private Waiter waiter;

    public Reservation() {
    }

    public Reservation(ReservationPK reservationPK) {
        this.reservationPK = reservationPK;
    }

    public Reservation(ReservationPK reservationPK, Date timeto, int status, int numberofcustomers) {
        this.reservationPK = reservationPK;
        this.timeto = timeto;
        this.status = status;
        this.numberofcustomers = numberofcustomers;
    }

    public Reservation(String customer, Date timefrom) {
        this.reservationPK = new ReservationPK(customer, timefrom);
    }

    public ReservationPK getReservationPK() {
        return reservationPK;
    }

    public void setReservationPK(ReservationPK reservationPK) {
        this.reservationPK = reservationPK;
    }

    public Date getTimeto() {
        return timeto;
    }

    public void setTimeto(Date timeto) {
        this.timeto = timeto;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNumberofcustomers() {
        return numberofcustomers;
    }

    public void setNumberofcustomers(int numberofcustomers) {
        this.numberofcustomers = numberofcustomers;
    }

    @XmlTransient
    public Collection<Place> getPlaceCollection() {
        return placeCollection;
    }

    public void setPlaceCollection(Collection<Place> placeCollection) {
        this.placeCollection = placeCollection;
    }

    public UserNormal getUserNormal() {
        return userNormal;
    }

    public void setUserNormal(UserNormal userNormal) {
        this.userNormal = userNormal;
    }

    public Waiter getWaiter() {
        return waiter;
    }

    public void setWaiter(Waiter waiter) {
        this.waiter = waiter;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reservationPK != null ? reservationPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reservation)) {
            return false;
        }
        Reservation other = (Reservation) object;
        if ((this.reservationPK == null && other.reservationPK != null) ||
                (this.reservationPK != null && !this.reservationPK.equals(other.reservationPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "kulyndarmaksiale.resteuraceear.model.Reservation[ reservationPK=" + reservationPK + " ]";
    }

    
    
}
