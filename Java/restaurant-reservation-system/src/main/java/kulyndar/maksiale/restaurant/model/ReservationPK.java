/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author kulyndar, maksiale;
 */
@Embeddable
public class ReservationPK implements Serializable {

    @Basic(optional = false)
   
    @Column(name = "customer")
    private String customer;
    @Basic(optional = false)
    
    @Column(name = "timefrom")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timefrom;

    public ReservationPK() {
    }

    public ReservationPK(String customer, Date timefrom) {
        this.customer = customer;
        this.timefrom = timefrom;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Date getTimefrom() {
        return timefrom;
    }

    public void setTimefrom(Date timefrom) {
        this.timefrom = timefrom;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (customer != null ? customer.hashCode() : 0);
        hash += (timefrom != null ? timefrom.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservationPK)) {
            return false;
        }
        ReservationPK other = (ReservationPK) object;
        if ((this.customer == null && other.customer != null) || (this.customer != null && !this.customer.equals(other.customer))) {
            return false;
        }
        if ((this.timefrom == null && other.timefrom != null) || (this.timefrom != null && !this.timefrom.equals(other.timefrom))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "kulyndarmaksiale.resteuraceear.model.ReservationPK[ customer=" + customer + ", timefrom=" + timefrom + " ]";
    }
    
}
