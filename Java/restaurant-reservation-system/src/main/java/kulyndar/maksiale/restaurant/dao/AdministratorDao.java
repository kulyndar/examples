/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;


import kulyndar.maksiale.restaurant.model.Administrator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kulyndar, maksiale;
 */
@Repository
public class AdministratorDao {
    @PersistenceContext
    EntityManager em;
    /**
     * Checks if administrator with that login already exists
     * @param login - login of administrator
     * @return true if such administrator already exists, otherwise false
     */
    public boolean existsAdmin(String login){
    try {
            Administrator user = em.find(Administrator.class, login);
            return user != null;

        } catch (Exception e) {
            return false;
        }  
    
    }
    /**
     * Searches administrator with login and password
     * @param login 
     * @param password
     * @return administrator with this login and password
     */
    public Administrator findByLoginPassword(String login, String password){
        List<Administrator> users = em.createNamedQuery("Administrator.findByLoginPass").setParameter("login", login).setParameter(
                "pass", password).getResultList();
        if(!users.isEmpty()){
            return users.get(0);
        }
        return null;
    
    }
}
