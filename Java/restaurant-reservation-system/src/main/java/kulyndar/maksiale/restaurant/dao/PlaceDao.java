/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;

import java.util.ArrayList;
import kulyndar.maksiale.restaurant.model.Place;
import kulyndar.maksiale.restaurant.model.Restaurant;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kulyndar, maksiale;
 */
@Repository
public class PlaceDao {
    
    @PersistenceContext
    EntityManager em;
    
    /**
     * Adds new place to DB
     * @param p - table to add
     */
    public void addPlace(Place p){
        em.persist(p);
    }
    /**
     * Removes table from DB
     * @param p  - table to remove
     */
    public void deletePlace(Place p){
        em.remove(p);
    }
    
    /**
     * Returns all tables in defined restaurant.
     * @param r - restaurant
     * @return list of tables
     */
    public List<Place> findPlaceInRestaurant(Restaurant r){
        return em.createNamedQuery("Place.findByRestaurant").setParameter("restaurant", r).getResultList();
    
    }
    /**
     * Returns all tables in defined restaurant with defined number of seats
     * @param r - restaurant
     * @param s - number of seats
     * @return  list of tables
     */
    public List<Place> findPlaceByRestaurantAndSeats(Restaurant r, int s){
        List<Place> places = findPlaceInRestaurant(r);
        List<Place> toReturn = new ArrayList<>();
        for(Place place: places){
            if(s == place.getNumberofseats()){
                toReturn.add(place);
            }
        }
    return toReturn;
    }
    
    /**
     * Find table by table number
     * @param name - table number
     * @return table
     */
    public Place findPlaceByName(String name){
        return em.find(Place.class, name);
    }
}

