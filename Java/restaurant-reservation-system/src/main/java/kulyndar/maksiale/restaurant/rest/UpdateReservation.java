/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import kulyndar.maksiale.restaurant.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kulyndar;
 */
@RestController
@RequestMapping("/wa/reservation/update")
public class UpdateReservation {

    @Autowired
    ReservationService service;

    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateReservation(@RequestBody ToUpdate entity) {
        String target = entity.date + " " + entity.time;
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date result = new Date();
        try {
            result = df.parse(target);

        } catch (ParseException ex) {
            Logger.getLogger(GiveMeTables.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (service.changeStatus(entity.customer, result, entity.status)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }
}

class ToUpdate {

    String date;
    String time;
    String customer;
    int status;

    public ToUpdate(String date, String time, String customer, int status) {
        this.date = date;
        this.time = time;
        this.customer = customer;
        this.status = status;
    }

    public ToUpdate() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
