/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.rest;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author kulyndar;
 */
@XmlRootElement
public class ReservationToSend implements Serializable{
    String timeFrom;
    String customer;
    String login;
    String [] tables;

    public ReservationToSend(String timeFrom, String customer, String login, String[] tables) {
        this.timeFrom = timeFrom;
        this.customer = customer;
        this.login = login;
        this.tables = tables;
    }

    public ReservationToSend() {
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String[] getTables() {
        return tables;
    }

    public void setTables(String[] tables) {
        this.tables = tables;
    }
    
    
}
