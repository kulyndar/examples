/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;

import kulyndar.maksiale.restaurant.model.Restaurant;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kulyndar, maksiale;
 */
@Repository
public class RestaurantDao {

    @PersistenceContext
    public EntityManager em;

    /**
     * Adds this restaurant to DB
     *
     * @param r restaurant that has to be added
     */
    public void persistRestaurant(Restaurant r) {
        em.persist(r);
    }

    /**
     * Deletes this restaurant from DB
     *
     * @param r restaurant that must be removed
     */
    public void deleteRestaurant(Restaurant r) {
        em.remove(r);
    }

    /**
     * Finds out if restaurant with this name exists
     *
     * @param name of this restaurant
     * @return true if this restaurant exists
     */
    public boolean existsRestaurant(String name) {
        try {
            Restaurant r = em.find(Restaurant.class, name);
            return r != null;

        } catch (Exception e) {
            return false;
        }
    }

    /**
     * List of all restaurants
     *
     * @return list of all restaurants
     */
    public List<Restaurant> findAllRestaurants() {
        return em.createNamedQuery("Restaurant.findAll").getResultList();
    }

    public Restaurant findRestaurantByName(String name) {
        if (existsRestaurant(name)) {
            return em.find(Restaurant.class, name);
        }
        return null;//throw exception
    }

}
