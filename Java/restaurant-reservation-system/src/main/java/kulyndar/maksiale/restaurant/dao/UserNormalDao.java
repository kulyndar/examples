/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;

import kulyndar.maksiale.restaurant.model.UserNormal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kulyndar, maksiale;
 */
@Repository
public class UserNormalDao {

    @PersistenceContext
    EntityManager em;

    /**
     * Adds this user to DB
     *
     * @param user that must be added to DB
     */
    public void persistUser(UserNormal user) {
        em.persist(user);
    }

    /**
     * Deletes this user from DB
     *
     * @param user that must be deleted from DB
     */
    public void deleteUser(UserNormal user) {
        em.remove(user);
    }

    /**
     * Finds out if such user exists
     *
     * @param login that must be found
     * @return true if this user exists
     */
    public boolean existsUser(String login) {
        try {
            UserNormal user = em.find(UserNormal.class, login);
            return user != null;

        } catch (Exception e) {
            return false;
        }

    }

    /**
     * Finds user by his login and password
     *
     * @param login that must be found
     * @param password that must be found
     * @return user with such login and password
     */
    public UserNormal findByLoginPassword(String login, String password) {
        List<UserNormal> users = em.createNamedQuery("UserNormal.findByLoginPassword").setParameter("login", login).setParameter(
                "pass", password).getResultList();
        if (!users.isEmpty()) {
            return users.get(0);
        }
        return null;

    }
    
    public UserNormal findByLogin(String login){
        List<UserNormal> users = em.createNamedQuery("UserNormal.findByLogin").setParameter("login", login).getResultList();
        return users.isEmpty()? null : users.get(0);
    }

    /**
     * Finds out if such email exists
     *
     * @param email that must be found
     * @return true if this email exists
     */
    public boolean existsEmail(String email) {

        List<UserNormal> user = em.createNamedQuery("UserNormal.findByEmail").setParameter("email", email).getResultList();
        return !user.isEmpty();

    }

    /**
     * Finds out if this phone exists
     *
     * @param phone that must be found
     * @return true if this phone exists
     */
    public boolean existsPhone(String phone) {

        List<UserNormal> user = em.createNamedQuery("UserNormal.findByPhone").setParameter("phone", phone).getResultList();
        return !user.isEmpty();
    }

    /**
     * Finds user with such phone
     *
     * @param phone that must be found
     * @return user with this phone
     */
    public UserNormal findByPhone(String phone) {
        if (existsPhone(phone)) {
            return (UserNormal) em.createNamedQuery("UserNormal.findByPhone").setParameter("phone", phone).getResultList().get(0);
        }
        //exception this phone does not exist
        return null;
    }

}
