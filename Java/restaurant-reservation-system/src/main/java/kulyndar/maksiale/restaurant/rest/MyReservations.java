/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.rest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import kulyndar.maksiale.restaurant.model.Reservation;
import kulyndar.maksiale.restaurant.model.User;
import kulyndar.maksiale.restaurant.service.ReservationService;
import kulyndar.maksiale.restaurant.service.authentication.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kulyndar;
 */
@RestController
@RequestMapping("/my/reservation")
public class MyReservations {
    
    @Autowired
    SecurityUtils utils;
    
    @Autowired
    ReservationService service;
    
    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReservationToSendMy> giveMyReservation(){
        User user = utils.getCurrentUser();
        List<Reservation> list = service.findByUser(user.getLogin());
        List<ReservationToSendMy> toSend = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            
        for (Reservation reservation : list) {
            String timeFrom = df.format(reservation.getReservationPK().getTimefrom());
            String restaurant = reservation.getWaiter().getRestaurant().getNameofrestaurant();
            int status = reservation.getStatus();
            int number = reservation.getNumberofcustomers();
            toSend.add(new ReservationToSendMy(restaurant, number, timeFrom, status));
        }
        toSend.sort(Comparator.comparing(ReservationToSendMy::getDate));
        Collections.reverse(toSend);
        return toSend;
    }
}
