/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;

import java.util.List;
import kulyndar.maksiale.restaurant.model.Restaurant;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author kulyndar, maksiale;
 */
public class RestaurantDaoTest extends BaseDaoTestRunner{
    
    @Autowired
    RestaurantDao dao;
    
    Restaurant r ;
    
    
    public RestaurantDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        r = new Restaurant("AlfaCentavra", "Nalcik", "Nikolaje", "12345");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of persistRestaurant method, of class RestaurantDao.
     */
    @Test
    public void testPersistRestaurant() {
        
        dao.persistRestaurant(r);
        assertTrue(dao.existsRestaurant(r.getNameofrestaurant()));
        assertFalse(dao.findAllRestaurants().isEmpty());
    }

    /**
     * Test of deleteRestaurant method, of class RestaurantDao.
     */
    @Test
    public void testDeleteRestaurant() {
        
        dao.persistRestaurant(r);
        assertTrue(dao.existsRestaurant(r.getNameofrestaurant()));
        dao.deleteRestaurant(r);
        assertFalse(dao.existsRestaurant(r.getNameofrestaurant()));
    }

    /**
     * Test of existsRestaurant method, of class RestaurantDao.
     */
    @Test
    public void testExistsRestaurant() {
       
        dao.persistRestaurant(r);
        assertTrue(dao.existsRestaurant(r.getNameofrestaurant()));
        
    }

    /**
     * Test of findAllRestaurants method, of class RestaurantDao.
     */
    @Test
    public void testFindAllRestaurants() {       
        dao.persistRestaurant(r);
        assertEquals(true, dao.findAllRestaurants().contains(r));    
    }
    
}
