/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;

import java.util.List;
import kulyndar.maksiale.restaurant.model.Place;
import kulyndar.maksiale.restaurant.model.Restaurant;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author kulyndar, maksiale;
 */
public class PlaceDaoTest extends BaseDaoTestRunner {

    @Autowired
    PlaceDao dao;

    @Autowired
    RestaurantDao daoRest;

    Place p;
    Place p6;
    Restaurant r;

    public PlaceDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        r = new Restaurant("Pdbjhv", "Praha", "Ulice", "12345");
        daoRest.persistRestaurant(r);
        p = new Place("A12", "Pohoda a klid", 2);
        p6 = new Place("A13", "U okna", 6);
        p.setRestaurant(r);
        p6.setRestaurant(r);

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addPlace method, of class PlaceDao.
     */
    @Test
    public void testAddPlace() {
        dao.addPlace(p);
        assertEquals(p, dao.findPlaceByName(p.getTablenumber()));
        assertEquals(null, dao.findPlaceByName(p6.getTablenumber()));
    }

    /**
     * Test of deletePlace method, of class PlaceDao.
     */
    @Test
    public void testDeletePlace() {
        dao.addPlace(p);
        assertEquals(p, dao.findPlaceByName(p.getTablenumber()));
        assertEquals(null, dao.findPlaceByName(p6.getTablenumber()));
        dao.deletePlace(p);
        assertEquals(null, dao.findPlaceByName(p.getTablenumber()));
        dao.deletePlace(p6);
        assertEquals(null, dao.findPlaceByName(p6.getTablenumber()));

    }

    /**
     * Test of findPlaceInRestaurant method, of class PlaceDao.
     */
    @Test
    public void testFindPlaceInRestaurant() {
        dao.addPlace(p);
        assertEquals(p, dao.findPlaceByName(p.getTablenumber()));
        assertEquals(true, dao.findPlaceInRestaurant(r).contains(p));
        dao.addPlace(p6);
        assertEquals(true, dao.findPlaceInRestaurant(r).contains(p) && dao.findPlaceInRestaurant(r).contains(p6));
        dao.deletePlace(p);
        assertEquals(false, dao.findPlaceInRestaurant(r).contains(p));
        dao.deletePlace(p6);
        assertEquals(false, dao.findPlaceInRestaurant(r).contains(p)|| dao.findPlaceInRestaurant(r).contains(p6));
    }

    /**
     * Test of findPlaceByRestaurantAndSeats method, of class PlaceDao.
     */
    @Test
    public void testFindPlaceByRestaurantAndSeats() {
         dao.addPlace(p);
         assertEquals(true, dao.findPlaceByRestaurantAndSeats(r, 2).contains(p));
         dao.addPlace(p6);
//         assertEquals(true, dao.findPlaceByRestaurantAndSeats(r, 2).contains(p));
         assertEquals(false, dao.findPlaceByRestaurantAndSeats(r, 2).contains(p6));
         assertTrue(dao.findPlaceByRestaurantAndSeats(r, 6).contains(p6));
         assertFalse(dao.findPlaceByRestaurantAndSeats(r, 6).contains(p));
         
    }

    /**
     * Test of findPlaceByName method, of class PlaceDao.
     */
    @Test
    public void testFindPlaceByName() {
        assertEquals(null, dao.findPlaceByName(p.getTablenumber()));
        dao.addPlace(p);
        assertEquals(p, dao.findPlaceByName(p.getTablenumber()));
        assertEquals(null, dao.findPlaceByName(p6.getTablenumber()));
        dao.deletePlace(p);
        assertEquals(null, dao.findPlaceByName(p.getTablenumber()));
        dao.addPlace(p6);
        assertEquals(p6, dao.findPlaceByName(p6.getTablenumber()));
        
    }

}
