/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;

import java.util.ArrayList;
import java.util.List;
import kulyndar.maksiale.restaurant.model.Place;
import kulyndar.maksiale.restaurant.model.Restaurant;
import kulyndar.maksiale.restaurant.model.Waiter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author kulyndar, maksiale;
 */
public class WaiterDaoTest extends BaseDaoTestRunner {

    @Autowired
    WaiterDao dao;

    @Autowired
    RestaurantDao daoRest;

    @Autowired
    PlaceDao daoPlace;

    Waiter w;
    Place p;

    public WaiterDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        Restaurant r = new Restaurant("PPPPP", "Praha", "absjbdkjbvdkj", "12345");
        daoRest.persistRestaurant(r);
        p = new Place("A12", "Pohoda a klidek", 4);
        p.setRestaurant(r);
        daoPlace.addPlace(p);   
        w = new Waiter("Ghost", "12345678", 1, "Jan Go");
        w.setRestaurant(r);
        w.setPlaceCollection(new ArrayList<Place>());
        w.getPlaceCollection().add(p);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of persistWaiter method, of class WaiterDao.
     */
    @Test
    public void testPersistWaiter() {

        dao.persistWaiter(w);
        assertTrue(dao.existsWaiter(w.getLogin()));
        assertEquals(w, dao.findWaiter(w.getLogin()));
    }

    /**
     * Test of deleteWaiter method, of class WaiterDao.
     */
    @Test
    public void testDeleteWaiter() {
        dao.persistWaiter(w);
        assertTrue(dao.existsWaiter(w.getLogin()));
        dao.deleteWaiter(w);
        assertFalse(dao.existsWaiter(w.getLogin()));
        assertEquals(null, dao.findWaiter(w.getLogin()));
    }

    /**
     * Test of findWaiter method, of class WaiterDao.
     */
    @Test
    public void testFindWaiter() {
        dao.persistWaiter(w);
        assertTrue(dao.existsWaiter(w.getLogin()));
        assertEquals(w, dao.findWaiter(w.getLogin()));
        assertEquals(null, dao.findWaiter("qwertyuiopasdfghjklzxcvbn777666"));

    }

    /**
     * Test of existsWaiter method, of class WaiterDao.
     */
    @Test
    public void testExistsWaiter() {
        dao.persistWaiter(w);
        assertTrue(dao.existsWaiter(w.getLogin()));
        assertFalse(dao.existsWaiter(null));
    }

    /**
     * Test of findByLoginPassword method, of class WaiterDao.
     */
    @Test
    public void testFindByLoginPassword() {
        dao.persistWaiter(w);
        assertEquals(w, dao.findByLoginPassword(w.getLogin(), w.getPass()));

        assertEquals(null, dao.findByLoginPassword(null, null));
        dao.deleteWaiter(w);
        assertEquals(null, dao.findByLoginPassword(w.getLogin(), w.getPass()));

    }

    /**
     * Test of findByPlace method, of class WaiterDao.
     */
    @Test
    public void testFindByPlace() {
       dao.persistWaiter(w);
       
       assertEquals(true, dao.findByPlace(p.getTablenumber()).contains(w));
       assertEquals(true, dao.findByPlace(null).isEmpty());
       dao.deleteWaiter(w);
       assertEquals(false, dao.findByPlace(p.getTablenumber()).contains(w));
    }

}
