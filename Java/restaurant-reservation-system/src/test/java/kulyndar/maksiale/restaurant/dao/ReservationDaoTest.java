/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;

import java.util.Date;
import java.util.List;
import kulyndar.maksiale.restaurant.model.Reservation;
import kulyndar.maksiale.restaurant.model.ReservationPK;
import kulyndar.maksiale.restaurant.model.UserNormal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author kulyndar, maksiale;
 */
public class ReservationDaoTest extends BaseDaoTestRunner {

    @Autowired
    ReservationDao dao;

    @Autowired
    UserNormalDao daoUser;
    
    UserNormal user;
    Reservation r ;

    public ReservationDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        user = new UserNormal("John Snow", "abc222222222", "axfgxgf@vhghgc.vhg", "1234567891234",
                "JSJSJS");
        r = new Reservation(new ReservationPK("John Snow", new Date(2017, 11, 12, 15, 0, 0)),
                new Date(2017, 11, 12, 17, 0, 0), 1, 4);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addReservation method, of class ReservationDao.
     */
    @Test
    public void testAddReservation() {
        daoUser.persistUser(user);
        
        dao.addReservation(r);
        assertTrue(dao.existsReservation(r.getReservationPK()));

    }

    /**
     * Test of existsReservation method, of class ReservationDao.
     */
    @Test
    public void testExistsReservation() {
        assertFalse(dao.existsReservation(r.getReservationPK()));
        daoUser.persistUser(user);
       
        dao.addReservation(r);
        assertTrue(dao.existsReservation(r.getReservationPK()));
    }

    /**
     * Test of findActive method, of class ReservationDao.
     */
    @Test
    public void testFindActive() {
        daoUser.persistUser(user);
       
        dao.addReservation(r);
        assertTrue(dao.findActive().contains(r));
        assertFalse(dao.findActivated().contains(r));
        assertFalse(dao.findCanceled().contains(r));
    }

    /**
     * Test of findCancelled method, of class ReservationDao.
     */
    @Test
    public void testFindCancelled() {
        daoUser.persistUser(user);
        r.setStatus(3);
        dao.addReservation(r);
        assertFalse(dao.findActive().contains(r));
        assertFalse(dao.findActivated().contains(r));
        assertTrue(dao.findCanceled().contains(r));
    }

    /**
     * Test of findActivated method, of class ReservationDao.
     */
    @Test
    public void testFindActivated() {
        daoUser.persistUser(user);
        r.setStatus(2);
        dao.addReservation(r);
        assertFalse(dao.findActive().contains(r));
        assertTrue(dao.findActivated().contains(r));
        assertFalse(dao.findCanceled().contains(r));
    }
    /**
     * Test of findActive method, of class ReservationDao. DB adds status 1 as default value
     */
    @Test
    public void testFindActiveDefault() {
        daoUser.persistUser(user);
        Reservation re = new Reservation();
        re.setNumberofcustomers(4);
        re.setTimeto(new Date(2017, 11, 12, 17, 0, 0));
        re.setReservationPK(new ReservationPK("John Snow", new Date(2017, 11, 12, 15, 0, 0)));
        dao.addReservation(re);
        assertTrue(dao.existsReservation(re.getReservationPK()));
        re.setStatus(1);
        assertTrue(dao.findActive().contains(re));
        assertFalse(dao.findActivated().contains(re));
        assertFalse(dao.findCanceled().contains(re));
    }

    /**
     * Test of findByUser method, of class ReservationDao.
     */
    @Test
    public void testFindByUser() {
        
        daoUser.persistUser(user);
        dao.addReservation(r);
        assertTrue(dao.findByUser(user).contains(r));
    }

}
