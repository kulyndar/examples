/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;

import kulyndar.maksiale.restaurant.model.Administrator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author kulyndar, maksiale;
 */
public class AdministratorDaoTest extends BaseDaoTestRunner{
    
    @Autowired
    AdministratorDao dao;
    
    Administrator admin;
    
    public AdministratorDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        admin = new Administrator("King12345", "Burger132456");
        dao.em.persist(admin);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of existsAdmin method, of class AdministratorDao.
     */
    @Test
    public void testExistsAdmin() {
        assertTrue(dao.existsAdmin(admin.getLogin()));
        assertFalse(dao.existsAdmin(null));
    }

    /**
     * Test of findByLoginPassword method, of class AdministratorDao.
     */
    @Test
    public void testFindByLoginPassword() {
        assertEquals(admin, dao.findByLoginPassword(admin.getLogin(), admin.getPass()));
        assertEquals(null, dao.findByLoginPassword(null, null));
    }
    
}
