/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kulyndar.maksiale.restaurant.dao;

import kulyndar.maksiale.restaurant.model.UserNormal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author kulyndar, maksiale;
 */
public class UserNormalDaoTest extends BaseDaoTestRunner {

    @Autowired
    UserNormalDao dao;

    UserNormal user;

    public UserNormalDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        user = new UserNormal("SuperPuper", "SuperPuper98", "SuperPuper98@mail.cz", "1234567891234", "Jahm Smith");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of persistUser method, of class UserNormalDao.
     */
    @Test
    public void testPersistUser() {
        dao.persistUser(user);
        assertTrue(dao.existsUser(user.getLogin()));
    }

    /**
     * Test of deleteUser method, of class UserNormalDao.
     */
    @Test
    public void testDeleteUser() {
        dao.persistUser(user);
        assertTrue(dao.existsUser(user.getLogin()));
        dao.deleteUser(user);
        assertFalse(dao.existsUser(user.getLogin()));
    }

    /**
     * Test of existsUser method, of class UserNormalDao.
     */
    @Test
    public void testExistsUser() {
        dao.persistUser(user);
        assertTrue(dao.existsUser(user.getLogin()));
        assertFalse(dao.existsUser(null));
    }

    /**
     * Test of findByLoginPassword method, of class UserNormalDao.
     */
    @Test
    public void testFindByLoginPassword() {
        dao.persistUser(user);
        assertTrue(dao.existsUser(user.getLogin()));
        assertEquals(user, dao.findByLoginPassword(user.getLogin(), user.getPass()));
        assertEquals(null, dao.findByLoginPassword(null, null));
        dao.deleteUser(user);
        assertEquals(null, dao.findByLoginPassword(user.getLogin(), user.getPass()));
    }

    /**
     * Test of existsEmail method, of class UserNormalDao.
     */
    @Test
    public void testExistsEmail() {
        dao.persistUser(user);
        assertTrue(dao.existsUser(user.getLogin()));
        assertTrue(dao.existsEmail(user.getEmail()));
        dao.deleteUser(user);
        assertFalse(dao.existsEmail(user.getEmail()));
    }

    /**
     * Test of existsPhone method, of class UserNormalDao.
     */
    @Test
    public void testExistsPhone() {
        dao.persistUser(user);
        assertTrue(dao.existsUser(user.getLogin()));
        assertTrue(dao.existsPhone(user.getPhone()));
        dao.deleteUser(user);
        assertFalse(dao.existsPhone(user.getPhone()));
    }

    /**
     * Test of findByPhone method, of class UserNormalDao.
     */
    @Test
    public void testFindByPhone() {
        dao.persistUser(user);
        assertTrue(dao.existsUser(user.getLogin()));
        assertEquals(user, dao.findByPhone(user.getPhone()));
        assertEquals(null, dao.findByPhone(null));
        dao.deleteUser(user);
        assertEquals(null, dao.findByPhone(user.getPhone()));
    }

}
