
package kulyndar.maksiale.restaurant.dao;

import kulyndar.maksiale.restaurant.config.PersistenceConfig;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class configures our tests so that we can use the Spring features in them - e.g. autowiring.
 * <p>
 * It is often good to extract this setup into a common superclass, so that we need not set the configuration on every test class.
 */
@RunWith(SpringJUnit4ClassRunner.class) // Tell JUnit to use Spring's test runner
/*
Which configuration classes should Spring load. This also means that for example service classes and REST controllers
won't be available for autowiring in tests inheriting from this class.
 */
@ContextConfiguration(classes = {PersistenceConfig.class})
// Reset the Spring context after each tests, recreating all the beans
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
//extend the transactions to whole tests in order to rollback the changes after each test
// Se also http://docs.spring.io/spring/docs/current/spring-framework-reference/html/integration-testing.html#testcontext-tx
@Transactional(transactionManager = "txManager")
public abstract class BaseDaoTestRunner {
}
