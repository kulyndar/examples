package ins.prototyp;

import javax.persistence.EntityManagerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.orm.jpa.JpaTransactionManager;

import ins.prototyp.config.AppConfig;

@SpringBootApplication
@ComponentScan(basePackages = "ins.prototyp.controller")
@EntityScan({ "ins.prototyp.model" })
@Import({AppConfig.class, TestData.class})
public class PrototypeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrototypeApplication.class, args);
	}
	
	

}
