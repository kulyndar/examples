package ins.prototyp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PrototypeController {
	
	@RequestMapping("/process")
	public String process(HttpServletRequest request) {
		if (request.isUserInRole("ROLE_ZAMESTNANEC")) {
			return "redirect:/zamestnanec/index";
		}
                if (request.isUserInRole("ROLE_MANAZER")) {
			return "redirect:/manazer/project-list";
		}
                if (request.isUserInRole("ROLE_BRIGADNIK")) {
			return "redirect:/brigadnik/index";
		}
		return "redirect:/login";
	}
	
	@PreAuthorize("hasRole('ROLE_ZAMESTNANEC')")
	@RequestMapping("/zamestnanec/index")
	public String index(Model model, Authentication auth) {
		model.addAttribute("hello", "Hello World!");
		model.addAttribute("username", auth.getName());
		return "/zamestnanec/index";
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	        return "redirect:/login?logout=true";
	    }
	    return "redirect:/login";
	    
	}
	
	@RequestMapping("/")
	public String login(Model model) {
		return "/login";
	}
	
	@RequestMapping("/login")
	public String loginX(Model model) {
		return "/login";
	}


}
