/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.controller;

import ins.prototyp.model.Answer;
import ins.prototyp.model.AnswerPK;
import ins.prototyp.model.Form;
import ins.prototyp.model.Question;
import ins.prototyp.service.AnketService;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author kulyndar;
 */
@Controller
@PreAuthorize("hasROLE('ROLE_BRIGADNIK')")
@RequestMapping("/brigadnik")
public class BrigadnikController {
    @Autowired
    AnketService formService;
    
    @GetMapping("/index")
    public String getIndex(Model model){
        List<Form> forms = formService.findAll();
        model.addAttribute("formlist", forms);
        return "brigadnik/list-of-questionnaires";
    }
    @GetMapping("/form/{id}/detail")
    public String getDetail(Model model, @PathVariable("id") int id){
        Form form = formService.find(id);
        model.addAttribute("form", form);
        return  "brigadnik/questionnaire-detail";
    }
    @GetMapping("/form/{id}/fill")
    public String getFillPage(Model model, @PathVariable("id") int id){
        Form form = formService.find(id);
        model.addAttribute("form", form);
        Collection<Question> questions = form.getQuestionCollection();
        model.addAttribute("questions", questions);
        return "brigadnik/questionnaire";
    }
    @PostMapping("/form/{id}/answer")
    public String answerForm(Model model, @PathVariable("id") int id, @RequestBody MultiValueMap<String, String> formParams){
        Answer[] ans = new Answer [formParams.get("answer").size()];
        Form form = formService.find(id);
//        List<Question> q = form.getQuestionCollection().stream().collect(Collectors.toList());
            List<Question> q = formService.getQuestions(form);
        for (int i = 0; i < ans.length; i++) {
            Answer answer = new Answer();
            answer.setAText(formParams.get("answer").get(i));
            System.out.println("COLLECTION SIZE "+q.size());
            answer.setQuestion(q.get(i));
            answer.setAnswerPK(new AnswerPK(q.get(i).getQuestionPK().getQId(), i+1));
            ans[i] = answer;
        }
        try {
            formService.writeAnswers(ans);
            formService.inreaseCount(form);
            
        } catch (IOException ex) {
            Logger.getLogger(BrigadnikController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidFormatException ex) {
            Logger.getLogger(BrigadnikController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "redirect:/brigadnik/form/"+id+"/detail";
    }
    
}
