/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.controller;

import ins.prototyp.model.Client;
import ins.prototyp.model.Employee;
import ins.prototyp.model.Project;
import ins.prototyp.model.Specialization;
import ins.prototyp.service.EmployeeService;
import ins.prototyp.service.ProjectService;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author kulyndar;
 */
@Controller
public class ProjectController {
    @Autowired
    ProjectService service;
    
    @Autowired
    EmployeeService empService;
    
    /*Show project page*/
    @RequestMapping("/project/{projName}")
    public String getProjectPage(Model model, @PathVariable("projName") String projName ){
        Project proj = service.find(projName);
        model.addAttribute("name", proj.getProjName());
        model.addAttribute("customer", proj.getClient().getCliName());
        model.addAttribute("supervisor", proj.getSupervisor().getEmpName());
        model.addAttribute("from", proj.getDateFrom());
        model.addAttribute("to", proj.getDateTo());
        model.addAttribute("status", proj.getStatus());
        model.addAttribute("description", proj.getDescription());
        model.addAttribute("spec", proj.getSpecialization().getSpecName());
        return "frontend/project-page";
    }
    
    /*Show employees on project*/
    @RequestMapping("/project/{projName}/employees")
    public String showEmployeesOnProject(Model model,@PathVariable("projName") String projName ){
        Collection<Employee> emplist = service.getProjectEmployees(projName);
        System.out.println(emplist.size());
        model.addAttribute("emplist", emplist);
        model.addAttribute("name", projName);
        return "frontend/employees-on-project-list";
    }
    
    /*New project*/
    @RequestMapping("/project/new")
    public String newProject(Model model){
        List<Specialization> spec = service.findAllSpec();
        List<Employee> emplist = empService.findAll();
        List<Client> clients = service.findAllClients();
        model.addAttribute("speclist", spec);
        model.addAttribute("emplist", emplist);
        model.addAttribute("customers", clients);
        return "frontend/new-project";
    }
    
    @PostMapping("/project/new")
    public String createProject(Model model, @RequestParam("projname") String projname, @RequestParam("customer") String customer,
            @RequestParam("supervisor") String supervisor, @RequestParam("spec") String spec, @RequestParam("from") String from,
            @RequestParam("to") String to,@RequestParam("description") String description){
        service.createProject(projname, customer, supervisor, spec, from, to, description);
        return "redirect:/new/project/"+projname+"/add-employee";
    }
    @RequestMapping("/new/project/{projName}/add-employee")
    public String addEmployee(Model model, @PathVariable("projName") String projName ){
        model.addAttribute("projname", projName);
    return "frontend/new-project-add-employee";
    }
    /*Close project*/
}
