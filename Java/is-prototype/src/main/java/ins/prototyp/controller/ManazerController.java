/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.controller;

import ins.prototyp.model.Client;
import ins.prototyp.model.Employee;
import ins.prototyp.model.Form;
import ins.prototyp.model.Project;
import ins.prototyp.model.Question;
import ins.prototyp.model.QuestionPK;
import ins.prototyp.model.Specialization;
import ins.prototyp.service.AnketService;
import ins.prototyp.service.EmpDTO;
import ins.prototyp.service.EmployeeService;
import ins.prototyp.service.ProjectService;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author kulyndar;
 */
@Controller
@PreAuthorize("hasROLE('ROLE_MANAZER')")
public class ManazerController {

    @Autowired
    ProjectService service;

    @Autowired
    EmployeeService empService;
    @Autowired
    AnketService formService;

    /*Close project*/
    @GetMapping("manazer/project/close/{id}")
    public String closeProject(Model model, @PathVariable("id") int projId) {
        service.closeProject(projId);
        return "redirect:/manazer/project-page/" + projId;

    }

    /*Show project page*/
    @RequestMapping("manazer/project-page/{projId}")
    public String getProjectPage(Model model, @PathVariable("projId") int projId) {
        Project proj = service.findByID(projId);
        model.addAttribute("proj", proj);
        if (service.isOpened(proj)) {
            /*check if there is form*/
            Form form = formService.findByProject(proj);
            if (form == null) {
                return "manazer/project-page";
            } else {
                int state = service.dataStatus(proj);
                model.addAttribute("state", state);
                model.addAttribute("form", form);
                return "manazer/project-page-with-questionnaire(supervisor)";
            }

        } else {
            return "manazer/closed-project-page";
        }

    }

    /*Project list*/
    @GetMapping("manazer/project-list")
    public String index(Model model, Authentication auth) {
        Employee user = empService.findByLogin(auth.getName());
        List<Project> projects = service.findAll();
        projects = projects.stream().filter(x -> x.getEmployeeCollection().contains(user)).collect(Collectors.toList());
        projects = projects.stream().sorted(Comparator.comparing(Project::getStatus).reversed().
                thenComparing(Project::getDateFrom)).collect(Collectors.toList());
        model.addAttribute("projects", projects);
        return "manazer/project-list";
    }

    /*Filter projects*/
    @GetMapping("manazer/project-list/filter")
    public String filter(Model model, Authentication auth, @RequestParam("type") String filter,
            @RequestParam("from") String dateFrom, @RequestParam("to") String dateTo) { //parametry: filter, dateFrom, dateTo 
        Employee user = empService.findByLogin(auth.getName());
        List<Project> projects = service.filter(filter, dateFrom, dateTo);
        projects = projects.stream().filter(x -> x.getEmployeeCollection().contains(user)).collect(Collectors.toList());
        projects = projects.stream().sorted(Comparator.comparing(Project::getStatus).reversed().
                thenComparing(Project::getDateFrom)).collect(Collectors.toList());
        model.addAttribute("projects", projects);
        return "manazer/project-list";

    }

    /*Show employees on project*/
    @RequestMapping("manazer/project-page/employees/{projId}")
    public String showEmployeesOnProject(Model model, @PathVariable("projId") int projId) {
        Project proj = service.findByID(projId);
        Collection<Employee> emplist = service.getProjectEmployees(proj.getProjName());
        System.out.println(emplist.size());
        model.addAttribute("emplist", emplist);
        model.addAttribute("name", proj.getProjName());
        return "manazer/employees-on-project-list";
    }

    /*New project*/
    @RequestMapping("manazer/project/new")
    public String newProject(Model model) {
        List<Specialization> spec = service.findAllSpec();
        List<Employee> emplist = empService.findAll();
        List<Client> clients = service.findAllClients();
        model.addAttribute("speclist", spec);
        model.addAttribute("emplist", emplist);
        model.addAttribute("customers", clients);
        return "manazer/new-project";
    }

    @PostMapping("manazer/project/new")
    public String createProject(Model model, @RequestParam("projname") String projname, @RequestParam("customer") String customer,
            @RequestParam("supervisor") String supervisor, @RequestParam("spec") String spec, @RequestParam("from") String from,
            @RequestParam("to") String to, @RequestParam("description") String description) {
        service.createProject(projname, customer, supervisor, spec, from, to, description);
        Project project = service.find(projname);
        return "redirect:/manazer/project/new/" + project.getId() + "/add-employee";
    }

    @GetMapping("manazer/project/new/{projId}/add-employee")
    public String addEmployee(Model model, @PathVariable("projId") int projId) {
        Project project = service.findByID(projId);
        Employee supervisor = project.getSupervisor();
        List<EmpDTO> emps = empService.getEmployees(project.getDateFrom(), project.getDateTo());
        emps.removeIf(e -> e.getLogin() == supervisor.getLogin());
        model.addAttribute("projname", project.getProjName());
        model.addAttribute("emps", emps);
        model.addAttribute("projId", projId);
        return "manazer/new-project-add-employee";
    }

    @PostMapping("manazer/project/new/{projId}/add-employee")
    public String addEmployeeToProject(Model model, @PathVariable("projId") int projId,
            @RequestBody MultiValueMap<String, String> formParams) {
        Project project = service.findByID(projId);
        System.out.println("PARAMS");
        System.out.println(formParams.get("zam").get(0));
        for (int i = 0; i < formParams.get("zam").size(); i++) {
            empService.subscribeToProject(projId, formParams.get("zam").get(i));
        }
        return "redirect:/manazer/project-list";
    }

    @GetMapping("manazer/employees/{login}")
    public String getEmployeeProjects(Model model, @PathVariable("login") String login) {
        Employee emp = empService.findByLogin(login);
        List<Project> projects = service.findAll();
        projects = projects.stream().filter(x -> x.getEmployeeCollection().contains(emp)).collect(Collectors.toList());
        model.addAttribute("emp", emp);
        model.addAttribute("projects", projects);
        return "manazer/remove-employee-from-project";
    }

    @PostMapping("manazer/employees/{login}/remove")
    public String getEmployeeProjects(Model model, @PathVariable("login") String login,
            @RequestBody MultiValueMap<String, String> formParams) {

        for (int i = 0; i < formParams.get("proj-del").size(); i++) {
            empService.unsubscripeFromProject(Integer.parseInt(formParams.get("proj-del").get(i)), login);
        }
        return "redirect:/manazer/employees/" + login;
    }

    @RequestMapping(value="manazer/project/{id}/export", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(@PathVariable(value = "id") int id) {
        Project proj = service.findByID(id);
        Form form = formService.findByProject(proj);
        return new FileSystemResource(new File("anketa"+form.getFormId()+".xlsx"));
    }
    
    /*Novy dotaznik*/
    @RequestMapping("manazer/new-questionnaire-detail/{project}")
	public String newQD(Model model,@PathVariable("project") Integer project) {
		model.addAttribute("forProject", project);
		return "manazer/new-questionnaire-detail";
	}
	
	@PostMapping(value="manazer/new-questionnaire-detail", params={"nForm"})
	public String newForm(Model model,
			@RequestParam("projectID") Integer projectID,
			@RequestParam("new-project-from") String npf,
			@RequestParam("new-project-to") String npt,
			@RequestParam("new-project-title") Integer answerCount,
			@RequestParam("new-project-description") String desc) {
		Form f = new Form();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date npfd = formatter.parse(npf);
			Date nptd = formatter.parse(npt);
		
		f.setAnswerCount(0);
		f.setDateFrom(npfd);
		f.setDescription(desc);
		f.setDateTo(nptd);
		f.setProject(service.findByID(projectID));
		f.setWantedCount(answerCount);
		f.setQuestionCollection(new ArrayList<Question>());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		formService.persist(f);
		return "redirect:/manazer/new-questionnaire/"+f.getFormId();
	}
	
	@GetMapping("manazer/new-questionnaire/{formID}")
	public String newQ(@PathVariable("formID") Integer formID, Model model) {
		String[] questions = new String[50]; //lol
		Form f = formService.find(formID);
		model.addAttribute("questions", questions);
		model.addAttribute("projectID", f.getProject().getId());
		model.addAttribute("formID", f.getFormId());
		return "manazer/new-questionnaire";
	}
	
	@PostMapping(value="manazer/new-questionnaire")
	public String saveQ(@RequestBody MultiValueMap<String,String> formData, Model model) {
		int i = 0;
		int formID = 0;
		int projectID = 0;
		for (Map.Entry<String, List<String>> q : formData.entrySet()) {
			System.out.println(q.getKey() + " : "+q.getValue().get(0));
			if (q.getKey().startsWith("project")) {
				projectID = Integer.parseInt(q.getValue().get(0));
				System.out.println(projectID);
			}
			if (q.getKey().startsWith("form")) {
				formID = Integer.parseInt(q.getValue().get(0));
				System.out.println(formID);
			}
			if (q.getKey().startsWith("question")) {
				if (!q.getValue().get(0).isEmpty()) {
					Form f = formService.find(formID);
					Question qt = new Question();
					qt.setForm(f);
					qt.setQText(q.getValue().get(0));
					qt.setQuestionPK(new QuestionPK(i++,f.getFormId()));
					f.getQuestionCollection().add(qt);
					formService.persist(f);
				}
			}
		}
		
		return "redirect:/manazer/project-page/"+projectID;
	}
    /*Open form*/
        @GetMapping("manazer/form/{id}/open")
       public String openForm(Model model, @PathVariable("id") int id){
        Form form = formService.find(id);
        model.addAttribute("form", form);
        Collection<Question> questions = form.getQuestionCollection();
        model.addAttribute("questions", questions);
        return "manazer/questionnaire";
    }
       @GetMapping("manazer/form/{id}/detail")
    public String getDetail(Model model, @PathVariable("id") int id){
        Form form = formService.find(id);
        model.addAttribute("form", form);
        return  "manazer/questionnaire-detail";
    }
    
}
