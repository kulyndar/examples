package ins.prototyp.controller;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ins.prototyp.model.Employee;
import ins.prototyp.model.Form;
import ins.prototyp.model.Project;
import ins.prototyp.model.Question;
import ins.prototyp.model.QuestionPK;
import ins.prototyp.service.AnketService;
import ins.prototyp.service.EmployeeService;
import ins.prototyp.service.ProjectService;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/zamestnanec")
@PreAuthorize("hasROLE('ROLE_ZAMESTNANEC')")
public class ZamestnanecController {
	
	@Autowired
	private ProjectService projectService;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private AnketService anketService;
	
	@GetMapping("/index")
	public String index(Model model, Authentication auth){
		Employee user = employeeService.findByLogin(auth.getName());
		List<Project> projects = projectService.findAll();
		projects = projects.stream().filter(x -> x.getEmployeeCollection().contains(user)).collect(Collectors.toList());
		projects = projects.stream().sorted(Comparator.comparing(Project::getStatus).reversed().thenComparing(Project::getDateFrom)).collect(Collectors.toList());
		model.addAttribute("projects", projects);
		return "zamestnanec/index";
	}
         @GetMapping("index/filter")
    public String filter(Model model, Authentication auth, @RequestParam("type") String filter,
            @RequestParam("from") String dateFrom, @RequestParam("to") String dateTo) { //parametry: filter, dateFrom, dateTo 
        Employee user = employeeService.findByLogin(auth.getName());
        List<Project> projects = projectService.filter(filter, dateFrom, dateTo);
        projects = projects.stream().filter(x -> x.getEmployeeCollection().contains(user)).collect(Collectors.toList());
        projects = projects.stream().sorted(Comparator.comparing(Project::getStatus).reversed().
                thenComparing(Project::getDateFrom)).collect(Collectors.toList());
        model.addAttribute("projects", projects);
        return "zamestnanec/index";

    }
	
	@GetMapping("/project-page/{project}")
	public String projectPage(Model model, @PathVariable("project") Integer project) {
		Project p = projectService.findByID(project);
		List<Form> forms = anketService.findAll();
		Form f = anketService.findByProject(p);
		model.addAttribute("proj", p);
		if (f != null) {
			model.addAttribute("formID", f.getFormId());
                        model.addAttribute("status", projectService.dataStatus(p));
			return "zamestnanec/project-page-q";
		}
		return "zamestnanec/project-page";
	}
	
	@GetMapping("/project-page/employees/{project}")
	public String projectEmployeesPage(Model model, @PathVariable("project") Integer project) {
		Project p = projectService.findByID(project);
		model.addAttribute("proj", p);
		return "zamestnanec/employees";
	}
	
	@RequestMapping("/new-questionnaire-detail/{project}")
	public String newQD(Model model,@PathVariable("project") Integer project) {
		model.addAttribute("forProject", project);
		return "zamestnanec/new-questionnaire-detail";
	}
	
	@PostMapping(value="/new-questionnaire-detail", params={"nForm"})
	public String newForm(Model model,
			@RequestParam("projectID") Integer projectID,
			@RequestParam("new-project-from") String npf,
			@RequestParam("new-project-to") String npt,
			@RequestParam("new-project-title") Integer answerCount,
			@RequestParam("new-project-description") String desc) {
		Form f = new Form();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date npfd = formatter.parse(npf);
			Date nptd = formatter.parse(npt);
		
		f.setAnswerCount(0);
		f.setDateFrom(npfd);
		f.setDescription(desc);
		f.setDateTo(nptd);
		f.setProject(projectService.findByID(projectID));
		f.setWantedCount(answerCount);
		f.setQuestionCollection(new ArrayList<Question>());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		anketService.persist(f);
		return "redirect:/zamestnanec/new-questionnaire/"+f.getFormId();
	}
	
	@GetMapping("/new-questionnaire/{formID}")
	public String newQ(@PathVariable("formID") Integer formID, Model model) {
		String[] questions = new String[50]; //lol
		Form f = anketService.find(formID);
		model.addAttribute("questions", questions);
		model.addAttribute("projectID", f.getProject().getId());
		model.addAttribute("formID", f.getFormId());
		return "zamestnanec/new-questionnaire";
	}
	
	@PostMapping(value="/new-questionnaire")
	public String saveQ(@RequestBody MultiValueMap<String,String> formData, Model model) {
		int i = 0;
		int formID = 0;
		int projectID = 0;
		for (Entry<String, List<String>> q : formData.entrySet()) {
			System.out.println(q.getKey() + " : "+q.getValue().get(0));
			if (q.getKey().startsWith("project")) {
				projectID = Integer.parseInt(q.getValue().get(0));
				System.out.println(projectID);
			}
			if (q.getKey().startsWith("form")) {
				formID = Integer.parseInt(q.getValue().get(0));
				System.out.println(formID);
			}
			if (q.getKey().startsWith("question")) {
				if (!q.getValue().get(0).isEmpty()) {
					Form f = anketService.find(formID);
					Question qt = new Question();
					qt.setForm(f);
					qt.setQText(q.getValue().get(0));
					qt.setQuestionPK(new QuestionPK(++i,f.getFormId()));
					f.getQuestionCollection().add(qt);
                                        System.out.println("NEW COLLECTION SIZE: "+f.getQuestionCollection().size());
					anketService.persist(f);
				}
			}
		}
		
		return "redirect:/zamestnanec/project-page/"+projectID;
	}
	
	@RequestMapping("/questionnaire/{formID}")
	public String viewQ(Model model, @PathVariable("formID") Integer formID) {
		Form f = anketService.find(formID);
		model.addAttribute("form", f);
		Collection<Question> questions = f.getQuestionCollection();
                model.addAttribute("questions", questions);
		return "zamestnanec/questionnaire";
	}
	
	@RequestMapping("/questionnaire-detail/{formID}")
	public String viewQD(Model model, @PathVariable("formID") Integer formID) {
		Form f = anketService.find(formID);
		model.addAttribute("form", f);
		return "zamestnanec/questionnaire-detail";
	}
    @RequestMapping(value="/project/{id}/export", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(@PathVariable(value = "id") int id) {
        Project proj = projectService.findByID(id);
        Form form = anketService.findByProject(proj);
        return new FileSystemResource(new File("anketa"+form.getFormId()+".xlsx"));
    }
}
