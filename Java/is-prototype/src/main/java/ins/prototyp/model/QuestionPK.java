/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author kulyndar;
 */
@Embeddable
public class QuestionPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "q_id")
    private int qId;
    @Basic(optional = false)
    @Column(name = "form_id")
    private int formId;

    public QuestionPK() {
    }

    public QuestionPK(int qId, int formId) {
        this.qId = qId;
        this.formId = formId;
    }

    public int getQId() {
        return qId;
    }

    public void setQId(int qId) {
        this.qId = qId;
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) qId;
        hash += (int) formId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof QuestionPK)) {
            return false;
        }
        QuestionPK other = (QuestionPK) object;
        if (this.qId != other.qId) {
            return false;
        }
        if (this.formId != other.formId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ins.mavenproject1.QuestionPK[ qId=" + qId + ", formId=" + formId + " ]";
    }
    
}
