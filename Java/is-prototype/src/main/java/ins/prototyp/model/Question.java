/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kulyndar;
 */
@Entity
@Table(name = "question")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Question.findAll", query = "SELECT q FROM Question q")
    ,
        @NamedQuery(name = "Question.findByQId", query = "SELECT q FROM Question q WHERE q.questionPK.qId = :qId")
    ,
        @NamedQuery(name = "Question.findByFormId", query = "SELECT q FROM Question q WHERE q.questionPK.formId = :formId")
    ,
        @NamedQuery(name = "Question.findByQText", query = "SELECT q FROM Question q WHERE q.qText = :qText")
    })
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected QuestionPK questionPK;
    @Basic(optional = false)
    @Column(name = "q_text")
    private String qText;
 
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "question")
    private Collection<Answer> answerCollection;
    @JoinColumn(name = "form_id", referencedColumnName = "form_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Form form;

    public Question() {
    }

    public Question(QuestionPK questionPK) {
        this.questionPK = questionPK;
    }

    public Question(QuestionPK questionPK, String qText) {
        this.questionPK = questionPK;
        this.qText = qText;

    }

    public Question(int qId, int formId) {
        this.questionPK = new QuestionPK(qId, formId);
    }

    public QuestionPK getQuestionPK() {
        return questionPK;
    }

    public void setQuestionPK(QuestionPK questionPK) {
        this.questionPK = questionPK;
    }

    public String getQText() {
        return qText;
    }

    public void setQText(String qText) {
        this.qText = qText;
    }

   
    @XmlTransient
    public Collection<Answer> getAnswerCollection() {
        return answerCollection;
    }

    public void setAnswerCollection(Collection<Answer> answerCollection) {
        this.answerCollection = answerCollection;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (questionPK != null ? questionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Question)) {
            return false;
        }
        Question other = (Question) object;
        if ((this.questionPK == null && other.questionPK != null) ||
                (this.questionPK != null && !this.questionPK.equals(other.questionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ins.mavenproject1.Question[ questionPK=" + questionPK + " ]";
    }
    
}
