/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kulyndar;
 */
@Entity
@Table(name = "project")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Project.findAll", query = "SELECT p FROM Project p")
    ,
        @NamedQuery(name = "Project.findByProjName", query = "SELECT p FROM Project p WHERE p.projName = :projName")
    ,
        @NamedQuery(name = "Project.findByDateFrom", query = "SELECT p FROM Project p WHERE p.dateFrom >= :dateFrom")
    ,
        @NamedQuery(name = "Project.findByDateTo", query = "SELECT p FROM Project p WHERE p.dateTo <= :dateTo")
    ,
        @NamedQuery(name = "Project.findByInterval", query = "SELECT p FROM Project p WHERE p.dateTo <= :dateTo AND p.dateFrom >= :dateFrom")
        ,@NamedQuery(name = "Project.findByIntervalList", query = "SELECT p FROM Project p WHERE (p.dateTo <= :dateTo AND p.dateTo >= :dateFrom) OR (p.dateFrom >= :dateFrom AND p.dateFrom <= :dateTo) OR (p.dateFrom<=:dateFrom AND p.dateTo >=:dateTo)"),
        @NamedQuery(name = "Project.findByStatus", query = "SELECT p FROM Project p WHERE p.status = :status")
    ,
        @NamedQuery(name = "Project.findByDescription", query = "SELECT p FROM Project p WHERE p.description = :description")})
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@GeneratedValue
	@Column(name = "id", nullable = false) 
	private Integer id;
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Basic(optional = false)
    @Column(name = "proj_name")
    private String projName;
    @Basic(optional = false)
    @Column(name = "date_from")
    @Temporal(TemporalType.DATE)
    private Date dateFrom;
    @Basic(optional = false)
    @Column(name = "date_to")
    @Temporal(TemporalType.DATE)
    private Date dateTo;
    @Basic(optional = false)
    @Column(name = "status")
    private String status;
    @Column(name = "description")
    private String description;
    @ManyToMany(mappedBy = "projectCollection")
    private Collection<Employee> employeeCollection;
    @JoinColumn(name = "supervisor", referencedColumnName = "login")
    @ManyToOne(optional = false)
    private Employee supervisor;
    @JoinColumn(name = "client", referencedColumnName = "cli_name")
    @ManyToOne(optional = false)
    private Client client;
    
    @ManyToOne(optional = true)
    private Specialization specialization;
    
    public Project() {
    }

    public Project(String projName) {
        this.projName = projName;
    }

    public Project(String projName, Date dateFrom, Date dateTo, String status) {
        this.projName = projName;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.status = status;
    }

    public String getProjName() {
        return projName;
    }

    public void setProjName(String projName) {
        this.projName = projName;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }
    
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Employee> getEmployeeCollection() {
        return employeeCollection;
    }

    public void setEmployeeCollection(Collection<Employee> employeeCollection) {
        this.employeeCollection = employeeCollection;
    }

    public Employee getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Employee supervisor) {
        this.supervisor = supervisor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (projName != null ? projName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Project)) {
            return false;
        }
        Project other = (Project) object;
        if ((this.projName == null && other.projName != null) || (this.projName != null && !this.projName.equals(other.projName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ins.mavenproject1.Project[ projName=" + projName + " ]";
    }
    
}
