/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author kulyndar;
 */
@Embeddable
public class AnswerPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "q_id")
    private int qId;
    @Basic(optional = false)
    @Column(name = "a_id")
    private int aId;

    public AnswerPK() {
    }

    public AnswerPK(int qId, int aId) {
        this.qId = qId;
        this.aId = aId;
    }

    public int getQId() {
        return qId;
    }

    public void setQId(int qId) {
        this.qId = qId;
    }

    public int getAId() {
        return aId;
    }

    public void setAId(int aId) {
        this.aId = aId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) qId;
        hash += (int) aId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnswerPK)) {
            return false;
        }
        AnswerPK other = (AnswerPK) object;
        if (this.qId != other.qId) {
            return false;
        }
        if (this.aId != other.aId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ins.mavenproject1.AnswerPK[ qId=" + qId + ", aId=" + aId + " ]";
    }
    
}
