/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kulyndar;
 */
@Entity
@Table(name = "form")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Form.findAll", query = "SELECT f FROM Form f")
    ,
        @NamedQuery(name = "Form.findByFormId", query = "SELECT f FROM Form f WHERE f.formId = :formId")
    ,
        @NamedQuery(name = "Form.findByDateFrom", query = "SELECT f FROM Form f WHERE f.dateFrom = :dateFrom")
    ,
        @NamedQuery(name = "Form.findByDateTo", query = "SELECT f FROM Form f WHERE f.dateTo = :dateTo")
    ,
        @NamedQuery(name = "Form.findByWantedCount", query = "SELECT f FROM Form f WHERE f.wantedCount = :wantedCount")
    ,
        @NamedQuery(name = "Form.findByDescription", query = "SELECT f FROM Form f WHERE f.description = :description")
    ,
        @NamedQuery(name = "Form.findByFormMap", query = "SELECT f FROM Form f WHERE f.formMap = :formMap")
,
        @NamedQuery(name = "Form.findByProject", query = "SELECT f FROM Form f WHERE f.project = :project")
})
public class Form implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "form_id")
    private Integer formId;
    @JoinColumn(name = "project", referencedColumnName = "id")
    @OneToOne(optional = false)
    private Project project;
    @Basic(optional = true)
    @Column(name = "date_from")
    @Temporal(TemporalType.DATE)
    private Date dateFrom;
    @Basic(optional = true)
    @Column(name = "date_to")
    @Temporal(TemporalType.DATE)
    private Date dateTo;
    @Basic(optional = true)
    @Column(name = "wanted_count")
    private int wantedCount;
    
    @Column(name = "answer_count")
    private int answerCount;
    @Column(name = "description")
    private String description;
    @Column(name = "form_map")
    private String formMap;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "form")
    private Collection<Question> questionCollection;

    public Form() {
    }

    public Form(Integer formId) {
        this.formId = formId;
    }

    public Form(Integer formId, Date dateFrom, Date dateTo, int wantedCount) {
        this.formId = formId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.wantedCount = wantedCount;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public int getWantedCount() {
        return wantedCount;
    }

    public void setWantedCount(int wantedCount) {
        this.wantedCount = wantedCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFormMap() {
        return formMap;
    }

    public void setFormMap(String formMap) {
        this.formMap = formMap;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
    
    @XmlTransient
    public Collection<Question> getQuestionCollection() {
        return questionCollection;
    }

    public void setQuestionCollection(Collection<Question> questionCollection) {
        this.questionCollection = questionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (formId != null ? formId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Form)) {
            return false;
        }
        Form other = (Form) object;
        if ((this.formId == null && other.formId != null) || (this.formId != null && !this.formId.equals(other.formId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ins.mavenproject1.Form[ formId=" + formId + " ]";
    }

    public int getAnswerCount() {
        return answerCount;
    }

    public void setAnswerCount(int answerCount) {
        this.answerCount = answerCount;
    }
    
    
}
