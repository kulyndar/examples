/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kulyndar;
 */
@Entity
@Table(name = "answer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Answer.findAll", query = "SELECT a FROM Answer a")
    ,
        @NamedQuery(name = "Answer.findByQId", query = "SELECT a FROM Answer a WHERE a.answerPK.qId = :qId")
    ,
        @NamedQuery(name = "Answer.findByAId", query = "SELECT a FROM Answer a WHERE a.answerPK.aId = :aId")
    ,
        @NamedQuery(name = "Answer.findByAText", query = "SELECT a FROM Answer a WHERE a.aText = :aText")})
public class Answer implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AnswerPK answerPK;
    @Basic(optional = false)
    @Column(name = "a_text")
    private String aText;
    @JoinColumns({
        @JoinColumn(name = "q_id", referencedColumnName = "q_id", insertable = false, updatable = false)
        ,
            @JoinColumn(name = "form_id", referencedColumnName = "form_id")})
    @ManyToOne(optional = false)
    private Question question;

    public Answer() {
    }

    public Answer(AnswerPK answerPK) {
        this.answerPK = answerPK;
    }

    public Answer(AnswerPK answerPK, String aText) {
        this.answerPK = answerPK;
        this.aText = aText;
    }

    public Answer(int qId, int aId) {
        this.answerPK = new AnswerPK(qId, aId);
    }

    public AnswerPK getAnswerPK() {
        return answerPK;
    }

    public void setAnswerPK(AnswerPK answerPK) {
        this.answerPK = answerPK;
    }

    public String getAText() {
        return aText;
    }

    public void setAText(String aText) {
        this.aText = aText;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (answerPK != null ? answerPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Answer)) {
            return false;
        }
        Answer other = (Answer) object;
        if ((this.answerPK == null && other.answerPK != null) || (this.answerPK != null && !this.answerPK.equals(other.answerPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ins.mavenproject1.Answer[ answerPK=" + answerPK + " ]";
    }
    
}
