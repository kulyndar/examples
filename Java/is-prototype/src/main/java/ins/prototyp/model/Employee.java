/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author kulyndar;
 */
@Entity
@Table(name = "employee")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e")
    ,
        @NamedQuery(name = "Employee.findByLogin", query = "SELECT e FROM Employee e WHERE e.login = :login")
    ,
        @NamedQuery(name = "Employee.findByEmpName", query = "SELECT e FROM Employee e WHERE e.login = :empName")})
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "login")
    private String login;
    @Basic(optional = false)
    @Column(name = "emp_name")
    private String empName;
    @Column(name = "password", nullable = false)
	private String password;
    @JoinTable(name = "proj_emp", joinColumns = {
        @JoinColumn(name = "emp", referencedColumnName = "login")}, inverseJoinColumns
            = {
                @JoinColumn(name = "project", referencedColumnName = "proj_name")})
    @ManyToMany
    private Collection<Project> projectCollection;
    @JoinTable(name = "emp_spec", joinColumns = {
        @JoinColumn(name = "emp", referencedColumnName = "login")}, inverseJoinColumns
            = {
                @JoinColumn(name = "spec", referencedColumnName = "spec_name")})
    @ManyToMany
    private Collection<Specialization> specializationCollection;


    public Employee() {
    }

    public Employee(String login) {
        this.login = login;
    }

    public Employee(String login, String empName) {
        this.login = login;
        this.empName = empName;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setLogin(String login) {
        this.login = login;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    @XmlTransient
    public Collection<Project> getProjectCollection() {
        return projectCollection;
    }

    public void setProjectCollection(Collection<Project> projectCollection) {
        this.projectCollection = projectCollection;
    }

    @XmlTransient
    public Collection<Specialization> getSpecializationCollection() {
        return specializationCollection;
    }

    public void setSpecializationCollection(Collection<Specialization> specializationCollection) {
        this.specializationCollection = specializationCollection;
    }



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (login != null ? login.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.login == null && other.login != null) || (this.login != null && !this.login.equals(other.login))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ins.mavenproject1.Employee[ login=" + login + " ]";
    }
    
}
