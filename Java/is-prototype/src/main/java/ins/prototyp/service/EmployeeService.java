/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.service;

import ins.prototyp.dao.EmployeeDao;
import ins.prototyp.dao.ProjectDao;
import ins.prototyp.model.Employee;
import ins.prototyp.model.Project;
import ins.prototyp.model.Specialization;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author kulyndar;
 */
@Service
public class EmployeeService {

    @Autowired
    EmployeeDao empDao;

    @Autowired
    ProjectDao dao;
    
    @Transactional
    public void persist(Employee emp) {
    	empDao.persist(emp);
    }
    
    public Employee findByLogin(String login) {
    	return empDao.findByName(login);
    }
    
    public List<EmpDTO> getEmployees(Date from, Date to) {
        List<EmpDTO> list = new ArrayList<>();
        List<Employee> emps = empDao.findAll();
        System.out.println("EMP spec");
        for (Employee emp : emps) {
            EmpDTO dto = new EmpDTO();
            dto.setLogin(emp.getLogin());
            dto.setName(emp.getEmpName());
            List<Project> projects = dao.findInIntervalList(from, to);
            int projCount = 0;
            for (Project project : projects) {
                if (project.getEmployeeCollection().contains(emp)) {
                    projCount++;
                }
            }
            System.out.println(emp.getSpecializationCollection().size());
            dto.setProjectCount(projCount);
            dto.setSpecializations(emp.getSpecializationCollection());
            dto.setSpecNames(dto.getSpecializations().stream().map(spec->spec.getSpecName()).collect(Collectors.toList()));
            list.add(dto);
        }
        System.out.println("DTO spec");
        for (EmpDTO empDTO : list) {
            System.out.println(empDTO.getSpecializations().size());
        }
        return list;
    }
    public List<Employee> findAll(){
        return empDao.findAll();
    }
    @Transactional
    public void unsubscripeFromProject(int proj, String empName) {
        Project project = dao.findByID(proj);
        Employee emp = empDao.findByName(empName);
        project.getEmployeeCollection().remove(emp);
        dao.persist(project);
    }
    @Transactional
    public void subscribeToProject(int proj, String empName) {
        Project project = dao.findByID(proj);
        Employee emp = empDao.findByName(empName);
        project.getEmployeeCollection().add(emp);
        dao.persist(project);
    }
}
