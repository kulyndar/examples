/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.service;

import ins.prototyp.model.Specialization;
import java.util.Collection;

/**
 *
 * @author kulyndar;
 */
public class EmpDTO {
    
    private String login;
    private String name;
    private int projectCount;
    private Collection<Specialization> specializations;
    private  Collection<String> specNames;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getProjectCount() {
        return projectCount;
    }

    public Collection<String> getSpecNames() {
        return specNames;
    }

    public void setSpecNames(Collection<String> specNames) {
        this.specNames = specNames;
    }

    public void setProjectCount(int projectCount) {
        this.projectCount = projectCount;
    }

    public Collection<Specialization> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(Collection<Specialization> specializations) {
        this.specializations = specializations;
    }

    
   
    
}
