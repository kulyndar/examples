/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.service;

import ins.prototyp.dao.FormDao;
import ins.prototyp.dao.ProjectDao;
import ins.prototyp.model.Answer;
import ins.prototyp.model.Form;
import ins.prototyp.model.Project;
import ins.prototyp.model.Question;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kulyndar;
 */
@Service
public class AnketService {
    
    @Autowired
    FormDao dao;
    
    @Autowired
    ProjectDao projDao;
    
    public void createForm(){
        Form form = new Form();
        
    }
    @Transactional
    public void persist(Form form){
        dao.persist(form);
    }
    public File createAnketFile(int aId) throws IOException {
        File file = new File("anketa" + aId + ".xlsx");
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;

    }

    public void writeAnswers(Answer[] answers) throws IOException, InvalidFormatException {
        int formID = answers[0].getQuestion().getQuestionPK().getFormId();
        File file = createAnketFile(formID);
        Workbook workbook;
        FileInputStream in = null;
        Sheet sheet;
        if(file.length()!=0){
            in = new FileInputStream(file);
            workbook = WorkbookFactory.create(in);
            sheet = workbook.getSheet("Answers");
        }else{
            workbook = new XSSFWorkbook();
            sheet = workbook.createSheet("Answers");
        }
        
        
        int rowNumbers = sheet.getLastRowNum();
        /*Create header with questions*/
        if (rowNumbers==0) {
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 12);
            

            // Create a CellStyle with the font
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            headerCellStyle.setShrinkToFit(true);
            headerCellStyle.setWrapText(true);
            Row header = sheet.createRow(0);

            for (int i = 0; i < answers.length; i++) {
                Cell cell = header.createCell(i);
                cell.setCellValue(answers[i].getQuestion().getQText());
                cell.setCellStyle(headerCellStyle);
                
            }
            

        }
        /*Write answers*/
        ++rowNumbers;
        Row row = sheet.createRow(rowNumbers);
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setShrinkToFit(true);
        cellStyle.setWrapText(true);
        for (int i = 0; i < answers.length; i++) {
            Answer answer = answers[i];
            Cell cell = row.createCell(i);
            cell.setCellValue(answer.getAText()); 
            cell.setCellStyle(cellStyle);
        }
        if(in!=null){
            in.close();
        }
        FileOutputStream out = new FileOutputStream(file);
        workbook.write(out);
        out.close();
        
    }
    public List<Question> getQuestions(String projname){
        Project proj = projDao.find(projname);
        Form form = dao.findByProject(proj);
        return (List<Question>) form.getQuestionCollection();
        
    }
    public List<Answer> getAnswers(int qId){
        return dao.findAllAnswers(qId);
    }
    public Form findByProject(Project proj){
        return dao.findByProject(proj);
    }
    public List<Form> findAll(){
        return dao.findAll();
    }
    public Form find(int id){
        return dao.find(id);
    }
    @Transactional
    public void inreaseCount(Form form){
        form.setAnswerCount(form.getAnswerCount()+1);
        dao.persist(form);
    }
    @Transactional
    public List<Question> getQuestions(Form f){
        return dao.findQuestions(f);
    }

}
