/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.service;

import ins.prototyp.dao.ClientDao;
import ins.prototyp.dao.EmployeeDao;
import ins.prototyp.dao.FormDao;
import ins.prototyp.dao.ProjectDao;
import ins.prototyp.dao.SpecializationDao;
import ins.prototyp.model.Client;
import ins.prototyp.model.Employee;
import ins.prototyp.model.Form;
import ins.prototyp.model.Project;
import ins.prototyp.model.Specialization;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kulyndar;
 */
@Service
public class ProjectService {

    final static String FORMAT = "yyyy-MM-dd";
    final static String OPENED = "open";
    final static String CLOSED = "closed";
    @Autowired
    ClientDao clientDao;

    @Autowired
    EmployeeDao empDao;

    @Autowired
    SpecializationDao specDao;

    @Autowired
    ProjectDao dao;

    @Autowired
    FormDao formDao;

    @Transactional
    public void persist(Project proj) {
        dao.persist(proj);
    }

    public Project findByID(Integer ID) {
        return dao.findByID(ID);
    }

    public Project find(String proj) {
        return dao.find(proj);
    }

    public List<Project> filter(String filter, String from, String to) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(FORMAT);
            Date dateFrom;
            Date dateTo;
            if ("".equals(from)) {
                dateFrom = null;
            } else {
                dateFrom = format.parse(from);
            }
            if ("".equals(to)) {
                dateTo = null;
            } else {
                dateTo = format.parse(to);
            }
            List<Project> projs = new ArrayList();
            if (dateFrom != null || dateTo != null) {
                if (dateFrom != null && dateTo != null) {
                    projs = dao.findInInterval(dateFrom, dateTo);

                } else if (dateFrom != null) {
                    projs = dao.findByDateFrom(dateFrom);
                } else {
                    projs = dao.findByDateTo(dateTo);
                }
                if ("open-projects-only".equals(filter)) {
                    List<Project> projToAdd = new ArrayList<>();
                    for (Project proj : projs) {
                        if (proj.getStatus().equals(OPENED)) {
                            projToAdd.add(proj);
                        }
                    }
                    projs = projToAdd;
                } else if ("closed-projects-only".equals(filter)) {
                    List<Project> projToAdd = new ArrayList<>();
                    for (Project proj : projs) {
                        if (proj.getStatus().equals(CLOSED)) {
                            projToAdd.add(proj);
                        }
                    }
                    projs = projToAdd;
                }
            } else {
                switch (filter) {
                    case "open-projects-only":
                        projs = dao.findByStatus(OPENED);
                        break;
                    case "closed-projects-only":
                        projs = dao.findByStatus(CLOSED);
                        break;
                    default:
                        projs = dao.findAll();

                }
            }

            return projs;
        } catch (ParseException ex) {
            Logger.getLogger(ProjectService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Transactional
    public void createProject(String name, String client, String supervisor, String specialization, String from, String to,
            String description) {

        try {
            Project proj = new Project();
            proj.setProjName(name);
            proj.setClient(clientDao.find(client));
            proj.setSupervisor(empDao.findByName(supervisor));
            List<Employee> list = new ArrayList();
            list.add(empDao.findByName(supervisor));
            proj.setEmployeeCollection(list);
            proj.setSpecialization(specDao.find(specialization));
            proj.setStatus(OPENED);
            DateFormat format = new SimpleDateFormat(FORMAT);
            proj.setDateFrom(format.parse(from));
            proj.setDateTo(format.parse(to));
            proj.setDescription(description);
            dao.persist(proj);
        } catch (ParseException ex) {
            Logger.getLogger(ProjectService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setEmployees(List<EmpDTO> emp, String project) {
        Project proj = dao.find(project);
        Collection<Employee> list = proj.getEmployeeCollection();
        if (list == null) {
            list = new ArrayList<>();
        }
        for (EmpDTO employee : emp) {
            list.add(empDao.findByName(employee.getName()));
        }
        dao.persist(proj);

    }

    public Collection<Employee> getProjectEmployees(String project) {
        Project proj = dao.find(project);
        return proj.getEmployeeCollection();
    }

    public ProjectDTO mapToDto(Project project) {
        ProjectDTO dto = new ProjectDTO();
        dto.setName(project.getProjName());
        dto.setCustomer(project.getClient().getCliName());
        dto.setDateFrom(project.getDateFrom());
        dto.setDateTo(project.getDateTo());
        dto.setSupervisor(project.getSupervisor().getEmpName());
        dto.setStatus("open".equals(project.getStatus()) ? "Otevřený" : "Zavřený");
        return dto;
    }

    public List<Project> findAll() {
        return dao.findAll();
    }

    public List<Specialization> findAllSpec() {
        return specDao.findAll();
    }

    public List<Client> findAllClients() {
        return clientDao.findAll();
    }

    public int dataStatus(Project proj) {
        Form form = formDao.findByProject(proj);
        int wanted = form.getWantedCount();
        int answered = form.getAnswerCount();
        System.out.println("DATA STATUS");
        System.out.println(((float)answered / wanted) * 100);
        return Math.round(((float) answered / wanted) * 100);
    }

    public Date getDataDateFrom(Project proj) {
        Form form = formDao.findByProject(proj);

        return form.getDateFrom();
    }

    public Date getDataDateTo(Project proj) {
        Form form = formDao.findByProject(proj);

        return form.getDateTo();
    }
    @Transactional
    public void closeProject(int projname) {
        Project proj = dao.findByID(projname);
        proj.setStatus("closed");
        dao.persist(proj);
    }

    public boolean isOpened(String projname) {
        Project proj = dao.find(projname);
        return "open".equals(proj.getStatus());
    }

    public boolean isOpened(Project proj) {
        return "open".equals(proj.getStatus());
    }
}
