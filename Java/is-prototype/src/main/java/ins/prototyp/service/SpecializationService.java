/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.service;

import ins.prototyp.dao.ClientDao;
import ins.prototyp.dao.EmployeeDao;
import ins.prototyp.dao.FormDao;
import ins.prototyp.dao.ProjectDao;
import ins.prototyp.dao.SpecializationDao;
import ins.prototyp.model.Client;
import ins.prototyp.model.Employee;
import ins.prototyp.model.Form;
import ins.prototyp.model.Project;
import ins.prototyp.model.Specialization;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kulyndar;
 */
@Service
public class SpecializationService {

    @Autowired
    SpecializationDao clientDao;

    @Transactional
    public void persist(Specialization client) {
    	clientDao.persist(client);
    }
    
    @Transactional
    public void refresh(){
        clientDao.refresh();
    }
    @Transactional
    public Specialization find(String specName){
        return clientDao.find(specName);
    }
}
