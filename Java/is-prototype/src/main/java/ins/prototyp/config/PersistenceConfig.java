package ins.prototyp.config;

import com.jolbox.bonecp.BoneCPDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySources(
        {@PropertySource("classpath:jpa.properties"),
         @PropertySource("classpath:jdbc.properties")})
@EnableTransactionManagement
@ComponentScan(basePackages = "ins.prototyp.dao")
public class PersistenceConfig {

    @Autowired
    private Environment environment;

    @Bean(name = "dataSource")
    public DataSource dataSource() {
        final BoneCPDataSource ds = new BoneCPDataSource();
        ds.setDriverClass(environment.getRequiredProperty("jdbc.driverClassName"));
        ds.setJdbcUrl(environment.getRequiredProperty("jdbc.url"));
        ds.setUsername(environment.getRequiredProperty("jdbc.username"));
        ds.setPassword(environment.getRequiredProperty("jdbc.password"));
        return ds;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource);
        emf.setJpaVendorAdapter(new EclipseLinkJpaVendorAdapter());
        emf.setPackagesToScan("ins.prototyp.model");

        final Properties props = new Properties();
        props.setProperty("databasePlatform", environment.getRequiredProperty("jpa.platform"));
        props.setProperty("generateDdl", "true");
        props.setProperty("showSql", "true");
        props.setProperty("eclipselink.weaving", "static");
        props.setProperty("eclipselink.ddl-generation", "drop-and-create-tables");
        emf.setJpaProperties(props);
        return emf;
    }

    @Bean(name = "txManager")
    JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
    
    @Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    	/*
		auth.jdbcAuthentication().dataSource(dataSource())
		.usersByUsernameQuery("select login, password, TRUE as enabled from employee where login=?")
		.authoritiesByUsernameQuery("select login, 'ZAMESTNANEC' as userrole from employee where login =?")
		.passwordEncoder(passwordEncoder());
		*/
    	
    	auth.inMemoryAuthentication()
    	.withUser("mnovy")
    	.password("{noop}asdf")
    	.roles("ZAMESTNANEC");
        
        auth.inMemoryAuthentication()
    	.withUser("michaela")
    	.password("{noop}asdf")
    	.roles("ZAMESTNANEC");
        
        auth.inMemoryAuthentication()
    	.withUser("mztraceny")
    	.password("{noop}asdf")
    	.roles("MANAZER");
        
    	auth.inMemoryAuthentication()
    	.withUser("user")
    	.password("{noop}asdf")
    	.roles("BRIGADNIK");
        
        
	}
    /*
	@Bean(name = "passwordEncoder")
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    */
    
}
