package ins.prototyp;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ins.prototyp.dao.EmployeeDao;
import ins.prototyp.model.Client;
import ins.prototyp.model.Employee;
import ins.prototyp.model.Form;
import ins.prototyp.model.Project;
import ins.prototyp.model.Question;
import ins.prototyp.model.QuestionPK;
import ins.prototyp.model.Specialization;
import ins.prototyp.service.AnketService;
import ins.prototyp.service.ClientService;
import ins.prototyp.service.EmployeeService;
import ins.prototyp.service.ProjectService;
import ins.prototyp.service.SpecializationService;

@Component
public class TestData {

    @Autowired
    EmployeeService employeeService;
    @Autowired
    ProjectService projectService;
    @Autowired
    ClientService clientService;
    @Autowired
    SpecializationService specializationService;
    @Autowired
    AnketService formService;

    @PostConstruct
    private void setupData() {
        Specialization s1 = new Specialization();
        s1.setSpecName("IT");
        specializationService.persist(s1);
        Specialization s2 = new Specialization();
        s2.setSpecName("Telekomunikace");
        specializationService.persist(s2);

        Employee e1 = new Employee();
        e1.setEmpName("Michal Nový");
        e1.setLogin("mnovy");
        e1.setPassword("michal");
        Collection<Specialization> spec1 = new ArrayList<>();
        spec1.add(s2);
        spec1.add(s1);
        e1.setSpecializationCollection(spec1);
        employeeService.persist(e1);

        Employee e2 = new Employee();
        e2.setEmpName("Marek Ztracený");
        e2.setLogin("mztraceny");
        e2.setPassword("marek");
        Collection<Specialization> spec2 = new ArrayList<>();
        spec2.add(s1);
        e2.setSpecializationCollection(spec2);
        employeeService.persist(e2);

        Employee e3 = new Employee();
        e3.setEmpName("Michaela Mladá");
        e3.setLogin("michaela");
        e3.setPassword("mich");
        Collection<Specialization> spec3 = new ArrayList<>();
        spec3.add(s2);
        e3.setSpecializationCollection(spec3);
        employeeService.persist(e3);
        
        Client c1 = new Client();
        c1.setCliName("Vodafone");
        clientService.persist(c1);

        Client c2 = new Client();
        c2.setCliName("IBM");
        clientService.persist(c2);

//		Specialization s1 = new Specialization();
//		Collection<Employee> semp1 = new ArrayList<>();
//		semp1.add(e1);
//		s1.setEmployeeCollection(semp1);
//		s1.setSpecName("IT");
//		specializationService.persist(s1);
//        Specialization s2 = new Specialization();
//        Collection<Employee> semp2 = new ArrayList<>();
//        semp2.add(e2);
//        semp2.add(e1);
//        s2.setEmployeeCollection(semp2);
//        s2.setSpecName("Telekomunikace");
//        specializationService.persist(s2);
//                Employee e11 = employeeService.findByLogin(e1.getLogin());
//                Collection<Specialization> spec1 = new ArrayList<>();
//                spec1.add(s1);
//                spec1.add(s2);
//                e11.setSpecializationCollection(spec1);
//                employeeService.persist(e11);
//                
//                Employee e22 = employeeService.findByLogin(e2.getLogin());
//                Collection<Specialization> spec2 = new ArrayList<>();
//                spec2.add(s1);
//                e22.setSpecializationCollection(spec2);
//                employeeService.persist(e22);
        Project p1 = new Project();
        p1.setClient(c1);
        p1.setDateFrom(Date.valueOf("2018-01-01"));
        p1.setDateTo(Date.valueOf("2018-12-31"));
        p1.setDescription("Popis projektu");
        Collection<Employee> cemp1 = new ArrayList<>();
        cemp1.add(e1);
        cemp1.add(e2);
        p1.setEmployeeCollection(cemp1);
        p1.setProjName("Průzkum spokojenosti");
        p1.setStatus("open");
        p1.setSupervisor(e2);
        p1.setSpecialization(s1);
        projectService.persist(p1);

        Project p2 = new Project();
        p2.setClient(c2);
        p2.setDateFrom(Date.valueOf("2018-04-02"));
        p2.setDateTo(Date.valueOf("2018-08-12"));
        p2.setDescription("Popis projektu");
        Collection<Employee> cemp2 = new ArrayList<>();
        cemp2.add(e1);
        cemp2.add(e2);
        p2.setEmployeeCollection(cemp2);
        p2.setProjName("Feedback pro IS");
        p2.setStatus("closed");
        p2.setSupervisor(e2);
        p2.setSpecialization(s1);
        projectService.persist(p2);
        
        Project p3 = new Project();
        p3.setClient(c2);
        p3.setDateFrom(Date.valueOf("2018-04-02"));
        p3.setDateTo(Date.valueOf("2018-08-12"));
        p3.setDescription("Popis projektu");
        Collection<Employee> cemp3 = new ArrayList<>();
        cemp3.add(e1);
        cemp3.add(e2);
        p3.setEmployeeCollection(cemp2);
        p3.setProjName("Bluemix výzkum");
        p3.setStatus("open");
        p3.setSupervisor(e2);
        p3.setSpecialization(s1);
        projectService.persist(p3);
        
//        Form form = new Form();
//        form.setAnswerCount(200);
//        form.setDateFrom(p3.getDateFrom());
//        form.setDateTo(p3.getDateTo());
//        form.setDescription("Form for project Bluemix výzkum");
//        form.setWantedCount(500);
//        
//        Question q1 = new Question();
//        q1.setForm(form);
//        q1.setQText("Who are you");
//        q1.setQuestionPK(new QuestionPK(1, 1));
//        
//        Question q2 = new Question();
//        q2.setForm(form);
//        q2.setQText("Where are you from");
//        q2.setQuestionPK(new QuestionPK(2, 1));
//        
//        Question q3 = new Question();
//        q3.setForm(form);
//        q3.setQText("Favorite color");
//        q3.setQuestionPK(new QuestionPK(3, 1));
//        
//        Collection<Question> que = new ArrayList<>();
//        que.add(q1);
//        que.add(q2);
//        que.add(q3);
//        
//        form.setQuestionCollection(que);
//        form.setProject(p3);
//        formService.persist(form);
        

    }
}
