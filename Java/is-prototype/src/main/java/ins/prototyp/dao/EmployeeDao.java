/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.dao;

import ins.prototyp.model.Employee;
import ins.prototyp.model.Specialization;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kulyndar;
 */
@Repository
public class EmployeeDao {

    @PersistenceContext
    EntityManager em;

    public void persist(Employee epm) {
        em.persist(epm);
    }
    public Employee findByName(String name){
        return em.createNamedQuery("Employee.findByEmpName", Employee.class).setParameter("empName", name).getResultList().get(0);
    }

    public boolean exists(String login) {
        if (em.find(Employee.class, login) != null) {
            return true;
        }
        return false;
    }
    public List<Employee> findAll(){
        return em.createNamedQuery("Employee.findAll", Employee.class).getResultList();
    }
    
}
