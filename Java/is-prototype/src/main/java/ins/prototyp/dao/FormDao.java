/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.dao;

import ins.prototyp.model.Answer;
import ins.prototyp.model.Form;
import ins.prototyp.model.Project;
import ins.prototyp.model.Question;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kulyndar;
 */
@Repository
public class FormDao {
     @PersistenceContext
    EntityManager em;
     
     public Form findByProject(Project proj){
         List<Form> list = em.createNamedQuery("Form.findByProject", Form.class).setParameter("project", proj).getResultList();
         return list.isEmpty()?null:list.get(0);
     }
     
     public List<Answer> findAllAnswers(int id){
         return em.createNamedQuery("Answer.findByQId", Answer.class).setParameter("qId", id).getResultList();
     }
     public  void persist(Form form){
         em.persist(form);
     }
     public List<Form> findAll(){
         return em.createNamedQuery("Form.findAll", Form.class).getResultList();
     }
     public Form find(int id){
         return em.find(Form.class, id);
     }
     public  List<Question> findQuestions(Form f){
         return em.createNamedQuery("Question.findByFormId", Question.class).setParameter("formId", f.getFormId()).getResultList();
     }
     
     
}
