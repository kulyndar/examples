/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.dao;

import ins.prototyp.model.Project;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kulyndar;
 */
@Repository
public class ProjectDao {

    @PersistenceContext
    EntityManager em;
    
    public Project findByID(Integer id) {
    	return em.find(Project.class, id);
    }
    
    public void persist(Project proj) {
        em.persist(proj);
    }

    public Project find(String name) {
        return em.createNamedQuery("Project.findByProjName", Project.class).setParameter("projName", name).getResultList().get(0);
    }

    public List<Project> findByDateFrom(Date date) {
        return em.createNamedQuery("Project.findByDateFrom", Project.class).setParameter("dateFrom", date).getResultList();
    }

    public List<Project> findByDateTo(Date date) {
        return em.createNamedQuery("Project.findByDateTo", Project.class).setParameter("dateTo", date).getResultList();
    }

    public List<Project> findByStatus(String status) {
        return em.createNamedQuery("Project.findByStatus", Project.class).setParameter("status", status).getResultList();
    }

    public List<Project> findAll() {
        return em.createNamedQuery("Project.findAll", Project.class).getResultList();
    }
    
    public List<Project> findInInterval(Date from, Date to){
        return em.createNamedQuery("Project.findByInterval", Project.class).setParameter("dateFrom", from).setParameter("dateTo", to).getResultList();
    }
     public List<Project> findInIntervalList(Date from, Date to){
        return em.createNamedQuery("Project.findByIntervalList", Project.class).setParameter("dateFrom", from).setParameter("dateTo", to).getResultList();
    }
    

}
