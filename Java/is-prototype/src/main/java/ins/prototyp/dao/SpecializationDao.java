/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.dao;

import ins.prototyp.model.Specialization;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kulyndar;
 */
@Repository
public class SpecializationDao {
    
    @PersistenceContext
    EntityManager em;
    
    public void persist(Specialization spec){
        em.persist(spec);
    }
    public List<Specialization> findAll(){
        return em.createNamedQuery("Specialization.findAll", Specialization.class).getResultList();
    }
    public Specialization find(String name){
        return em.find(Specialization.class, name);
    }
    public void refresh(){
        em.getEntityManagerFactory().getCache().evictAll();
    }
    
    
}
