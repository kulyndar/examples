/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ins.prototyp.dao;

import ins.prototyp.model.Client;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kulyndar;
 */
@Repository
public class ClientDao {
    @PersistenceContext
    EntityManager em;
    
    public Client find(String name){
        return em.find(Client.class, name);
    }
    public List<Client> findAll(){
        return em.createNamedQuery("Client.findAll", Client.class).getResultList();
    }
    
    public void persist(Client client) {
    	em.persist(client);
    }
}
